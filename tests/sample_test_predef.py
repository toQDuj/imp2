import sys
sys.path.append('/Users/brian/Documents/NIMS_postdoc/python/imp2')
import imp2
imp2.parameters.loadParams('testPredef.json')
imp2.DS() #load data
imp2.FL() #normalize to flux
imp2.TI() #normalize to time
imp2.TR() #normalize to transmission
imp2.TH() #normalize to thickness
imp2.AU() #scale to absolute units
imp2.MK() #set mask
imp2.QCalc() #calculate Q vectors
imp2.SA() #compensate for self-absorption (needs to be done after Q calculation)
imp2.Integrator() #integrate/ azimuthal averaging
imp2.Write1D(filename = 'tmp/test1Dresults.csv') #write the integrated data to a file
imp2.parameters.writeConfig('tmp/testParam.impcfg') #save the configuration (useful if edited on the fly)
imp2.Write2D(filename = 'tmp/2Doutput.h5')
imp2.WritePNG(filename = 'tmp/fishies.png', imscale = 'log')

