import sys
sys.path.append('/Users/brian/Documents/NIMS_postdoc/python/imp2')
import imp2
imp2.DS(fname = 'data/Mehdi_Meng_1_005.gfrm', dtransform = 3)
imp2.FL(flux = 1.)
imp2.TI(time = 21600.)
imp2.TR(tfact = 0.4)
imp2.TH(thick = 0.001)
imp2.AU(cfact = 12.455)
imp2.MK(mfname = 'data/Bruker_mask_ud.png', mwindow = [0.99, 1.1])

imp2.QCalc(wlength = 12.398/5.41*1.e-10, clength = 1.052,
        plength = [210.526e-6, 210.526e-6], beampos = [508.5, 533.2])
#self-absorption correction needs angles
imp2.SA(sac = 'plate')
imp2.BG(filename = "tmp/bgnd2D.h5")
imp2.Integrator(nbin = 200, binscale = "linear")
imp2.Write1D(filename = 'tmp/test1Dresults.csv') 
imp2.parameters.writeConfig('tmp/testParam.impcfg') #save the configuration (useful if edited on the fly)
imp2.Write2D(filename = 'tmp/2Doutput.h5')
imp2.WritePNG(filename = 'tmp/fishies.png', imscale = 'log')

