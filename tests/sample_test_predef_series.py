import sys
sys.path.append('/Users/brian/Documents/NIMS_postdoc/python/imp2')
import imp2
#testing loading of a set of images (all from the same sample)
imp2.parameters.loadParams('testPredefSeries.json')
imp2.DS() #load data
imp2.FL() #normalize to flux
imp2.TI() #normalize to time
imp2.TR() #normalize to transmission
imp2.TH() #normalize to thickness
imp2.AU() #scale to absolute units
imp2.MK() #set mask
imp2.QCalc() #calculate Q vectors
imp2.SA() #compensate for self-absorption (needs to be done after Q calculation)
imp2.Integrator() #integrate/ azimuthal averaging
imp2.Write1D(filename = 'tmp/test1Dresults.csv') #write the integrated data to a file
imp2.parameters.writeConfig('tmp/testParam.impcfg') #save the configuration (useful if edited on the fly)
imp2.Write2D() #write the 2D data to a file (takes no arguments)
imp2.Write2D(filename = 'tmp/test2D.bla') #write the 2D data to a file (takes no arguments)
imp2.WritePNG(filename = 'tmp/fishies.png', imscale = 'log')
