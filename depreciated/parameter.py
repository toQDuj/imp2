#!/usr/bin/env python                                                          
#coding: utf-8
#

__author__ = "Brian R. Pauw"
__contact__ = "brian@stack.nl"
__license__ = "GPLv3+"
__date__ = "2013/11/29"
__status__ = "beta"

import logging, inspect
import numpy as np

class Parameter(object):
    """
    Overview
    ========
    Defines a parameter by its attributes and provides additional functionality
    for checking the validity of parameters. Parameters can be defined throuh
    use of keyword-value pairs or a simple JSON dict.

    Example Usage
    ============
    testParameter=Parameter()
    testParameter.factory(name="tpar",value=12.3,valueRange=[0,inf],unit="mm",
        explanation="this is a test parameter")

    #change the value:
    testParameter.setValue(14.3)
    #or
    testParameter.set("Value",14.3)
    #set an evil bit (not part of the base attributes):
    testParameter.set("evil",True)
    #retrieve it at a later stage:
    testParameter.get("evil")
    
    Parses Parameter configuration supplied as keyword-value pairs, 
    adding Parameter attributes if they are not part of the base set.
    Can also take a simple json dict, using Parameter.factory(**[jsonDict])

    Base Parameter attributes are:
    - *Name*: internal (code) variable name
    - *DisplayName*: Short, descriptive name for GUI
    - *Value*: The value of the parameter
    - *ValueRange*: The valid value limits (for string values, this can be a 
        list of valid string choices). Two-element vector. For arrays, this  
        must be valid for all array values.
    - *AbsoluteSTD*: The uncertainty on the absolute parameter value in 1 STD
    - *RelativeSTD*: The relative (fractional) uncertainty of value in 1 STD
    - *DataType*: Can be 'float', 'int', 'array', 'str' or 'bool'
    - *Explanation*: A string with a detailed explanation
    - *Default*: Default value to revert to in case of invalid supplied value
    - *Unit*: Short string that indicates the units, f.ex. 'm' or '1/(s m)' 
    - *Size*: The required number of values in an array.

    """
    #using Ingo Bressler's cutesnake/parameter functions for lots of inspiration:

    logging.getLogger('Parameter')   
    logging.basicConfig(level = logging.DEBUG)

    _name = None #internal variable name
    _displayName = None #short but descriptive name for GUIs
    _value = None #value of the parameter
    _valueRange = None #list of valid strings or 2-element min/max array 
    _absoluteSTD = None #value in same units as value indicating 1 STD 
    _relativeSTD = None #fractional, single value indicating 1 STD
    _dataType = None #data type, must be class or type
    _explanation = None #detailed explanation of value
    _default = None #default value to revert to in case of error
    _unit = "" #units (or list of units) for the value
    _size = 1 #number of values in array if applicable, default is one. doesn't apply to strings
    
    #this dict indicates the base parameter attributes, base attributes
    #may use additional checks, whereas custom ones are accepted as is.
    _nameDict= { 
            'name':'Name', #identifier used in code
            'displayname':'DisplayName',
            'value':'Value',
            'valuerange':'ValueRange',
            'absolutestd':'AbsoluteSTD',
            'relativestd':'RelativeSTD',
            'datatype':'DataType',
            'explanation':'Explanation',
            'default':'Default',
            'unit':'Unit',
            'size':'Size',
            }

    @classmethod
    def factory(self,**kwargs):

        for kw in kwargs:
            try:
                self.set(kw,kwargs[kw])
            except:
                logging.warning('Could not set: {} to: {}'
                        .format(kw,kwargs[kw]) )

        #postprocessing:
        self.checkSize()
        self.clipValue()
        return self

    
    ################### BASIC FUNCTIONS ###################
    #added to improve compatibility with Ingo's parameter class
    def __str__(self):
        return "{}: {}".format(self.displayName(), self.value())

    def __eq__(self,other):
        if self.dataType() == other.dataType():
            try:
                return self.attributes()==other.attributes()
            except:
                return False
        else:
            return False

    def __ne__(self,other):
        return not self.__eq__(other)

    ################### METASETTERS AND METAGETTERS ###################

    @classmethod
    def set(self,name,value):
        """sets a particular attribute, irrespective of whether it is a 
        standard parameter attribute or a custom one"""
        if name.lower() in self._nameDict:
            setterName = 'set' + self._nameDict[name.lower()]
            setterFunc = self.getattr(setterName)
            setterFunc(value)

        else: #was a custom attribute
            self.setattr(name,value)

    @classmethod
    def get(self,name):
        """gets value whether it is an extra attribute or standard value"""
        if name.lower() in self._nameDict:
            getterName = self._nameDict[name.lower()]
            #lower the first letter:
            getterName[0] = getterName[0].lower()
            getterFunc = self.getattr(getterName)
            return getterFunc(value)

        else: #was a custom attribute
            return self.getattr(name)


    ################### SETTERS ###################

    @classmethod
    def setattr(self,aname,avalue):
        setattr(self,aname,avalue)
        #alternative, use self.__setattr__

    @classmethod
    def setName(self,name):
        #no check necessary, but should be a string
        self._name=str(name)

    @classmethod
    def setDisplayName(self,displayName):
        #no check necessary, but should be a string
        self._displayName=str(displayName)

    @classmethod
    def setValue(self,value,default=False):
        """
        Sets the value in the parameter. Will cast the value into the datatype
        if specified before, and will check whether the value falls within 
        the value range if set before.
        """

        if not self.dataType() is None: 
            if isinstance(value,self.dataType()):
                output = value
            else:
                logging.info('value {} not of the right datatype, trying to cast into {}'
                        .format(value,self._dataType) )
                try:
                    output = self.dataType()(value)
                except:
                    logging.error('Could not cast value {} into datatype {}'
                            .format(value,self._dataType) )
                    return
                logging.info('successfully cast.')
        else:
            #no datatype supplied
            logging.warning('dataType not yet set, casting value and setting dataType as type {}'
                    .format( type(value) ) )
            self.setDataType( type(value) )
            output = value
        
        if default:
            self._default = output
            pass
        else:
            self._value = output

    @classmethod
    def setValueRange(self,Vals):
        """should take care of putting minimum value in [0] position
        also should allow valueRange to be a list of valid options
        in case of a string value"""
        if not (self.isList(Vals) or self.isArray(Vals)):
            logging.warning('ValueRange should be a list of strings or 2-element array, not setting valueRange.')
            return

        if self.isList(Vals):
            output = Vals

        elif self.isArray(Vals):
            output = np.array([np.min(Vals), np.max(Vals)])

        else:
            logging.warning('Please explain how you got here')
            output = None

        self._valueRange = output

    @classmethod
    def setDataType(self,value=None):
        if value is None:
            value=type(self.value())

        #in case the value is supplied as a string (for example from JSON)
        stringSubst = {
                "array":np.array,
                "int":int,
                "float":float,
                "str":str,
                "bool":bool,
                "unicode":unicode,
                }

        if self.isString(value):
            try:
                logging.info('DataType received string: {}, changing to type'.
                        format(value) )
                value=stringSubst[value.lower()]
            except:
                logging.warning('supplied datatype: {} not understood!'
                        .format(value))

        #data type should be class or type
        if not ( inspect.isclass(value) or isinstance( value, type) ):
            logging.warning('DataType must be a class or type')
            if not self.value() is None:
                value = type(self.value())
                logging.warning('setting dataType to {}'
                        .format(value))
            else:
                logging.warning('not setting dataType')
                return

        self._dataType = value

    @classmethod
    def absoluteSTD(self):
        return self._absoluteSTD

    @classmethod
    def setAbsoluteSTD(self,value):
        #nothing much to do
        self._absoluteSTD =float(value)

    @classmethod
    def setRelativeSTD(self,value):
        #nothing much to do
        self._relativeSTD = float(value)

    @classmethod
    def setExplanation(self,value):
        #nothing much to do
        self._explanation = str(value)

    @classmethod
    def setDefault(self,value):
        self.setValue(value,default=True)
        #not checking for clipping, should be unneccesary for defaults

    @classmethod
    def setUnit(self,value):
        self._unit = str(value)

    @classmethod
    def setSize(self,value):
        if not isinstance(value,int):
            logging.warning('size value should be an integer')
            try:
                value = int(value)
                logging.warning('setting size to {}'.format(value))
            except:
                logging.warning('not setting size value, remains at {}'
                        .format(self.size() ) )
                return
        self._size = value

    ################### GETTERS ###################

    @classmethod
    def getattr(self,aname):
        if self.hasattr(aname):
            return getattr(self,aname)
        else:
            raise AttributeError

    @classmethod
    def hasattr(self,aname):
        return aname in self.attributeNames()

    @classmethod
    def attributeNames(self):
        """list of attibute names"""
        return self.attributes().keys()

    @classmethod
    def attributes(self):
        """dictionary of attibutes and their values or locations"""
        return vars(self)

    @classmethod
    def name(self):
        return self._name

    @classmethod
    def displayName(self):
        return self._displayName

    @classmethod
    def value(self):
        return self._value

    @classmethod
    def valueRange(self):
        return self._valueRange

    @classmethod
    def dataType(self):
        return self._dataType

    #one more to follow standard definition:
    @classmethod
    def dtype(cls):
        return self.dataType()

    @classmethod
    def relativeSTD(self):
        return self._relativeSTD

    @classmethod
    def explanation(self):
        return self._explanation

    @classmethod
    def default(self):
        return self._default

    @classmethod
    def unit(self):
        return self._unit

    @classmethod
    def size(self):
        return self._size

    ################### CHECKERS ###################

    @classmethod
    def checkSize(self,value=None):
        """checks whether the number of values in a value array is identical
        to the required number of elements"""
        if value is None:
            value=self.value()
        reqSize=self.size()
        if self.isString(value) or self.isList(value):
            logging.info('cannot check for size on a non-numeric or non-array value')
            return True

        if (reqSize == 1) and not self.isArray(value):
            return True
        else:
            return prod( shape(value) )== reqSize

    @classmethod
    def clipValue(self,value=None):
        """
        this will test if the value is out of the provided bounds, and clips
        the value to the bounds if necessary. 
        Works for arrays, numbers, boolean and strings 
        
        """
        if value is None:
            value=self.value()
        if value is None: #retrieved value is also not set
            #nothing to do if there is no value to clip
            return

        valueRange=self.valueRange()
        if valueRange == None:
            logging.info( 'no valueRange set for {}, cannot perform clipping'
                    .format( self.name() ) )
            return

        #test to see if the supplied value falls within limits
        if not valueRange is None:
            if self.isArray(value):
                test=(value == value.clip( valueRange[0], valueRange[1]) )
                if False in test:
                    logging.warning('supplied values {} out of range {} to {}. Clipping...'
                            .format(value,valueRange[0],valueRange[1]) )
                    output = value.clip( valueRange[0], valueRange[1] )

            elif self.isNumeric(value):
                if ( value >= valueRange[0] ) and ( value <= valueRange[1] ):
                    pass #no problem
                else:
                    logging.warning('supplied values {} out of range {} to {}. Clipping...' 
                            .format(value,valueRange[0],valueRange[1]) )     
                    value=np.maximum( value, valueRange[0] )
                    value=np.minimum( value, valueRange[1] )
                    output=value

            elif self.isBoolean(value):
                #nothing to do as far as I can tell
                pass

            elif self.isString(value):
                #check if value is part of list in valueRange
                if not value in valueRange:
                    #set to default
                    logging.warning('value {} not valid. Should be either of {}'
                            .format(value,valueRange) )
                    defa=self.default()
                    if not defa is None:
                        logging.warning('Default provided, setting to default: {}'
                                .format(self.default()) )
                        output=defa
                    else:
                        logging.warning('Cannot revert to default: no default set')
                        return
                else:
                    output = value

            else:
                logging.warning('Data type of value not supported by the clipping function')
        self.setValue(output)

    ################### TESTS ###################

    @classmethod
    def isBoolean(self,value=None):
        if value is None:
            value=self.value()
        return isinstance(value, bool)

    @classmethod
    def isList(self,value=None):
        if value is None:
            value=self.value()
        return isinstance(value, list)

    @classmethod
    def isString(self,value=None):
        if value is None:
            value=self.value()
        return isinstance(value, basestring)

    @classmethod
    def isArray(self,value=None):
        #check for numpy.ndarray isinstance
        if value is None:
            value=self.value()
        #try:
        #    value[0]
        #except TypeError:
        #    return False
        #return isNumeric(value[0])

        return isinstance(value,np.ndarray)

    @classmethod
    def isNumeric(self,value=None):
        """
        Checks if the value in self or supplied value is numeric through duck typing
        Check if it is an array before checking if it is numeric.
        """
        if value is None:
            value=self.value()

        #check if it is a string:
        if self.isString(value):
            return False

        try:
            float(value)
        except StandardError:
            try:
                complex(value)
            except StandardError:
                return False

        return True

