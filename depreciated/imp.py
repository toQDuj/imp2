#these functions read and plot the .dat files from the impossible project SAXS reduction
import os
import time
import re #regular expressions
from scipy import interpolate #needed for GC fitting
#import numbers #for checking if number is int
#from multiprocessing import Pool 

#pool=Pool(2) #2 cores
#parallel processing from user753720 at stackoverflow:

from multiprocessing import Process, Pipe  
import multiprocessing
from itertools import izip  

def spawn(f):  
    def fun(pipe,x):  
        pipe.send(f(x))  
        pipe.close()  
    return fun  

def parmap(f,X):  
    pipe=[Pipe() for x in X]  
    processes=[Process(target=spawn(f),args=(c,x)) for x,(p,c) in izip(X,pipe)]  
    numProcesses = len(processes)  
    processNum = 0  
    outputList = []  
    while processNum < numProcesses:  
        endProcessNum = min(processNum+multiprocessing.cpu_count(), numProcesses)  
        for proc in processes[processNum:endProcessNum]:  
            proc.start()  
        for proc in processes[processNum:endProcessNum]:  
            proc.join()  
        for proc,c in pipe[processNum:endProcessNum]:  
            outputList.append(proc.recv())  
        processNum = endProcessNum  
    return outputList    

#if __name__ == '__main__':  
#    print parmap(lambda x:x**x,range(1,5))     
#end copy from stackoverflow

def imp_daemon(Display=False,Ext='impdat',Dir='current'):
    #this function continuously polls the working directory for new or updated .impdat files. These files contain just the imp header, sufficient for starting the processing. 
    #this function uses a new imp label, styp. This indicates the type of measurement, be it direct beam, silver behenate*, background, glassy carbon or sample measurement. Background and sample measurement labels will also include information on whether it is anisotropic data or isotropic data.
    Kansas=os.getcwd()
    if Dir!='current':
        if os.path.exists(Dir):
            Kansas=Dir
        else: 
            print "path supplied to the imp daemon is not a valid path, using current directory"
    fh=[]
    if Display:
        fh=figure(figsize=(12,8))
        ion()


    ListDir=os.listdir(Kansas)
    LD=list() #because ListDir.remove() does not work nice with for loops
    #remove all files not containing the impdat identifier
    for File in ListDir:
        if Ext in File:
            LD.append(File)
            
    ModTime=dict()
    NewModTime=dict()
    for File in LD:
        ModTime[File]=os.path.getmtime(File)
    #now we have the directory condition (files and last modified times) at start. Whenever a file is modified, we can find out which one and process it
    while True:
        #our nice infinite daemon loop
        #wait a bit for propriety
        time.sleep(.5)
        #check if there has been any change in the directory
        NewListDir=os.listdir(Kansas)
        NLD=list()
        NewModTime=dict()
        #remove all files not containing the impdat identifier
        for File in NewListDir:
            if Ext in File:
                NLD.append(File)
        #find all new modification times
        for File in NLD:
            NewModTime[File]=os.path.getmtime(File)

        #find out if there have been any changes in either number of files or file mod times:
        if NewModTime!=ModTime:
            print "*****************************************************************"
            print "change detected"
            #Ch-ch-ch-ch-changes!
            #find the file(s) that have changed since last time and process
            for File in NLD:
                #                try:
                if not File in LD:
                    print "file {0} created".format(File)
                    imp_process(File,Display=fh)
                elif NewModTime[File]>ModTime[File]:
                    print "file {0} changed".format(File)
                    imp_process(File,Display=fh)
            print "all new files processed"
            #                except Exception, what: #catch exceptions, but do stop at keyboard interrupts and system exits
            #                    try:
            #                        message, info = what
            #                    except:
            #                        message, info = what, None
            #                    if info:
            #                        #"...print source code info..."
            #                        print "SyntaxError:", msg
            #                    return
            #after processing, update the lists
            ModTime=NewModTime
            LD=NLD

def imp_process(filename,Return_msgs=False,Display=[]):
    #this will process a measurement based on the information in its header
    DLib,discard=readimpheader(filename)
    if not check_DLib(DLib):
        print "DLib check failed"
        return
    DLib=check_DLib(DLib,ReturnDLib=True)

    #in case we need ofname
    ofbasename=DLib['sfname'].rsplit('.',1)[0]
    #if DLib['bfname']=='': #no background supplied
    ofname=ofbasename+'_cor.imp'
    ofbname=ofbasename+'_bgs.imp'
    ofdname=ofbasename+'_bgs.dat'
    #else:
    #    ofname=ofname+'_bgs.imp'
        
    if DLib['styp']=='directbeam':
        imp_imagecorrect(DLib,Force=True)
        imp_finddirect(DLib)

    elif DLib['styp']=='ringcal':
        print "ring calibration not implemented yet"
    elif DLib['styp'] in {'gccal','darkcurrent','bgnd','sample'}:
        imp_imagecorrect(DLib,Force=True)
        imp_imgint(ofname,Force=True)
        if DLib['styp']=='gccal':
            #determine calibration factor
            #Sc=imp_gccal(ofname,DLib['cfname'],Qlimits=DLib['ilim'][0:2],Psilimits=DLib['ilim'][2:4])
            #try to read integrated data, otherwise take GC image:
            if os.path.isfile(ofdname):
                GCmfname=ofdname
            else:
                GCmfname=ofname
            #print 'calibrating with {0}'.format(GCmfname) 
            if Display!=[]:
                Sc,Qfit,Ifit=imp_gccal(GCmfname,DLib['cfname'],Qlimits=DLib['ilim'][0:2],Psilimits=DLib['ilim'][2:4],OutputI=True)
            else:
                Sc=imp_gccal(GCmfname,DLib['cfname'],Qlimits=DLib['ilim'][0:2],Psilimits=DLib['ilim'][2:4])
            print 'Scaling factor {0}, background level {1}'.format(Sc[0],Sc[1])
            
    if Display!=[]:
        fh=Display
        fh.clf()
        axim = fh.add_subplot(121, aspect='equal', axisbg='0.75')
        #check if we have a background subtracted image:
        if os.path.isfile(ofbname):
            DLtemp,darr=readimp(ofbname)
            Od=imp_darr2vars(DLtemp,darr)
            img=Od['I']/DLtemp['mtime']
        elif os.path.isfile(ofname):
            DLtemp,darr=readimp(ofname)
            Od=imp_darr2vars(DLtemp,darr)
            img=Od['I']/DLtemp['mtime']
        else: 
            img=zeros((2,2))
        
        if os.path.isfile(ofdname):
            DLtemp,darr=readdat(ofdname)
            Od=imp_darr2vars(DLtemp,darr)
            I=Od['I']
            Q=Od['Q']
            E=Od['SE']
        else:
            I=1
            Q=1
            E=1

        title('Corrected countrate (Hz)(log scale)')
        imshow(log10(img)) 
        colorbar()
        xlabel('Q 1/m')
        ylabel('Q 1/m')
        axgraph = fh.add_subplot(122, axisbg='0.9')
        title('integrated plot (log-lin)')
        if DLib['styp']=='gccal':
            errorbar(Q,I*Sc[0]+Sc[1],E*Sc[0])
            plot(Qfit,Ifit,'r-')
        else:
            errorbar(Q,I,E)
        yscale('log')
        xlabel('Q 1/m')
        ylabel('I')
        #fh.show()
        #show()
        draw()

    if Return_msgs:
        return msgs
    else:
        return

def imp_imgbgsints(Force=False):
    #this program processes a series of images which have not yet been background subtracted. Background subtraction performed automatically.
    Kansas=os.getcwd()
    ListDir=os.listdir(Kansas)
    dati=0
    
    #figure out how many files we have of the right type
    LDi=1
    ValidFileList=[]
    for filename in ListDir:
        if not ('_cor.imp' in filename):
            #skip this one
            continue
        LDi+=1 #add one to the length
        ValidFileList.append(filename)
#
#    """
    print 'There are %i files' %(LDi)
    FDi=0
    for filename in ValidFileList:
        FDi+=1
        print '{0}/{1}'.format(FDi,LDi)
        imp_imgint(filename,Force)
    """
    ChunkSize=20 #for parallel processing, the number of simultaneous open files are limited
    #parallel processing:
    ChunkEnd=0
    while ChunkEnd<size(ValidFileList):
        Endi=numpy.min(array([size(ValidFileList),ChunkEnd+ChunkSize]))
        parmap(lambda fn: imp_imgint(fn,Force),ValidFileList[ChunkEnd:Endi])
        ChunkEnd=Endi
    """
def imp_imgints(Force=False):
    Kansas=os.getcwd()
    ListDir=os.listdir(Kansas)
    dati=0
    
    #figure out how many files we have of the right type
    LDi=1
    for filename in ListDir:
        if not ('_bgs.imp' in filename):
            #skip this one
            continue
        LDi+=1 #add one to the length

    print 'There are %i files' %(LDi)
    for filename in ListDir:
        if not ('_bgs.imp' in filename):
            #skip this one
            continue
        else:
            imp_imgint(filename,Force)

def imp_imagecorrect(DLib,Force=False,ofname=''):
    #check the DLib first:
    if not(check_DLib(DLib)):
        print "DLib incorrect, cannot proceed with image correction until DLib is valid"
        return
    if ofname=='':
        ofname=DLib['sfname'].rsplit('.',1)[0]
        ofname=ofname+'_cor.imp'
    #check if file exists
    if os.path.isfile(ofname):
        if (Force==True) or (Force==1):
            print "Output file '{0}' exists, reprocessing forced".format(ofname)
        else:
            print "Output file '{0}' exists, not reprocessing".format(ofname)
            return

    #read raw images
    simage=imp_imageread(DLib['sfname'],DLib) 

    #darkcurrent corrections:
    if DLib['dccount']!='':
        print "correcting for darkcurrent with homogeneous counts"
        if DLib['dctimedep'].lower()=='false':
            #setting darkcurrent measurement time identical to sample measurement time (dctimedep=false)
            DLib['dcmtime']=DLib['mtime']
        if DLib['dcmtime']=='':
            print "darkcurrent count set, but no dc measurement time set, assuming 1 (second)"
            DLib['dcmtime']=1
        if DLib['dcfname']!='':
            print "both darkcurrent file and dc count provided, using counts"
        if DLib['dcnpix']==1:
            print "for darkcurrent counts, number of pixels dcnpix defaults to dsize (detector size) but is currently one. Adjusting to dsize"
            DLib['dcnpix']=prod(DLib['dsize'])
        dcimage=DLib['dccount']
        dcnpix=DLib['dcnpix']
    elif DLib['dcfname']!='':
        print "correcting for darkcurrent with image {0}".format(DLib['dcfname'])
        #using darkcurrent filename
        if DLib['dctimedep'].lower()=='false':
            #setting darkcurrent measurement time identical to sample measurement time (dctimedep=false)
            DLib['dcmtime']=DLib['mtime']
        if DLib['dcmtime']=='':
            print "darkcurrent image set, but no dc measurement time set, assuming 1 (second)"
            DLib['dcmtime']=1
        if DLib['dcnpix']>1:
            print "for darkcurrent images, dcnpix should be set to one. Assuming one."
            if DLib['dccount']=='':
                #if we are not in danger of using dcnpix for dccount purposes, store in DLib
                DLib['dcnpix']=1
        dcimage=imp_imageread(DLib['dcfname'],DLib)
        dcnpix=1
    else:
        dcimage=0
        dcnpix=1
        
    if DLib['gammafname']!='':
        print "gamma correction not yet implemented. Please consult your dealer"
    if DLib['fffname']!='':
        ffimage=imp_imageread(DLib['fffname'],DLib)
    else:
        ffimage=1.
    if DLib['dwindow']!='':
        dmask=(simage>=numpy.min(DLib['dwindow'])*(simage<=numpy.max(DLib['dwindow'])))
        print "dwindow applied after correcting for fixme"
    else:
        dmask=''
    if DLib['poni']=='': #missing point of normal incidence
        DLib['poni']=DLib['pmid']
    
    #ready for geometrical corrections. 
    #old version, but found out that the 0.5 pixel shift is unnecessary
    #Lh=numpy.array(DLib['plength'][0]*(arange(1,(1+size(simage,1)))-DLib['poni'][0])-0.5*DLib['plength'][0])
    #Lv=numpy.array(DLib['plength'][1]*(arange(1,(1+size(simage,0)))-DLib['poni'][1])-0.5*DLib['plength'][1])[:,newaxis] #enverticalize
    Lh=numpy.array(DLib['plength'][0]*(arange(1,(1+size(simage,1)))-DLib['poni'][0]-1)) #fixed, pixelshift -1. minuscule correction, 2012-07-06
    Lv=numpy.array(DLib['plength'][1]*(arange(1,(1+size(simage,0)))-DLib['poni'][1]-1))[:,newaxis] #enverticalize
    Ld=sqrt(Lh**2+Lv**2)
    Lp=sqrt(DLib['clength']**2+Ld**2) #length to each pixel

    sphcf=Lp**2/(DLib['plength'].prod())*Lp/DLib['clength']
    #this is done to keep sphcf within reasonable orders of magnitude:
    sphcf=sphcf/numpy.min(sphcf) #such a correction is negated by the absolute intensity calibration correction. You are doing that, right?

    #polarization correction
    imsize=array([size(simage,1),size(simage,0)]) 
    Q,PSI=qpsi_2D(imsize,DLib['droffset'],DLib['wlength'],DLib['plength'],DLib['clength'],DLib['pmid']) #this function is defined in saxstools.py
    PSI_rad=(PSI-DLib['pangle'])/360*2*pi
    twotheta_rad=2*arcsin(Q*DLib['wlength']/(4*pi))
    Pinplane=DLib['pcor']
    Poutplane=1.-Pinplane
    F=Pinplane*(1-(sin(PSI_rad)*sin(twotheta_rad))**2)+Poutplane*(1-(cos(PSI_rad)*sin(twotheta_rad))**2)

    #sample self-absorption
    if DLib['ssac']=='plate':
        aD=-log(DLib['tfact']) #alpha D, where D is the thickness of the material
        if aD==0:
            #print 'You have selected plate sample geometry for sample self absorption, but the transmission factor is 1. Assuming no sample self-absorption'
            ssf=1.
        else:
            #origin of NAN at beam center, happens when pmid is exactly on the int. center of pixel
            ssf=exp(aD)*(-exp(-aD) + exp(-aD/cos(twotheta_rad))) / (aD - aD/cos(twotheta_rad))
            #fix NaN at center if exists:
            if sum(isnan(ssf))>0:
                loc=argwhere(isnan(ssf))
                ssf[loc[0][0],loc[0][1]]=1 #should be one. fixed 2012-07-06
    else:
        ssf=1.

    #calculate corrected intensity
    #darkcurrent- and time-corrected image:
    #dcci=simage*1./float(DLib['mtime'])-dcimage*1./float(DLib['dcmtime'])
    #new version 20120806: allowing use of dccount and dcnpix:
    print "size of dcimage={0}, dcmtime={1}, dcnpix={2}".format(prod(shape(dcimage)),float(DLib['dcmtime']),float(dcnpix))
    dcci=simage*1./float(DLib['mtime'])-dcimage*1./(float(dcnpix)*float(DLib['dcmtime']))
    Iomg=ssf*F/DLib['sflux']*dcci/ffimage*sphcf
    #Poisson calculation errors
    if DLib['etype'] in ('Poisson','auto'):
        #errors in simage and darkcurrent (though dc probably does not follow Poisson
        if shape(dcimage)==(): #no need to count darkcurrent error
            erdc=sqrt(numpy.maximum(simage*1.,1.))/float(DLib['mtime'])
        else:
            erdc=sqrt(numpy.maximum(simage*1.,1.))/float(DLib['mtime'])+sqrt(numpy.maximum(dcimage*1.,1.))/(float(dcnpix)*float(DLib['dcmtime']))
        
        #largest non-zero element
        lnzero=numpy.min(erdc[simage>0])
        #calculate Poisson errors. Darkcurrent is not counted in photons, so use of dcci should not be a problem
        #TODO: check what I mean with the photon statement, since now we do have this correction option.
        Eomg=ssf*F/DLib['sflux']*(numpy.maximum(erdc,lnzero))/ffimage*sphcf
        #correction for thickness, transmission and calibration factor
        Eomg=Eomg/(DLib['tfact']*DLib['thick'])*DLib['cfact']
    else:
        Eomg=''
    print 'cfact={0}'.format(DLib['cfact'])
    Iomg=Iomg/(DLib['tfact']*DLib['thick'])*DLib['cfact']

    #    write results into image
    #DLib has to have the keys 'channels' with the number of channels, and 
    # 'channel1', 'channel2' etc, with the channel information identifiers  'I','Q','PSI','IERR','QERR','PSIERR' or 'MASK'
    channeln=1
    DLib['channel'+str(channeln)]='I'
    if (Eomg!='')or(dmask!=''):
        Iomg=Iomg[:,:,newaxis]
    if Eomg!='':
        channeln+=1
        DLib['channel'+str(channeln)]='IERR'
        Eomg=Eomg[:,:,newaxis]
        Iomg=concatenate((Iomg,Eomg),2)
    if dmask!='':
        channeln+=1
        DLib['channel'+str(channeln)]='MASK'
        dmask=dmask[:,:,newaxis]
        Iomg=concatenate((Iomg,dmask),2)
    DLib['channels']=channeln
    DLib['size']=DLib['dsize']
    print 'writing image to file {0}'.format(ofname)
    writeimp(DLib,Iomg,ofname)

    
def imp_imageread(filename,DLib):
    #reads and transforms images.
    if filename.rsplit('.',1)[1].lower()=='png':
        #just a PNG image, return image with averaged RGB(A) values.
        darr=(imread(filename)) 
        if ndim(darr)>2:
            darr=numpy.mean(darr,2)
        darr=imtransform(darr,DLib['dtransform'])
        return darr
        
    EnDict=dict() #dictionary with various ways of denoting the endianness
    EnDict['little']=['little','l','ieee-le']
    EnDict['big']=['big','b','ieee-be']
    EnDict['native']=['n','native']

    ByDict=dict() #dictionary with number of bytes per data type
    ByDict['one']=['char','uchar','signed char','schar','unsigned char','uint8','int8','char*1','integer*1']
    ByDict['two']=['short','unsigned short','ushort','uint16','int16','integer*2']
    ByDict['four']=['int','unsigned integer','uint','integer*3','single','real*4','long','unsigned long','float','float32','uint32','int32']
    ByDict['eight']=['long long','unsigned long long','double','float64','uint64','int64','integer*4','real*8']

    DtDict=dict() #dictionary translating data types into their Python format character
    DtDict['c']=['char','c']
    DtDict['b']=['schar','int8','signed char','integer*1','b']
    DtDict['x']=['uchar','unsigned char','uint8','x']
    DtDict['h']=['short','int16','integer*2','h']
    DtDict['H']=['ushort','unsigned short','uint16','H']
    DtDict['i']=['int','integer*3','signed integer','int32','i']
    DtDict['I']=['uint','uint32','unsigned integer','I']
    DtDict['l']=['long','l']
    DtDict['L']=['unsigned long','L']
    DtDict['q']=['long long','int64','integer*4','q']
    DtDict['Q']=['unsigned long long','uint64','Q']
    DtDict['f']=['float','float32','single','real*4','f']
    DtDict['d']=['double','float64','real*8','d']

    
    if DLib['dendian'].lower() in EnDict['little']: DTP='<'
    elif DLib['dendian'].lower() in EnDict['big']: DTP='>'
    elif DLib['dendian'].lower() in EnDict['native']: DTP='='
    else:
        print "No endianness given, assuming native"
        DTP='='
    #now we complete the string with the correct format character
    if DLib['ddtype'] in DtDict['c']: DTP=DTP+'c'
    elif DLib['ddtype'] in DtDict['b']: DTP=DTP+'b'
    elif DLib['ddtype'] in DtDict['x']: DTP=DTP+'x'
    elif DLib['ddtype'] in DtDict['h']: DTP=DTP+'h'
    elif DLib['ddtype'] in DtDict['H']: DTP=DTP+'H'
    elif DLib['ddtype'] in DtDict['i']: DTP=DTP+'i'
    elif DLib['ddtype'] in DtDict['I']: DTP=DTP+'I'
    elif DLib['ddtype'] in DtDict['l']: DTP=DTP+'l'
    elif DLib['ddtype'] in DtDict['L']: DTP=DTP+'L'
    elif DLib['ddtype'] in DtDict['q']: DTP=DTP+'q'
    elif DLib['ddtype'] in DtDict['Q']: DTP=DTP+'Q'
    elif DLib['ddtype'] in DtDict['f']: DTP=DTP+'f'
    elif DLib['ddtype'] in DtDict['d']: DTP=DTP+'d'
    else:
        print "No matching data type found in data type to format character dictionary. Assuming uint32. Did you use lower case?"
        DTP=DTP+'f'
    NBT=int(round(DLib['dsize'][0]*DLib['dsize'][1]))
    #determine how many bytes per character
    if DLib['ddtype'] in ByDict['one']: BPC=1
    elif DLib['ddtype'] in ByDict['two']: BPC=2
    elif DLib['ddtype'] in ByDict['four']: BPC=4
    elif DLib['ddtype'] in ByDict['eight']: BPC=8
    else:
        print "No bytesize match in dictionary found. Assuming 4 bytes per number (float32)"
        BPC=4
    fh=open(filename,'rb') #reopen the file for reading the binary data
    fh.seek(-(NBT*BPC),2)
    darr=zeros((int(DLib['dsize'][1]),int(DLib['dsize'][0])),dtype=DTP) #arrays in python are v*h or row/col ordered, but for some reason, this is the other way around.
    #read the data
    darr[:,:]=reshape(numpy.fromfile(file=fh,dtype=DTP,count=NBT),array([int(DLib['dsize'][1]),int(DLib['dsize'][0])]))
    fh.close()
    darr=imtransform(darr,DLib['dtransform']) #transform to the right orientation

    #here we fix shit done by some companies for no good reason
    if DLib['fixme'].lower()=='rigaku':
        #rigaku breaks the definition of the 16th bit in their image:
        #darr[darr>2**15]=32*(darr[darr>2**15]-2**15+2**15/32)
        #first move to 32 bits so we have the range we need
        darr=uint32(darr)
        #find out where the 16th bit is active
        di=(darr>=2**15)
        #and apply the magic Rigaku sprinkles.
        darr[di]=32*(darr[di]-2**15+1024)

    return darr

def imp_gccal(mfname,calfname,Qlimits=[-inf,inf],Psilimits=[-inf,inf],OutputI=False):
    #two file names must be supplied, here, a glassy carbon 2D measurement file (corrected or background subtracted), and a 1D calibration file (.dat). The calibration file is supplied in absolute units by the calibrator of the glassy carbon sample (usually Jan Ilavsky). 
    #this function runs after correction and background subtraction of the measurement, interpolates the glassy carbon data onto the 2D Q,PSI grid of the measurement, and scales the intensity to compare. 
    #eventually, when there is such a need, I will be working on supporting 1D measurement inputs as well. (i.e. when the BH camera is operational)

    if '.imp' in mfname:
        DLib,darr=readimp(mfname)
    else: 
        DLib,darr=readdat(mfname)
    #read the calibration file (should be impossible project .dat-format, i.e. add a header and move the units to SI units where necessary. Q should be in reciprocal meters, absolute units in reciprocal meters) 
    if '.imp' in calfname:
        calDLib,caldarr=readimp(calfname)
    else:
        calDLib,caldarr=readdat(calfname)

    #check the DLib first:
    if not(check_DLib(DLib)):
        print "DLib incorrect, cannot proceed with GC calibration until DLib is valid"
        return
    if not(check_DLib(calDLib)):
        print "calDLib incorrect, cannot proceed with GC calibration until calDLib is valid"
        return
    #extract variables from darr
    mData=imp_darr2vars(DLib,darr)
    calData=imp_darr2vars(calDLib,caldarr) 
    
    #get what we need
    Q=mData['Q']
    if 'PSI' in mData:
        PSI=mData['PSI']
    I=mData['I']
    try:
        IERR=mData['IERR']
    except KeyError:
        IERR=mData['SE']

    calQ=calData['Q']
    calI=calData['I']
    calIERR=calData['SE']

    Qlimits=numpy.array([numpy.min(Qlimits),numpy.max(Qlimits)])
    #apply limits to within existing GC data and within supplied q- and psi limits
    Qlimits[0]=numpy.maximum(numpy.min(calQ),Qlimits[0])
    Qlimits[0]=numpy.maximum(numpy.min(Q),Qlimits[0])
    Qlimits[1]=numpy.minimum(numpy.max(calQ),Qlimits[1])
    Qlimits[1]=numpy.minimum(numpy.max(Q),Qlimits[1])
    if 'PSI' in mData:
        validbools=(Q>=Qlimits[0])&(Q<=Qlimits[1])&(PSI>=Psilimits[0])&(PSI<=Psilimits[1])
    else:
        validbools=(Q>=Qlimits[0])&(Q<=Qlimits[1])

    I=I[validbools]
    if ('IERR' in locals()):
        IERR=IERR[validbools]
    else:
        #assume errors
        IERR=sqrt(numpy.maximum(I,numpy.min(I[I!=0])))

    #foncify errors, make sure there are no zero-values in the errors
    IERR[IERR==0]=numpy.min(IERR[IERR!=0])
    calIERR[calIERR==0]=numpy.min(calIERR[calIERR!=0])

    Q=Q[validbools]

    #set up the (linear) interpolation from caldata onto mdata
    If=scipy.interpolate.interp1d(calQ,calI)
    Ef=scipy.interpolate.interp1d(calQ,calIERR)
    
    #determine calibration factor and background level, requires saxstools.py
    if OutputI:
        Sc,Cv,Ifit=Iopt_v1(If(Q),I,IERR+Ef(Q),[1.,.1],OutputI=True) #Iopt_v1 more robust but slower than Iopt
    else:
        Sc,Cv=Iopt_v1(If(Q),I,IERR+Ef(Q),[1.,.1])

    #invert as we are trying to get a correction factor, not the scaling of the GC itself.
    #Sc[0]=1/Sc[0]#intensity scaling factor
    #Sc[1]=-Sc[1]#background level
    if OutputI:
        return Sc,Q,If(Q) #returns glassy carbon data, scale intensity accordingly.
    else:
        return Sc

def imp_darr2vars(DLib,darr):
    #returns a dictionary with all information present in the channels in darr, and additional Q and PSI matrices either or not overwritten by the darr values. 
    #from imp_imgint. If this works, we can use it there too. 
    Odict=dict()

    #for 2D arrays
    if ndim(darr)==3:
        imsize=array([int(DLib['size'][0]),int(DLib['size'][1])]) #redefine as ints.
        if DLib['droffset']=='': #a more proper property-value checking must take place
            DLib['droffset']=[]
        Q,PSI=qpsi_2D(imsize,DLib['droffset'],DLib['wlength'],DLib['plength'],DLib['clength'],DLib['pmid']) #this function is defined in saxstools.py
        Odict['Q']=Q
        Odict['PSI']=PSI
        #read the channels. Valid channels are I,IERR,Q,QERR,PSI,PSIERR,MASK
        VList=['I','SE','IERR','Q','QERR','PSI','PSIERR','MASK'] #valid list
        for channeli in range(DLib['channels']):
            Chn=DLib['channel'+str(channeli+1)]
            if Chn.isalpha() & (Chn in VList): #basic input checking, must only contain alpha
                exec 'Odict[\'{0}\']=darr[:,:,{1}]'.format(Chn,int(channeli)) #the one case where exec is actually useful?
    elif ndim(darr)==2:
        #1D datasets always contain q, so we just process the rest
        VList=['I','SE','IERR','Q','QERR','PSI','PSIERR','MASK'] #valid list
        for columni in range(DLib['columns']):
            Chn=DLib['column'+str(columni+1)]
            if Chn.isalpha() & (Chn in VList): #basic input checking, must only contain alpha
                exec 'Odict[\'{0}\']=darr[:,{1}]'.format(Chn,int(columni)) #the one case where exec is actually useful?
        
    else:
        print('Data array darr does not have 2 or 3 dimensions, please check')
        return
    
    return Odict
    #MILD VARIABLE TESTS
    #test if variable exists
    #if ('I' in locals())==False:
    #    print "Missing intensity in the file. Cannot possibly continue."
    #    return
    #check sizes
    #if (size(I,0)!=(size(I,0)+size(Q,0)+size(PSI,0))/3) | (size(I,1)!=(size(I,1)+size(Q,1)+size(PSI,1))/3):
    #    print "Mismatch in sizes of I, Q and/or PSI. cannot continue"
    #    return
    #we assume the sizes of the other matrices are correctly written and read

def imp_finddirect(DLib,Output=True): #use with a DLib for a direct beam center file
    #check the DLib first:
    if not(check_DLib(DLib)):
        print "DLib incorrect, cannot proceed with image correction until DLib is valid"
        return
    simage=imp_imageread(DLib['sfname'],DLib) 
    #raw image we use here. 
    #get an estimate for the peak position
    #   make index matrices corresponding to the horizontal and vertical indices
    hind=numpy.array(range(size(simage,1)))
    vind=numpy.array(range(size(simage,0)))[:,newaxis]+hind*0
    hind=hind+0*vind
    #   find maximum in image as estimate for peak position
    maxestvh=numpy.array(unravel_index(argmax(simage),shape(simage))) #in V,H!
    maxest=numpy.array((maxestvh[1],maxestvh[0]))
    #   roughly estimate the width of the peak in horizontal and vertical dimensions
    #maximg=simage.flatten()[argmax(simage)]
    maximg=numpy.max(simage)
    feh=numpy.maximum(1,sum(simage[maxest[1],:]>=0.5*maximg)) #there is a problem with NAN's in simage center 2012-07-02, 
    fev=numpy.maximum(1,sum(simage[:,maxest[0]]>=0.5*maximg))
    fwhmest=numpy.float32(numpy.array((feh,fev))) #was not automatically float, fixed 2012-07-07
    sigmaest=fwhmest/2*sqrt(2*log(2)) #convert FWHM to sigma for gaussian profiles
    #estimate background maybe? probably not necessary
    
    #we then fit a peak function to the data, using scipy.optimize.leastsq and the "csqr_directfit" function 
    
    #initialize Sc
    Sc=numpy.array((maximg,maxest[0],maxest[1],sigmaest[0],sigmaest[1],0.,numpy.min(simage))) #scaling, center estimate, width estimate, rotation, background estimate
    Sc,success=scipy.optimize.leastsq(csqr_directfit,Sc,args=(simage,hind,vind),full_output=0)
    print "direct beam center is at {0} horizontal, {1} vertical".format(Sc[1],Sc[2])
    print "direct beam Gaussian width (sigma) is {0} pixels horizontal, {1} vertical".format(Sc[3],Sc[4])
    print "direct beam peak height is {0}, background is {1}, rotation is {2}".format(Sc[0],Sc[6],Sc[5])
    if success!=1:
        print "optimization failed."
    else:
        print "optimization successful"
    if Output:
        return Sc
    else:
        return

def twod_gaussian(hind,vind,muh,muv,sigh,sigv,theta):
    #general function for a 2D gaussian (wikipedia)
    a=cos(theta)**2/(2*sigh**2)+sin(theta)**2/(2*sigv**2)
    b=-sin(2*theta)/(4*sigh**2)+sin(2*theta)/(4*sigv**2)
    c=sin(theta)**2/(2*sigh**2)+cos(theta)**2/(2*sigv**2)
    twodg=exp( -( a*(hind-muh)**2 +2*b*(hind-muh)*(vind-muv) +c*(vind-muv)**2 ) )
    return twodg

def csqr_directfit(Sc,I,hind,vind):
    #least-squares error for use with scipy.optimize.leastsq, finding the direct beam centre
    Scaling=Sc[0] #scaling factor
    muh=Sc[1] #horizontal mean
    muv=Sc[2]
    sigh=Sc[3] #horizontal sigma 
    sigv=Sc[4] #vertical sigma 
    theta=Sc[5] #gaussian cross-section rotation
    bgnd=Sc[6] #background
    # two dimensional gaussian:
    twodg=Scaling*twod_gaussian(hind,vind,muh,muv,sigh,sigv,theta)+bgnd
    E=sqrt(numpy.maximum(1,I))
    cs=(I-Scaling*twodg-bgnd)/E
    #print "Size E",size(E)
    #print "Sc: %f, %f" %(Sc[0],Sc[1])
    return cs.flatten()

def imp_imgint(filename,Force=False,Binning='weighted',DLib=[],BDLib=[]):
    #this file reads the filename and integrates using the parameters incorporated in the imp image. If an .imp file name is supplied which does not end in _bgs.imp, it is assumed the background subtraction has not yet been performed. This will then be done here, without writing a _bgs.imp file. Last imput parameters are an alternative DLibs for DLib and BDLib. If DLib=[your supplied library], this will be used instead of the data stored in the imp file. Same goes for BDLib, which applies to the background
    #IMP FILE READING
#    if '_bgs.imp' in filename: #this program does the background subtraction. 
#        ofname=filename.replace('.imp','.dat')
    if '_cor.imp' in filename:
        ofname=filename.replace('_cor.imp','_bgs.dat')
    else:
        ofname=filename.replace('.imp','_bgs.dat')

    #check whether we have already processed it:
    Kansas=os.getcwd()
    ListDir=os.listdir(Kansas)
    if (ofname in ListDir) and not(Force):
        print 'file already exists. not forcing (if you wish to force processing, declare Force=True as argument in input)'
        return

    if DLib==[]: #no DLib supplied 
        DLib,darr=readimp(filename) #output arguments are the library and the data array
        #parse and complete
        DLib=check_DLib(DLib,ReturnDLib=True,Verbose=True)
        print 'pong'
    else:
        DLb,darr=readimp(filename)
    imsize=array([int(DLib['size'][0]),int(DLib['size'][1])]) #redefine as ints.
    if DLib['droffset']=='': #a more proper property-value checking must take place
        DLib['droffset']=[]
    Q,PSI=qpsi_2D(imsize,DLib['droffset'],DLib['wlength'],DLib['plength'],DLib['clength'],DLib['pmid']) #this function is defined in saxstools.py
    #read the channels. Valid channels are I,IERR,Q,QERR,PSI,PSIERR,MASK
    VList=['I','IERR','Q','QERR','PSI','PSIERR','MASK'] #valid list
    for channeli in range(DLib['channels']):
        Chn=DLib['channel'+str(channeli+1)]
        if Chn.isalpha() & (Chn in VList): #basic input checking, must only contain alpha
            exec '%s=darr[:,:,%i]' %(Chn,channeli) #the one case where exec is actually useful?


    #MILD VARIABLE TESTS
    #test if variable exists
    if ('I' in locals())==False:
        print "Missing intensity in the file. Cannot possibly continue."
        return
    #check sizes
    if (size(I,0)!=(size(I,0)+size(Q,0)+size(PSI,0))/3) | (size(I,1)!=(size(I,1)+size(Q,1)+size(PSI,1))/3):
        print "Mismatch in sizes of I, Q and/or PSI. cannot continue"
        return
    #we assume the sizes of the other matrices are correctly written and read

    #optional background subtraction processing
    if ((DLib['bfname'])!='')and(not('.imp' in DLib['bfname'])):
        print 'background file must be the corrected .imp image filename, not subtracting now'
        DLib['bfname']=''
    if (DLib['bfname'])!='':
        print 'background not yet subtracted. Subtracting for your pleasure'
        print('background filename supplied, loading...')
        #IMP FILE READING
        if BDLib==[]:
            BDLib,Bdarr=readimp(DLib['bfname']) #output arguments are the library and the data array
        else:
            BDLb,Bdarr=readimp(DLib['bfname']) #output arguments are the library and the data array
        if BDLib['droffset']=='': #a more proper property-value checking must take place
            BDLib['droffset']=[]

        #do we need to redo the background processing? let's find out:
        redo=False
        #print "Bgnd thick {0} fgnd thick {1}".format(BDLib['thick'],DLib['thick'])
        if (BDLib['thick']!=DLib['thick'])and(DLib['bgfollowthick'].lower()=='true'):
            redo=True
            BDLib['thick']=DLib['thick']
            print 'background thickness value set to sample value, will reprocess background'
        if (BDLib['cfact']!=DLib['cfact'])and(DLib['bgfollowcfact'].lower()=='true'):
            redo=True
            BDLib['cfact']=DLib['cfact']
            print 'background calibration factor value set to sample value, will reprocess background'

        if redo:
            tofname=BDLib['sfname'].rsplit('.',1)[0]
            tofname=tofname+'_cor_temp.imp'
            imp_imagecorrect(BDLib,True,tofname)
            #reload background file
            BDLb,Bdarr=readimp(tofname) #output arguments are the library and the data array

        VList=['I','IERR','Q','QERR','PSI','PSIERR','MASK'] #valid list
        for channeli in range(BDLib['channels']):
            Chn=BDLib['channel'+str(channeli+1)]
            if Chn.isalpha() & (Chn in VList): #basic input checking, must only contain alpha
                exec '%s=Bdarr[:,:,%i]' %('B'+Chn,channeli) #the one case where exec is actually useful?
        #perform background subtraction
        I=I-BI
        if ('IERR' in locals()) and ('BIERR' in locals()):
            IERR=IERR+BIERR
        elif ('IERR' in locals()) or ('BIERR' in locals()):
            print 'Either the background or foreground comes with errors, but the other lacks errors, will use STD for error determination'
            if 'IERR' in locals():
                del IERR
            else:
                del BIERR
    else:
        print 'no background file specified'

    #MASKING
    #mask image
    if (DLib['mfname']!='')&(DLib['mwindow']!=[])&(size(DLib['mwindow'])==2):
        #Mimage=transpose(imread(DLib['mfname'])) #has to be transposed first
        #Mimage=(imread(DLib['mfname'])) #has to be transposed first
        #Mimage=imtransform(Mimage,DLib['dtransform']) #transform to the right raster orientation
        Mimage=imp_imageread(DLib['mfname'],DLib)
        if (size(Mimage,0)!=size(I,0))|(size(Mimage,1)!=size(I,1)):
            print "Mismatch in size of Mask versus detector image. cannot continue"
            return
        mmask=(Mimage>=numpy.min(DLib['mwindow']))*(Mimage<=numpy.max(DLib['mwindow']))
    else:
        mmask=1 #no mask, just a factor
    #integration limits mask
    ilim=DLib['ilim']
    if DLib['ilim'][2]<DLib['ilim'][3]:
        lmask=(Q>ilim[0])*(Q<ilim[1])*(PSI>ilim[2])*(PSI<ilim[3])
    else:
        lmask=(Q>ilim[0])*(Q<ilim[1])*((PSI>ilim[2])|(PSI<ilim[3]))
    #apply the masks
    if ('MASK' in locals()):
        Mask=MASK*lmask*mmask
    else:
        Mask=lmask*mmask
    #apply masks
    if 'IERR' in locals():
        Qs=Q[(Mask>0)]
        Is=I[(Mask>0)]
        Es=IERR[(Mask>0)]
        Psis=PSI[(Mask>0)]
    else:
        Qs=Q[(Mask>0)]
        Is=I[(Mask>0)]
        Psis=PSI[(Mask>0)]
        Es=[]

    if DLib['bgsoutput'].lower()=='true':
        print "saving unintegrated but background subtracted image"
        #write 2D unintegrated result to file
        #this function writes an imp-file from an array of 32-bit float numbers using native endianness. Data is written after a "#DATA" -tag. All items from DLib are written in the file as well.
        #DLib has to have the keys 'channels' with the number of channels, and 
        # 'channel1', 'channel2' etc, with the channel information identifiers  'I','Q','PSI','IERR','QERR','PSIERR' or 'MASK'
        DLib['channel1']='Q'
        DLib['channel2']='PSI'
        DLib['channel3']='I'
        DLib['channel4']='IERR'
        DLib['channel5']='MASK'
        DLib['channels']=5
        darr=Q[:,:,newaxis]
        darr=concatenate((darr,PSI[:,:,newaxis]),2)
        darr=concatenate((darr,I[:,:,newaxis]),2)
        darr=concatenate((darr,IERR[:,:,newaxis]),2)
        darr=concatenate((darr,MASK[:,:,newaxis]),2)
        tofname=ofname.replace('.dat','.imp')
        DLib['endian']='n'
        DLib['type']='float32'
        DLib['size']=array([size(darr,1),size(darr,0)])
        print "writing result to {0}".format(ofname)
        writeimp(DLib,darr,tofname)

    #return Q,I,PSI,Mask
    #BINNING
    #in case of azimuthal binning
    if DLib['idir'].lower()=='a':
        if ilim[2]>ilim[3]: #we are crossing the "date line"; 360->0 degrees, adjust psi to compensate
            Psis[Psis>180]=Psis[Psis>180]-360
        #bin away!    
        if Binning=='weighted':
            qbin,Ibin,SEbin=binning_weighted_1D(Psis,Is,Es,int(DLib['nbin']),Stats=DLib['etype']) #not quite qbin, but ends up in the same column in the file
        else:
            qbin,Ibin,SEbin=binning_1D(Psis,Is,Es,int(DLib['nbin']),Stats=DLib['etype']) #not quite qbin, but ends up in the same column in the file
    elif DLib['idir'].lower()=='2d':
        #modulo 180
        Psis=Psis%180
        
        if Binning!='weighted':
            print "unweighted 2D binning not implemented. consult your dealer"
            return
        if size(DLib['nbin'])==1:
            #return Qs,Psis,Is
            Qbin,DQbin,PSIbin,DPSIbin,Ibin,SEbin,STDbin=binning_halving_2D(Qs,Psis,Is,E=[],Nbins=int(DLib['nbin']),Stats='SE') #test function
            return
        else:
            qbin,psibin,Ibin,SEbin=binning_weighted_2D(Qs,Psis,Is,Es,int(DLib['nbin'][0]),int(DLib['nbin'][1]),Stats=DLib['etype'].lower()) 
            return

    else:
        if Binning=='weighted':
            qbin,Ibin,SEbin=binning_weighted_1D(Qs,Is,Es,int(DLib['nbin']),Stats=DLib['etype']) 
        else:
            qbin,Ibin,SEbin=binning_1D(Qs,Is,Es,int(DLib['nbin']),Stats=DLib['etype']) 

    if DLib['idir'].lower()!='2d':
        #WRITE RESULT TO FILE
        writedat(ofname,qbin,Ibin,SEbin,DLib)
    else:
        #write 2D result to file
        DLib['channel1']='qbin'
        DLib['channel2']='psibin'
        DLib['channel3']='Ibin'
        DLib['channel4']='SEbin'
        DLib['channels']=4
        darr=qbin[:,:,newaxis]
        darr=concatenate((darr,psibin[:,:,newaxis]),2)
        darr=concatenate((darr,Ibin[:,:,newaxis]),2)
        darr=concatenate((darr,SEbin[:,:,newaxis]),2)
        ofname=ofname.replace('.dat','_2Dbin.imp')
        DLib['endian']='n'
        DLib['type']='float32'
        DLib['size']=array([size(darr,1),size(darr,0)])
        print "writing result to {0}".format(ofname)
        writeimp(DLib,darr,ofname)
        #this function writes an imp-file from an array of 32-bit float numbers using native endianness. Data is written after a "#DATA" -tag. All items from DLib are written in the file as well.
        #DLib has to have the keys 'channels' with the number of channels, and 
        # 'channel1', 'channel2' etc, with the channel information identifiers  'I','Q','PSI','IERR','QERR','PSIERR' or 'MASK'

def prompt_DLib(DLib=''):
    #this can be used to cycle through all fields and fill in the values in the fields. Meant for test purposes only. A (partially) prefilled DLib can be used as a basis.
    DL,DLsize,DLlab,DLtyp,DLlims=default_DLib()
    print "Value input for DLib. Please enter multiple values separated by spaces, as (without ticks): '2 5 3 8'"
    if DLib=='':
        DLib=DL
    for DLkey in DLib.keys():
        if not(DLkey in DL.keys()): #not listed in the standard library, none of our concern
            continue
        validanswer=False
        while validanswer==False:
            if DLib[DLkey]=='':
                DefaultVal=DL[DLkey]
            else:
                DefaultVal=DLib[DLkey]
            print "Please enter a value for key '{0}' ({1}), 'E' for empty string, 'Q' to quit, or hit return for default: {2}".format(DLkey,DLlab[DLkey],DefaultVal)

            inpval=raw_input('[{0}]>'.format(DefaultVal))
            if inpval=='': #return
                DLib[DLkey]=DefaultVal
            elif inpval=='E': 
                DLib[DLkey]=''
            elif inpval=='Q':
                return DLib
            else:
                ###from readimpheader:
                #we may have a string, single number, or a set of numbers. Find out which:
                ParseString=inpval.strip() #remove heading and trailing whitespaces
                ParseString=re.sub(r'\s+',' ',ParseString)#remove multiple whitespaces
                ParseList=re.split('[ ,;]',ParseString) #numbers cannot have commas as decimal separators
                ArraySize=size(ParseList)
                if re.search('[a-d,f-z,A-D,F-Z]![inf,Inf,INF]',ParseString):
                    ParseStringIsNumber=False
                elif ArraySize==0:
                    #nothing to do, empty line
                    DLib[DLkey]=[]
                    continue
                elif ArraySize==1:
                    #single values only
                    ParseStringIsNumber=True #initial assumption
                    #one test if it is a string, is if it contains any of the following characters, but not inf, Inf or INF:
                    #try converting each element into a number
                    PLV=(str2num(ParseList[0]))
                    if PLV==[]: #one value is not a number, so we have a string
                        ParseStringIsNumber=False
                    else:
                        Value=PLV
                        DLib[DLkey]=Value
                        continue
        
                else:
                    Value=[]
                    ParseStringIsNumber=True #initial assumption
                    for Vi in range(ArraySize):
                        #try converting each element into a number
                        #print "list element",ParseList[Vi]
                        #print 'converted to a number: %f' %(num(ParseList[Vi]))
                        PLV=str2num(ParseList[Vi])
                        if PLV==[]: #one value is not a number, so we have a string
                            ParseStringIsNumber=False
                            break
                        else:
                            Value.append(PLV)
                            DLib[DLkey]=array(Value)
                            continue
        
                if ParseStringIsNumber==False: #we ahve a string instead
                    ParseString=inpval #we may have thrown away information by the replace of mutliple spaces, so we re-read the line 
                    Value=ParseString #nothing to do, is already a string
                    #will not detect cases of '1e5' or '1E5', but will also not detect strings with numbers in them
                    #store in library DLib
                    DLib[DLkey]=Value
            #end of while loop, check value
            validanswer=check_DLib(DLib,{DLkey})
            if validanswer==False:
                print "a valid answer must be supplied before moving to the next key"
    return DLib

def compare_DLib(DL1,DL2,chkkeys=[],IgnoreUniqueKeys=False,DefaultDLkeys=False,Verbose=False,SkipKeys=list()):
    #this function compares two DLib libraries, and returns True if identical, False if different. Optional value IgnoreUniqueKeys can be set to True to not return False when encountering a DLib key not present in the one or the other.
    #DefaultDLkeys can be set true, in which case it only checks keys listed in the default DLib, but no custom keys
    #Verbose can be set to True for detailed output
    mismatchfound=False
    keylist=DL1.keys()
    if DefaultDLkeys:
        DL,DLsize,DLlab,DLtyp,DLlims=default_DLib()
        keylist=DL.keys()
    if len(SkipKeys)!=0:
        #we have keys to skip:
        for key in SkipKeys:
            if key in keylist:
                keylist.remove(key)
    for DLkey in keylist:
        if Verbose:
            print "checking key {0}".format(DLkey)
        if not DLkey in DL2.keys():
            if not IgnoreUniqueKeys:
                if Verbose:
                    print "missing key in comparison between DLibs: {0}".format(DLkey)
                return False
            else:
                if Verbose:
                    print "missing key in comparison between DLibs: {0}, skipping...".format(DLkey)
                continue
        if isinstance(DL1[DLkey],(str,int,float,numpy.float32)):
            if DL1[DLkey]!=DL2[DLkey]:
                if Verbose:
                    print "Key {0} does not match between DLibs, values {1} and {2}, mismatch found".format(DLkey,DL1[DLkey],DL2[DLkey])
                mismatchfound=True
        elif isinstance(DL1[DLkey],numpy.ndarray):
            if sum(DL1[DLkey]!=DL2[DLkey])>0:
                if Verbose:
                    print "Key {0} does not match between DLibs, values {1} and {2}, mismatch found".format(DLkey,DL1[DLkey],DL2[DLkey])
                mismatchfound=True
        else:
            print "class {0} of DLkey {1} not supported by compare_DLib subroutine, skipping".format(DL1[DLkey].__class__,DLkey)

    if mismatchfound==False:
        if Verbose:
            print "DLib comparison passed"
        return True
    else:
        if Verbose:
            print "DLib comparison failed"
        return False
            
    
        
def default_DLib():
    #this function defines the default entries in DLib. Additionally, type, size, bounds and description libraries are supplied for each DLib entry. They may be used for other purposes such as checking DLib validity.
    #if there is a value in the default library, it is assumed that this value is necessary for the calculations. If thevalue does not exist in the DLib-to-be-checked, the default value is substituted
    #EMPTY FIELDS MUST HAVE '' IN THE DEFAULT LIBRARY

    DLib={
        #here, default DLib values are supplied
        'name':'', #sample identifier
        'sfname':'', #filename
        'ststamp':'', #julian day
        'styp':'sample', #measurement type
        'ssac':'', #sample self-absorption correction, can be set to "plate", only geometry implemented
        'mtime':1., #seconds
        'tfact':1., #0-1
        'thick':.001, #meters
        'sflux':1., #a.u.
        'notes':'', #string
        'bname':'', #background identifier
        'bfname':'', #background filename (must be .imp)
        'bgfollowthick':'True', #background "thickness" follows value for sample
        'bgfollowcfact':'False', #background calibration factor follows cfact value of sample
        'bgsoutput':'False', #background subtracted unintegrated image storage
        'cname':'', #calibration sample identifier
        'cfact':1., #a.u.
        'cfname':'', #calibrated data filename (the one in absolute units), only used if this is the glassy carbon measurement and you want to determine the calibration factor
        'dname':'', #detector name
        'dsize':numpy.array([1024,1024]), #h,v, pixels
        'ddtype':'uint32', #f.ex. uint32
        'dendian':'n', #'n'
        'dtransform':'1', #1-8, int
        'dwindow':numpy.array([-inf,inf]), #window in between which raw detector values are considered 'valid' for excluding dead pixels pinned to max or min values 
        'fixme':'', #fixes for particular peculiarities in image formats by some companies
        'mfname':'', #mask filename, identical size as sample, standard image format
        'mwindow':numpy.array([-inf,inf]), #2-element a.u. mask window to go with the PNG mfname
        'fffname':'', #filename, flatfield
        'dcfname':'', #darkcurrent filename
        'dccount':'', #darkcurrent detected counts, supersedes filename.
        'dcnpix':1., #number of pixels over which dccount has been detected, defaults to dsize 
        'dcmtime':1., #seconds, should be close to mtime
        'dctimedep':True, #darkcurrent is time dependent
        'dmapfname':'', #distortion map filename (not yet implemented)
        'gammafname':'', #gamma curve correction (not yet implemented)
        'clength':3, #meters, camera length
        'wlength':1.54e-10, #meters, wavelength
        'plength':numpy.array([174e-6,174e-6]), #2-element meters, pixel size h, v
        'pmid':numpy.array([512,512]), #2-element pixels, beam center
        'poni':'', #2-element pixels, point of normal incidence (not yet implemented)
        'droffset':0, #degrees, rotation offset for detectors rotated around the beam axis
        'nbin':200, #integer, number of bins
        'ilim':numpy.array([-inf,inf,-inf,inf]), #4-element, qmin qmax, psimin, psimax, integration limits
        'idir':'r', #'r' (radial) 'a' azimuthal or '2d' for 2D binning (beta)
        'pcor':0.5, #0-1 in-plane polarization factor, should be 0.5 for nonpolarized beams, 0.95 for most synchrotrons
        'pangle':0, #polarization angle, usually 0, but may be tilted for k-b mirrors
        'etype':'STD', #error calculation type 'auto' recommended for photon-counting detectors, 'STD' for non-photon-counting
        }
    DLib_size={
        #here, default DLib values are supplied
        'name':'', #sample identifier
        'sfname':'', #filename
        'ststamp':'', #julian day
        'styp':'', #sample type
        'ssac':'', #sample self-absorption correction, can be set to "plate", only geometry implemented
        'mtime':1, #seconds
        'tfact':1, #0-1
        'thick':1, #meters
        'sflux':1, #a.u.
        'notes':'', #string
        'bname':'', #background identifier
        'bfname':'', #background filename (must be .imp)
        'bgfollowthick':'', #background "thickness" follows value for sample
        'bgfollowcfact':'', #background calibration factor follows cfact value of sample
        'bgsoutput':'', #background subtracted unintegrated image storage
        'cname':'', #calibration sample identifier
        'cfact':1, #a.u.
        'cfname':'', #calibration filename
        'dname':'', #detector name
        'dsize':2, #h,v, pixels
        'ddtype':'', #f.ex. uint32
        'dendian':'', #'n'
        'dtransform':1, #1-8, int
        'dwindow':2,#window in between which raw detector output is considered "true"
        'fixme':'', #fixes for particular peculiarities in image formats by some companies
        'mfname':'', #mask filename, identical size as sample, standard image format
        'mwindow':2, #2-element a.u. mask window, applied to the mask image
        'fffname':'', #filename, flatfield
        'dcfname':'', #darkcurrent filename
        'dccount':'', #darkcurrent detected counts, supersedes filename.
        'dcnpix':1, #number of pixels over which dccount has been detected, defaults to dsize 
        'dcmtime':1, #seconds, should be close to mtime
        'dctimedep':'', #darkcurrent is time dependent
        'dmapfname':'', #distortion map filename (not yet implemented)
        'gammafname':'', #gamma curve correction (not yet implemented)
        'clength':1, #meters, camera length
        'wlength':1, #meters, wavelength
        'plength':2, #2-element meters, pixel size h, v
        'pmid':2, #2-element pixels, beam center
        'poni':2, #2-element pixels, point of normal incidence (not yet implemented)
        'droffset':1, #degrees, rotation offset for detectors rotated around the beam axis
        'nbin':'', #integer, number of bins, can be one element for 1D binning, or two for number of bins in q, psi
        'ilim':4, #4-element, qmin qmax, psimin, psimax, integration limits
        'idir':'', #'r' (radial) 'a' azimuthalor '2d' for 2D binning (beta)
        'pcor':1, #0-1 in-plane polarization factor, should be 0.5 for nonpolarized beams, 0.95 for most synchrotrons
        'pangle':1, #polarization angle, usually 0
        'etype':'', #error calculation type 'auto' recommended for photon-counting detectors, 'STD' for non-photon-counting
        }
    DLib_labels={
        #these labels indicate the meaning of the keywords/Fieldnames
        'name':'Sample name',
        'sfname':'Sample filename',
        'ststamp':'Sample file timestamp',
        'styp':'Sample type, "directbeam", "darkcurrent", "ringcal", "gccal", "bgnd","background","sample","I0","I1"',
        'ssac':'sample self-absorption correction, only "plate" implemented',
        'mtime':'Measurement duration (s)',
        'tfact':'Sample measurement transmission factor (0-1)',
        'thick':'Sample thickness (m)',
        'sflux':'Sample incoming flux (A.U.)',
        'notes':'Freeform notes field',
        'bname':'Background name',
        'bfname':'Background filename',
        'bgfollowthick':'background "thickness" follows value for sample ("True" or "False")',
        'bgfollowcfact':'background calibration factor follows cfact value of sample ("True" or "False")',
        'bgsoutput':'background subtracted unintegrated image storage("True" or "False")',
        'cname':'Calibration factor name',
        'cfact':'Calibration factor',
        'cfname':'Calibration filename with calibrated data to compare against, only used when calculating the calibration factor using this measurement. Should be in impossble 1D .dat format',
        'dname':'Detector name',
        'dsize':'Detector size (pixels horizontal, pixels vertical)',
        'ddtype':'Detector datatype (f.ex. "uint32")',
        'dendian':'Detector Endianness ("b" for big endian, "l" for little endian, "n" for native)',
        'dtransform':'Detector image transformation (1-8 integer)',
        'dwindow':'window of raw detector output values in between which detector output is considered valid.',
        'fixme':'fixes for particular peculiarities in image formats by some companies, use Rigaku for their weird 16th-bit fix',
        'mfname':'mask filename, should be a PNG image of the *ORIGINAL* data image masked to min or max',
        'mwindow':'mask acceptance window (PNG image intensity units)',
        'fffname':'flatfield filename',
        'dcfname':'darkcurrent filename',
        'dccount':'darkcurrent detected counts OVER DETECTOR measured over time dcmtime, supersedes filename.',
        'dcnpix':'number of pixels over which dccount has been detected, defaults to prod(dsize)',
        'dcmtime':'darkcurrent measurement time',
        'dctimedep':'darkcurrent is time-dependent ("True") or not ("False")',
        'dmapfname':'distortion map filename (not implemented yet)',
        'gammafname':'intensity correction gamma curve filename',
        'clength':'camera length (m)',
        'wlength':'wavelength (m)',
        'plength':'pixel length (m)',
        'pmid':'beam center (pixels horiz. pixels vert)',
        'poni':'point of normal incidence (not implemented yet)',
        'droffset':'detector rotation offset (degrees clockwise from 12 o\' clock)',
        'nbin':'number of bins, one integer for 1d binning, two for 2d binning, directions q and psi respectively.',
        'ilim':'integration limits (qmin qmax psimin psimax)',
        'idir':'integration direction ("r" radial, "a" azimuthal, "2d" for two-dimensional)',
        'pcor':'Polarization factor in horizontal plane (0-1, 0.5 for no polarisation, 0.95 for most synchrotrons)',
        'pangle':'polarization angle, usually 0, but may be tilted for k-b mirrors',
        'etype':'Type of error calculation ("STD" standard deviation in bin, "Poisson" poisson (counting) statistics'
        }      
    DLib_type={
        #here, DLib data types are indicated. 'string' for strings, 'fname' for filenames (will be checked for existence), 'float' for floating-point numbers and 'int' for integers
        'name':'string', #sample identifier
        'sfname':'fname', #filename
        'ststamp':'float', #julian day
        'styp':'string', #sample type
        'ssac':'string', #sample self-absorption correction, can be set to "plate", only geometry implemented
        'mtime':'float', #seconds
        'tfact':'float', #0-1
        'thick':'float', #meters
        'sflux':'float', #a.u.
        'notes':'string', #string
        'bname':'string', #background identifier
        #'bfname':'fname', #background filename (must be .imp)
        'bfname':'string', #background filename, can now be an identifier (measID) pointing to another measurement
        'bgfollowthick':'string', #background "thickness" follows value for sample
        'bgfollowcfact':'string', #background calibration factor follows cfact value of sample
        'bgsoutput':'string', #background subtracted unintegrated image storage
        'cname':'string', #calibration sample identifier
        'cfact':'float', #a.u.
        'cfname':'fname',
        'dname':'string', #detector name
        'dsize':'int', #h,v, pixels
        'ddtype':'string', #f.ex. uint32
        'dendian':'string', #'n'
        'dtransform':'int', #1-8, int
        'dwindow':'float',
        'fixme':'string',
        'mfname':'fname', #mask filename, identical size as sample, standard image format
        'mwindow':'float', #2-element a.u. mask window, for excluding dead pixels pinned to max or min values 
        'fffname':'fname', #filename, flatfield
        'dcfname':'fname', #darkcurrent filename
        'dccount':'float', #could be integer, but not necessarily
        'dcnpix':'float', #an integer number of pixels, only used for dccount
        'dcmtime':'float', #seconds, should be close to mtime
        'dctimedep':'string', #
        'dmapfname':'fname', #distortion map filename (not yet implemented)
        'gammafname':'fname', #gamma curve correction (not yet implemented)
        'clength':'float', #meters, camera length
        'wlength':'float', #meters, wavelength
        'plength':'float', #2-element meters, pixel size h, v
        'pmid':'float', #2-element pixels, beam center
        'poni':'float', #2-element pixels, point of normal incidence (not yet implemented)
        'droffset':'float', #degrees, rotation offset for detectors rotated around the beam axis
        'nbin':'int', #integer, number of bins
        'ilim':'float', #4-element, qmin qmax, psimin, psimax, integration limits
        'idir':'string', #'r' (radial) 'a' azimuthal, '2d' for 2D binning
        'pcor':'float', #0-1 in-plane polarization factor, should be 0.5 for nonpolarized beams, 0.95 for most synchrotrons
        'pangle':'float', #polarization angle, usually 0, but may be tilted for k-b mirrors
        'etype':'string', #error calculation type 'auto' recommended for photon-counting detectors, 'STD' for non-photon-counting
        }
    DLib_lims={
        #here, the upper and lower limits of each int/float is given, as well as the allowed strings in case these are limited
        'name':'', #sample identifier
        'sfname':'', #filename
        'ststamp':numpy.array([0,inf]), #julian day
        'styp':{'directbeam','darkcurrent','ringcal','gccal','bgnd','sample'}, #sample type
        'ssac':'', #sample self-absorption correction, can be set to "plate", only geometry implemented
        'mtime':numpy.array([0,inf]), #seconds
        'tfact':numpy.array([0,inf]), #0-1
        'thick':numpy.array([0,inf]), #meters
        'sflux':numpy.array([0,inf]), #a.u.
        'notes':'', #string
        'bname':'', #background identifier
        'bfname':'', #background filename (must be .imp)
        'bgfollowthick':['True','False'], 
        'bgfollowcfact':['True','False'],
        'bgsoutput':['True','False'],
        'cname':'', #calibration sample identifier
        'cfact':numpy.array([0,inf]), #a.u.
        'cfname':'', #calibration filename (must be .dat)
        'dname':'', #detector name
        'dsize':numpy.array([0,inf,0,inf]), #h,v, pixels
        'ddtype':['char','uchar','signed char','schar','unsigned char','uint8','int8','char*1','integer*1','short','unsigned short','ushort','uint16','int16','integer*2','int','unsigned integer','uint','integer*3','single','real*4','long','unsigned long','float','float32','uint32','int32','long long','unsigned long long','double','float64','uint64','int64','integer*4','real*8','x','c','b','h','H','i','I','l','L','q','Q','f','d'], #f.ex. uint32
        'dendian':['little','l','ieee-le','big','b','ieee-be','n','native'], #'n'
        'dtransform':numpy.array([1,8]), #1-8, int
        'dwindow':numpy.array([-inf,inf,-inf,inf]),
        'fixme':'',
        'mfname':'', #mask filename, identical size as sample, standard image format
        'mwindow':numpy.array([-inf,inf,-inf,inf]), #2-element a.u. mask window, for excluding dead pixels pinned to max or min values 
        'fffname':'', #filename, flatfield
        'dcfname':'', #darkcurrent filename
        'dccount':numpy.array([-inf,inf]), #count number
        'dcnpix':numpy.array([1,inf]), #an integer number of pixels, only used for dccount
        'dcmtime':numpy.array([0,inf]), #seconds, should be close to mtime
        'dctimedep':['True','False'], #
        'dmapfname':'', #distortion map filename (not yet implemented)
        'gammafname':'', #gamma curve correction (not yet implemented)
        'clength':numpy.array([0,inf]), #meters, camera length
        'wlength':numpy.array([0,inf]), #meters, wavelength
        'plength':numpy.array([0,inf,0,inf]), #2-element meters, pixel size h, v
        'pmid':numpy.array([-inf,inf,-inf,inf]), #2-element pixels, beam center
        'poni':numpy.array([-inf,inf,-inf,inf]), #2-element pixels, point of normal incidence (not yet implemented)
        'droffset':numpy.array([-360,360]), #degrees, rotation offset for detectors rotated around the beam axis
        'nbin':numpy.array([1,inf,1,inf]), #integer, number of bins
        'ilim':numpy.array([0,inf,0,inf,-inf,inf,-inf,inf]), #4-element, qmin qmax, psimin, psimax, integration limits
        'idir':{'r','a','radial','azimuthal','2d','2D'}, #'r' (radial) 'a' azimuthal
        'pcor':numpy.array([0,1]), #0-1 in-plane polarization factor, should be 0.5 for nonpolarized beams, 0.95 for most synchrotrons
        'pangle':numpy.array([-360,360]), #polarization angle, usually 0, but may be tilted for k-b mirrors
        'etype':{'std','STD','auto','Auto','AUTO','Poisson','poisson'} #error calculation type 'auto' recommended for photon-counting detectors, 'STD' for non-photon-counting
        }
    return DLib,DLib_size,DLib_labels,DLib_type,DLib_lims 

def check_DLib(DLib,DLKeyList='',ReturnDLib=False,Verbose=False):
    #this function tests the contents of DLib to see if they are valid and agree with the types and limits set in default_DLib. Optional argument DLKeyList indicates the keys to be tested. If not supplied, all keys are tested.
    #if Verbose is False, non-critical warnings are not displayed
    DL,DLsize,DLlab,DLtyp,DLlims=default_DLib()
    #THERE SHOULD BE TESTS HERE OF INTERNAL CONSISTENCY OF EACH LIBRARY TO CHECK FOR PROGRAMMER ERRORS
    programmererrorsfound=0 #count the number of programmer errors encountered during parsing
    if DLKeyList=='':
        DLKeyList=DL.keys()
    for DLkey in DL.keys():
        #check if every library contains the fields of the default DLib
        if not(DLkey in DLsize):
            print "field '{0}' does not exist in DLsize".format(DLkey)
            programmererrorsfound+=1
        if not(DLkey in DLlab):
            print "field '{0}' does not exist in DLlab".format(DLkey)
            programmererrorsfound+=1
        if not(DLkey in DLtyp):
            print "field '{0}' does not exist in DLtyp".format(DLkey)
            programmererrorsfound+=1
        if not(DLkey in DLlims):
            print "field '{0}' does not exist in DLlims".format(DLkey)
            programmererrorsfound+=1
    if programmererrorsfound!=0:
        print "{0} programmer errors found, fix the libraries in default_DLib first".format(programmererrorsfound)
        return False

    #check if the data type listed in the DLtyp library is one of the four allowed
    for DLkey in DL.keys():
        if not(DLtyp[DLkey] in ('int','float','string','fname')):
            print "data type '{0}' not recognized by checker, from library field '{1}', should be one of 'int','float','string' or 'fname'.".format(DLtyp[DLkey],DLkey)
            programmererrorsfound+=1
        #check if the size of the limits listed for each key are twice that of the number of values for that key
        if DLsize[DLkey]!='':
            if size(DL)>DLsize[DLkey]:
                print "Size discrepancy between default DLib and DLsize for field '{0}'".format(DLKey)
                programmererrorsfound+=1
            if DLsize[DLkey]!=size(DLlims[DLkey])/2:
                print "Size discrepancy between DLsize and DLlims for field '{0}'\n DLlims should have one pair of values (upper and lower limits) for each value in DLib".format(DLkey)
                programmererrorsfound+=1
    if programmererrorsfound!=0:
        print "{0} programmer errors found, fix the libraries in default_DLib first".format(programmererrorsfound)
        return False

    errorsfound=0 #count the number of errors encountered during parsing
    warningsfound=0 #count the number of warnings encountered during parsing
    #check if a sample file has been given 
    if DLib['sfname']=='':
        print "Missing sample filename, cannot do a thing without that. Fieldname 'sfname'."
        errorsfound+=1

    #check the data types in DLib
    for DLkey in DLKeyList:
        if not(DLkey in DL.keys()):
            if Verbose:
                print "user supplied DLkey: '{0}', no check necessary/possible".format(DLkey)
            continue
        if not DLkey in DLib: #missing field
            print "missing field {0} in DLib, adding with default of {1}".format(DLkey,DL[DLkey])
            DLib[DLkey]=DL[DLkey]#initialize
        if DLib[DLkey]=='': #if the value is empty
            #...but there is a default value
            if DL[DLkey]!='':
                print "Missing value in DLib field '{0}', filling in default of {1}".format(DLkey,DL[DLkey])
                print "field '{0}' description: {1}".format(DLkey,DLlab[DLkey])
                DLib[DLkey]=DL[DLkey]
                warningsfound+=1
            else:
                continue #go to the next entry
        elif DLtyp[DLkey]=='string':
            if (DLlims[DLkey]!='')and(not(DLib[DLkey] in DLlims[DLkey])): #if there are limitations to the permitted strings
                print "value '{0}' in library field '{1}' not valid, should be one of: '{2}'".format(DLib[DLkey],DLkey,DLlims[DLkey])
                print "field '{0}' description: {1}".format(DLkey,DLlab[DLkey])
                errorsfound+=1
        elif DLtyp[DLkey]=='fname':
            #check if file exists
            if not(os.path.isfile(DLib[DLkey])):
                print "Field '{0}' should contain a filename, but this is not an existing file: '{1}'".format(DLkey,DLib[DLkey])
                print "field '{0}' description: '{1}'".format(DLkey,DLlab[DLkey])
                errorsfound+=1
        elif DLtyp[DLkey]=='int':
            if DLib[DLkey]!='':
                interror=False
                limerror=False
                if isinstance(DLib[DLkey],numpy.ndarray): #if it is an array, check each value separately
                    for ai in range(size(DLib[DLkey])):
                        if not(isinstance(DLib[DLkey][ai],(int,long))):
                            interror=True
                        if not((DLib[DLkey][ai]<=numpy.max(DLlims[DLkey][(2*ai):(2*ai+2)]))and(DLib[DLkey][ai]>=numpy.min(DLlims[DLkey][(2*ai):(2*ai+2)])) ):
                            limerror=True
                           
                else:
                    if not(isinstance(DLib[DLkey],(int,long))):
                        interror=True
                    if not((DLib[DLkey]<=numpy.max(DLlims[DLkey][0:2]))and(DLib[DLkey]>=numpy.min(DLlims[DLkey][0:2])) ):
                        limerror=True
                if interror:
                    print "At least one of these '{0}' in library field '{1}' is not an integer!".format(DLib[DLkey],DLkey)
                    print "field '{0}' description: '{1}'".format(DLkey,DLlab[DLkey])
                    errorsfound+=1
                if limerror:
                    print "At least one of these '{0}' in library field '{1}' exceeds its limits. Upper and lower limits for each value are:'{2}'".format(DLib[DLkey],DLkey,DLlims[DLkey])
                    print "field '{0}' description: '{1}'".format(DLkey,DLlab[DLkey])
                    errorsfound+=1


            #check if it is of class integer and if its fields are within limits, and if its size is correct
        elif DLtyp[DLkey]=='float': 
            #check if it is of class float or int and if the fields are within limits, and if its size is correct
            if DLib[DLkey]!='':
                floaterror=False
                limerror=False
                if isinstance(DLib[DLkey],numpy.ndarray): #if it is an array, check each value separately
                    for ai in range(size(DLib[DLkey])):
                        if not(isnumber(DLib[DLkey][ai])): #should there be more classes here?
                            floaterror=True
                        if not((DLib[DLkey][ai]<=numpy.max(DLlims[DLkey][(2*ai):(2*ai+2)]))and(DLib[DLkey][ai]>=numpy.min(DLlims[DLkey][(2*ai):(2*ai+2)])) ):
                            limerror=True
                else:
                    if not(isnumber(DLib[DLkey])):
                        floaterror=True
                    if not((DLib[DLkey]<=numpy.max(DLlims[DLkey][0:2]))and(DLib[DLkey]>=numpy.min(DLlims[DLkey][0:2])) ):
                        limerror=True
                if floaterror:
                    print "At least one of these '{0}' in library field '{1}' is not a number!".format(DLib[DLkey],DLkey)
                    print "field '{0}' description: '{1}'".format(DLkey,DLlab[DLkey])
                    errorsfound+=1
                if limerror:
                    print "At least one of these '{0}' in library field '{1}' exceeds its limits. Upper and lower limits for each value are:'{2}'".format(DLib[DLkey],DLkey,DLlims[DLkey])
                    print "field '{0}' description: '{1}'".format(DLkey,DLlab[DLkey])
                    errorsfound+=1

        else:
            print "Error for programmer: data type '{0}' not recognized by checker, from library field '{1}'".format(DLtyp[DLkey],DLkey)
            return False
    if ReturnDLib:
        return DLib
    if errorsfound==0:
        return True
    else:
        print "{0} errors found".format(errorsfound)
        return False
    
def isnumber(x):
    #simple test for numberishness
    try:
        x+1
        return True
    except TypeError:
        return False

def writedat(filename,Q,I,SE,DLib):
    #early inflexible version for writing binned data to files.
    fh=open(filename,'w')
    #header
    fh.write('Impossible project binned data file, UTF-8, unix-style LF EOL character, data in floating point (engineering), units are SI units, e.g. q in inverse meters.\n')
    fh.write('columns=3\n')
    fh.write('column1=Q\n')
    fh.write('column2=I\n')
    fh.write('column3=SE\n') #standard error
    for DLkey in DLib.keys():
        ostr=str(DLib[DLkey])
        ostr=ostr.replace('[','') #remove brackets
        ostr=ostr.replace(']','')
        fh.write('{0}={1}\n'.format(DLkey,ostr))
    fh.write('#DATA\n')
    for ri in range(size(Q)):
        fh.write('{0};{1};{2}\n'.format(Q[ri],I[ri],SE[ri]))

    fh.close()


def imtransform(Img,Tnum):
    #image transformation following P. Boesecke's raster orientation schema. Tnum can range from 1-8.
    if not(isnumber(Tnum)):
        Tnum=str2num(Tnum)
    if Tnum>4: 
        Img=transpose(Img) 
        Tnum=9-Tnum
    if Tnum==1: return Img 
    if Tnum==2: return fliplr(Img)
    if Tnum==3: return flipud(Img)
    if Tnum==4: return fliplr(flipud(Img))

def averimps(filelist):
    #behaves like readimps, but delivers the average without taking up too much memory. Useful for large series of files. Averages all the channels and outputs settings library DLib of the last file and an averaged data array darr
    DLib,darr=readimp(filelist[0])
    dout=zeros(array([size(darr,0),size(darr,1),size(darr,2)]))
    #loop over channels
    for Ci in range(DLib['channels']):
        #check for error Channel, because they need to be added differently
        if DLib['channel'+str(Ci+1)]=='IERR':
            Echan=Ci

    if 'IERR' in DLib.viewvalues():
        print 'Error channel found, channel number {0}. properly propagating errors. Assuming the error channel number is constant over all measurements'.format(Echan+1)
    Nf=0        
    for filename in filelist:
        DLib,darr=readimp(filename)
        #loop over channels
        for Ci in range(DLib['channels']):
            #check for error Channel, because they need to be added differently
            if DLib['channel'+str(Ci+1)]=='IERR':
                if Echan!=Ci:
                    print 'Error: channel number of error channel is not identical for all measurements! Error channel for sample {0} is {1}, whereas it should be {2}'.format(filename,Ci+1,Echan+1)
                    return
                dout[:,:,Ci]=dout[:,:,Ci]+darr[:,:,Ci]**2
            else:
                dout[:,:,Ci]=dout[:,:,Ci]+darr[:,:,Ci]
        Nf+=1
    
    #loop over channels for the final averaging
    for Ci in range(DLib['channels']):
        #check for error Channel, because they need to be added differently
        if Ci==Echan:
            dout[:,:,Ci]=sqrt(dout[:,:,Ci])/Nf
        else:
            dout[:,:,Ci]=dout[:,:,Ci]/Nf

    return DLib,dout
    

def readimps(filelist,Channel='I'):
    #reads a sequence of files, and outputs only the matrix in the first channel or the indicated (zero-based) channel. i.e. Channel=1 reads channel2, Channel=3 reads channel4, Channel=0 reads channel1. Alternatively, Channel can be a string identifying the channel to be output, for example Channel='IERR' will output the intensity error channels (if available). Channel='I' is default. Output is a multidimensional matrix which can be averaged in the third dimension
    #a file list can be provided by, for example, running:
    #-->
    #flist=[]
    #for fi in arange(1,11):
    #    flist.append('Air_No_Cap_'+format(fi,'05d')+'_cor.imp')
    #<--
    #flist will then contain your file list
    #test read to check size of output


    DLib,darr=readimp(filelist[0])
    dout=zeros(array([size(darr,0),size(darr,1),size(filelist)]))
    Nf=0        
    for filename in filelist:
        DLib,darr=readimp(filename)
        #check Channel
        if isinstance(Channel,str):
            if not Channel in DLib.viewvalues():
                print 'Channel {0} not available in file {1}. Aborting...'.format(Channel,filename)
                return
            Ci=[k for k, v in DLib.iteritems() if v == Channel][0] #adapted from a line by Ene Uran
            Ci=re.sub('channel','',Ci)
            Ci=str2num(Ci)-1
        else:
            Ci=Channel

        dout[:,:,Nf]=darr[:,:,Ci]
        Nf+=1

    return dout



def readimp(filename):
    EnDict=dict() #dictionary with various ways of denoting the endianness
    EnDict['little']=['little','l','ieee-le']
    EnDict['big']=['big','b','ieee-be']
    EnDict['native']=['n','native']

    ByDict=dict() #dictionary with number of bytes per data type
    ByDict['one']=['char','uchar','signed char','schar','unsigned char','uint8','int8','char*1','integer*1']
    ByDict['two']=['short','unsigned short','ushort','uint16','int16','integer*2']
    ByDict['four']=['int','unsigned integer','uint','integer*3','single','real*4','long','unsigned long','float','float32','uint32','int32']
    ByDict['eight']=['long long','unsigned long long','double','float64','uint64','int64','integer*4','real*8']

    DtDict=dict() #dictionary translating data types into their Python format character
    DtDict['c']=['char','c']
    DtDict['b']=['schar','int8','signed char','integer*1','b']
    DtDict['x']=['uchar','unsigned char','uint8','x']
    DtDict['h']=['short','int16','integer*2','h']
    DtDict['H']=['ushort','unsigned short','uint16','H']
    DtDict['i']=['int','integer*3','signed integer','int32','i']
    DtDict['I']=['uint','uint32','unsigned integer','I']
    DtDict['l']=['long','l']
    DtDict['L']=['unsigned long','L']
    DtDict['q']=['long long','int64','integer*4','q']
    DtDict['Q']=['unsigned long long','uint64','Q']
    DtDict['f']=['float','float32','single','real*4','f']
    DtDict['d']=['double','float64','real*8','d']

    
    DLib,Datablock_start=readimpheader(filename)
    if DLib['endian'].lower() in EnDict['little']: DTP='<'
    elif DLib['endian'].lower() in EnDict['big']: DTP='>'
    elif DLib['endian'].lower() in EnDict['native']: DTP='='
    else:
        print "No endianness given, assuming native"
        DTP='='
    #now we complete the string with the correct format character
    if DLib['type'] in DtDict['c']: DTP=DTP+'c'
    elif DLib['type'] in DtDict['b']: DTP=DTP+'b'
    elif DLib['type'] in DtDict['x']: DTP=DTP+'x'
    elif DLib['type'] in DtDict['h']: DTP=DTP+'h'
    elif DLib['type'] in DtDict['H']: DTP=DTP+'H'
    elif DLib['type'] in DtDict['i']: DTP=DTP+'i'
    elif DLib['type'] in DtDict['I']: DTP=DTP+'I'
    elif DLib['type'] in DtDict['l']: DTP=DTP+'l'
    elif DLib['type'] in DtDict['L']: DTP=DTP+'L'
    elif DLib['type'] in DtDict['q']: DTP=DTP+'q'
    elif DLib['type'] in DtDict['Q']: DTP=DTP+'Q'
    elif DLib['type'] in DtDict['f']: DTP=DTP+'f'
    elif DLib['type'] in DtDict['d']: DTP=DTP+'d'
    else:
        print "No matching data type found in data type to format character dictionary. Assuming float32"
        DTP=DTP+'f'
    NBT=int(round(DLib['size'][0]*DLib['size'][1]))
    
    #determine how many characters per byte
    if DLib['type'] in ByDict['one']: charsize=1
    elif DLib['type'] in ByDict['two']: charsize=2
    elif DLib['type'] in ByDict['four']: charsize=4
    elif DLib['type'] in ByDict['eight']: charsize=8
    else:
        print "No bytesize match in dictionary found. Assuming 4 bytes per number (float32)"
        charsize=4
    #print 'charsize={0}'.format(charsize)
    
    fh=open(filename,'rb') #reopen the file for reading the binary data
    
    fh.seek(-NBT*charsize*int(DLib['channels']) ,2)
    darr=zeros((int(DLib['size'][1]),int(DLib['size'][0]),int(DLib['channels'])),dtype=DTP)
    #read the data
    #print 'NBT={0}, DTP={1}, size0={2}, size1={3}'.format(NBT,DTP,DLib['size'][0],DLib['size'][1])
    for Ci in range(int(DLib['channels'])):
        fdat=numpy.fromfile(file=fh,dtype=DTP,count=NBT)
        #print 'length of data block {0} is {1}'.format(Ci,shape(fdat))
        castinto=array([int(DLib['size'][0]),int(DLib['size'][1])])
        #print 'castinto: {0} {1}'.format(castinto[0],castinto[1])
        darr[:,:,Ci]=transpose(reshape(fdat,castinto))
    fh.close()
    return DLib,darr

def writeimp(DLib,darr,filename):
    #this function writes an imp-file from an array of 32-bit float numbers using native endianness. Data is written after a "#DATA" -tag. All items from DLib are written in the file as well.
    #DLib has to have the keys 'channels' with the number of channels, and 
    # 'channel1', 'channel2' etc, with the channel information identifiers  'I','Q','PSI','IERR','QERR','PSIERR' or 'MASK'
    fh=open(filename,'wb') #reopen the file for reading the binary data
    
    fh.write('Impossible project image data file, UTF-8, native endianness, 32-bit floating point, unix-style LF EOL character\n')
    if not 'endian' in DLib.viewkeys():
        DLib['endian']='n'
    if not 'type' in DLib.viewkeys():
        DLib['type']='float32'
    if not 'size' in DLib.viewkeys():
        DLib['size']=array([size(darr,1),size(darr,0)])

    for Ki in DLib.viewkeys():
        Kval=str(DLib[Ki])
        Kval=re.sub(r'\s+',' ',Kval)#remove multiple whitespaces
        Kval=re.sub(r'[\[,\]]','',Kval)#remove brackets
        fh.write('{0}={1}\n'.format(Ki,Kval))
    #write #data tag:
    fh.write('\n#DATA\n')

    #write the data
    if darr.dtype!=float32:
        print('data array not in 32-bit floating point, recasting as such')
        darr=numpy.float32(darr) #do not make it too big
    for Ci in range(int(DLib['channels'])):
        castinto=array([int(DLib['size'][0])*int(DLib['size'][1])])
        ndarray.tofile(reshape(transpose(darr[:,:,Ci]),castinto),fh)
        #darr[:,:,Ci]=transpose(reshape(numpy.fromfile(file=fh,dtype=DTP,count=NBT),array([int(DLib['size'][0]),int(DLib['size'][1])])))
    fh.close()

def readimpheader(filename,foffset=0):
    #this reads the library in the header and returns that library as well as the number of bytes from the file start where the datablocks begin
    import re #we need the regular expressions for the split using multiple possible separators
    fh=open(filename,'r')
    fh.seek(foffset)
    cpos=fh.tell()
    exitfound=False
    DLib=dict()#the dictionary with the parameter-value pairs from the header
    while not(exitfound):
        cpos=fh.tell()
        line=fh.readline()
        if '#DATA' in line:
            #this line is the last line of the header. Skip the rest.
            exitfound=1
            break
        elif cpos==fh.tell():
            #no more lines read
            exitfound=1
            break
        
        elif line.count('=')!=1:
            #skip this line as it does not contain any designations we need
            continue
        if line.endswith('\n'):
            #remove newline character before parsing
            line=line[:-1]        
        if line.endswith('\r'):
            #remove carriage return character before parsing
            line=line[:-1]        
        EqualsLoc=line.find('=')
        EntryName=line[0:EqualsLoc].strip()
        ParseString=line[(EqualsLoc+1):]
        #this string may be a string or a number or a set of numbers, including numbers in exponential notation such as '1e-5' or '-1E+10'.
        #so we try parsing the string as numbers:
        #we may have a single number, or a set of numbers. Find out which:
        ParseString=ParseString.strip() #remove leading and trailing whitespaces
        ParseString=re.sub(r'\s+',' ',ParseString)#remove multiple whitespaces
        ParseList=re.split('[ ,;]',ParseString) #numbers cannot have commas as decimal separators
        ArraySize=size(ParseList)
        if re.search('[a-d,f-z,A-D,F-Z]![inf,Inf,INF]',ParseString):
            ParseStringIsNumber=False
        elif ArraySize==0:
            #nothing to do, empty line
            DLib[EntryName]=[]
            continue
        elif ArraySize==1:
            #single values only
            ParseStringIsNumber=True #initial assumption
            #one test if it is a string, is if it contains any of the following characters, but not inf, Inf or INF:
            #try converting each element into a number
            PLV=(str2num(ParseList[0]))
            if PLV==[]: #one value is not a number, so we have a string
                ParseStringIsNumber=False
            else:
                Value=PLV
                DLib[EntryName]=Value
                continue

        else:
            Value=[]
            ParseStringIsNumber=True #initial assumption
            for Vi in range(ArraySize):
                #try converting each element into a number
                #print "list element",ParseList[Vi]
                #print 'converted to a number: %f' %(num(ParseList[Vi]))
                PLV=str2num(ParseList[Vi])
                if PLV==[]: #one value is not a number, so we have a string
                    ParseStringIsNumber=False
                    break
                else:
                    Value.append(PLV)
                    DLib[EntryName]=array(Value)
                    continue

        if ParseStringIsNumber==False: #we ahve a string instead
            ParseString=line[(EqualsLoc+1):] #we may have thrown away information by the replace of mutliple spaces, so we re-read the line 
            #but remove leading or trailing spaces
            ParseString=ParseString.strip()
            Value=ParseString #nothing to do, is already a string
            #will not detect cases of '1e5' or '1E5', but will also not detect strings with numbers in them
            #store in library DLib
            DLib[EntryName]=Value
    Datablock_start=fh.tell()    
    fh.close()
    return DLib,Datablock_start

def readdat(filename):
    DLib,datapos=readimpheader(filename)
    fh=open(filename)
    cpos=fh.tell()
    line=fh.readline()
    linei=1
    datafound=False
    while True: #will be identical when at EOF
        #parse line, search for beginning string
        if "DATA" in line:
            datafound=True
            break
        elif cpos==fh.tell(): #we have reached the end of file
            print "End of file reached without finding data"
            break
        else:
            cpos=fh.tell()
            line=fh.readline()#read next line
            linei+=1
            #print 'We are at position %d, line %d' %(cpos,linei)

    #when done with the while loop
    if datafound:
        pass
        #print 'The data starts at line %d' %linei
    elif ~datafound:
        print 'no data found, exiting'
        return
    fh.close()
    if not('columns' in DLib):
    #old style data, three columns only, q, intensity and standard deviation
        q,I,E=genfromtxt(filename,delimiter=';',skiprows=linei,autostrip=True,unpack=True)
        darr=zeros(array([size(q),4]))
        darr[:,0]=q
        darr[:,1]=I
        darr[:,2]=E
        #last column of darr is empty
    else:
        c1,c2,c3=genfromtxt(filename,delimiter=';',skiprows=linei,autostrip=True,unpack=True)
        VList=['I','Q','SE']
        for columni in range(DLib['columns']):
            Chn=DLib['column'+str(columni+1)]
            Cn='c'+str(columni+1)
            if Chn.isalpha() & (Chn in VList): #basic input checking, must only contain alpha
                exec '%s=%s' %(Chn,Cn) #the one case where exec is actually useful?
        darr=zeros(array([size(Q),3]))
        darr[:,0]=Q
        darr[:,1]=I
        darr[:,2]=SE
    return DLib,darr

def readdats():

    Kansas=os.getcwd()
    ListDir=os.listdir(Kansas)
    dati=0
    
    #find out how large q, I and E have to be    
    for filename in ListDir:
        if not ('.dat' in filename):
            #skip this one
            continue
        DLib,darr=readdat(filename)
        q,I,SE=darr[:,0],darr[:,1],darr[:,2]

        break #yea, this could be done more neatly with a while loop above
    #figure out how many files we have of the right type
    LDi=1
    for filename in ListDir:
        if not ('.dat' in filename):
            #skip this one
            continue
        LDi+=1 #add one to the length

    print 'There are %i files' %(LDi)
    sq=size(q);    
    ql=np.ndarray([LDi,sq])
    Il=np.ndarray([LDi,sq])
    SEl=np.ndarray([LDi,sq])
    for filename in ListDir:
        if not ('.dat' in filename):
            #skip this one
            continue
        #print filename
        DLib,darr=readdat(filename)
        ql[dati,0:sq],Il[dati,0:sq],SEl[dati,0:sq]=darr[:,0],darr[:,1],darr[:,2]
        dati+=1
    return ql,Il,SEl
    
def str2num(s):
    #robust interpretation of numbers
    if (numnum(s))==[]:
        return []
    elif abs(float(numnum(s)))==inf:
        return float(numnum(s)) #infinity cannot be cast as integer
    elif int(numnum(s))==float(numnum(s)): #is an integer
        return int(numnum(s))
    else:
        return float(numnum(s))
        
def numnum(s):
    try:
        return int(s)
    except ValueError:
        try:
            return float(s)
        except ValueError:
            return []
    



"""
    #now we parse the remainder of the file, assuming three columns of data separated by a semicolon
    cpos=fh.tell()
    line=fh.readline()#read next line
    datarray=np.ndarray(shape=[1,3])
    while (cpos<fh.tell()) and (line.count(';')==2): #end of file not reached and two semicolons on line
        [ql,il,el]=line.split(';')
        #strip leading and trailing spaces and convert to floats
        ql=float(ql.strip())
        il=float(il.strip())
        el=float(el.strip())
        
        print 'floats %f %f %f' %(ql,il,el)
        cpos=fh.tell()
        line=fh.readline()
"""

#close file
#fh.close();
            
    
