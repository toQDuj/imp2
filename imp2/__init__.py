#!/usr/bin/env python                                                          
#coding: utf8    

__author__ = "Brian R. Pauw"
__contact__ = "brian@stack.nl"
__license__ = "GPLv3+"
__copyright__ = "National Institute of Materials Science, Tsukuba, Japan"
__date__ = "2013-12-16"
__status__ = "alpha"
version = "0.0.1"

__all__ = []
# __all__ = ["cfg"]
# import cfg

# load finished modules here:
from factory import factory
from tools.AnisoMap import AnisoMap #for displaying anisotropicity maps
from tools.CurveComparator import CurveComparator #fits scaling factors
from modules.UncertaintyEstimator import UncertaintyEstimator # for ph. count.

# map some basic attribute links for information
# data = cfg.data
# parameters = cfg.parameters
# logEntry = cfg.data.logEntry

