import imp2 

# neat, but does not do what I intend:
# factory = type('imp', (), imp2.__dict__)
from impdata import impData
from correctionInfo import cInfo
from modules.correctionBase import correctionBase

class factory:
    # generates an instance of the imp2 modules
    def __init__(self):
        self.__dict__.update(imp2.__dict__)
        # initialise:
        self.data = impData()
        self.parameters = cInfo()
        # store handles to data and parameters in base class
        setattr(correctionBase, "data", self.data)
        setattr(correctionBase, "parameters", self.parameters)
        # to be depreciated:
        setattr(correctionBase, "par", self.parameters)
        # inherits the data and parameters stored in the base class:
        import modules.AU
        #from modules.AU import AU 
        import modules.BG
        import modules.DC
        import modules.DS
        import modules.FF
        import modules.FL
        import modules.Integrator
        import modules.MK
        import modules.QCalc
        import modules.QCalcVertCylDet
        import modules.Read1D
        import modules.Read2D
        import modules.SA
        import modules.SP
        import modules.TH
        import modules.TI
        import modules.TR
        import modules.UncertaintyEstimator
        import modules.Write1D
        import modules.Write2D 
        import modules.WritePNG 
        # convenience mappings
        self.AU = self.modules.AU.AU
        self.BG = self.modules.BG.BG
        self.DC = self.modules.DC.DC
        self.DS = self.modules.DS.DS
        self.FF = self.modules.FF.FF
        self.FL = self.modules.FL.FL
        self.Integrator = self.modules.Integrator.Integrator
        self.MK = self.modules.MK.MK
        self.QCalc = self.modules.QCalc.QCalc
        self.QCalcVertCylDet = self.modules.QCalcVertCylDet.QCalcVertCylDet
        self.Read1D = self.modules.Read1D.Read1D
        self.Read2D = self.modules.Read2D.Read2D
        self.SA = self.modules.SA.SA
        self.SP = self.modules.SP.SP
        self.TH = self.modules.TH.TH
        self.TI = self.modules.TI.TI
        self.TR = self.modules.TR.TR
        self.UncertaintyEstimator = self.modules.UncertaintyEstimator.UncertaintyEstimator
        self.Write1D = self.modules.Write1D.Write1D
        self.Write2D = self.modules.Write2D.Write2D
        self.WritePNG = self.modules.WritePNG.WritePNG

