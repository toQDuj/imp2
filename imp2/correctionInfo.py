#!/usr/bin/env python                                                          
#coding: utf8    

import logging, json
import numpy as np
from impparameter import factory as impParamFactory
import os
from inspect import getabsfile

class ExtendedEncoder(json.JSONEncoder):
    """JSON encoder extended to deal with Unicode"""
    def default(self, obj):
        if isinstance(obj, unicode):
            return str(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        elif obj == np.array or obj == np.ndarray :
            return "array"
        elif isinstance(obj, type):
            #return a suitable string
            if obj is float:
                return "float"
            elif obj is unicode:
                return "unicode"
            elif obj is str:
                return "str"
            elif obj is int:
                return "int"
            else: 
                return repr(obj)

        return json.JSONEncoder.default(self, obj)


class cInfo(object):
    """
    This class contains all the information required to read, verify and write
    correction configuration parameter files.
    """
    parameters = None # set externally
    logging.getLogger('cInfo')
    logging.basicConfig(level = logging.DEBUG)
    _paramDefFile = None
    parameterNames = None

    def __init__(self,**kwargs):
        """initialise the defaults and populate the database with values
        where appropriate
        default parameter file can be provided using kwarg:
        paramDefFile = 'path/to/file'
        """
        self.parameterNames = list()

        fname = kwargs.get("paramDefFile", None)
        if fname is None:
            #try one more:
            fname = getabsfile(impParamFactory)
            #settings should be in the same directory:
            fdir = os.path.split(fname)[0]
            fname = os.path.join(fdir, "sampleParameters.json")

        if not os.path.exists(fname):
            logging.error('no default parameter file found!')
            return false

        self.setParDefFile(fname)
        self.loadParams() 

    def loadParams(self, filename = None):
        """writes the default definitions and bounds for the configuration
        parameters to self.parameters
        Can also be used to update existing parameters from supplied fname"""
        if filename is None:
            filename = self.parDefFile()

        with open(filename, 'r') as jfile:
            parDict = json.load(jfile)
            logging.info('loading parameters from file: {}'.format(filename))

        if self.parameters is None:
            #create if it does not exist yet
            self.parameters = lambda: None

        #now we cast this information into the Parameter class:
        for kw in parDict.keys():
            if not kw in self.parameterNames:
                #make a new parameter
                self.parameterNames.append(kw)
                temp=impParamFactory(**parDict[kw])
                setattr(self.parameters,kw,temp)
                logging.info('successfully ingested parameter: {}'.format(kw))
            else:
                #update existing parameter
                self.set(kw,**parDict[kw])
                logging.info('successfully updated parameter: {}'.format(kw))

    def writeConfig(self, filename = None):
        """
        writes the configuration to a settings file. 
        Required input parameter is the filename to write to.
        """

        #create a dictionary containing one (sub-)dictionary per parameter
        parDict = dict()
        for kw in self.parameterNames:
            subDict = dict()
            par = self.getPar(kw)
            for aName in par.attributeNames():
                subDict[aName] = par.get(aName)

            parDict[kw] = subDict

        #write dictionary to file
        with open(filename,'wb') as jfile:
            json.dump(parDict, jfile, cls=ExtendedEncoder, indent = 4, sort_keys=True)

    def parseConfig(self):
        """
        Runs through the entire settings, raising warnings where necessary
        """
        for pn in self.parameterNames:
            pf = self.getPar(pn)
            if pf.value is None:
                continue #skip this value, has not yet been set
            pf.checkSize()
            pf.clipValue()
            
    def read(self, filename):
        """reads a settings file"""
        pass

    def setParDefFile(self, value):
        if os.path.exists(value):
            self._paramDefFile=value
        else:
            logging.warning('invalid path to parameter definitions file')
            return False
        return True
    
    def parDefFile(self):
        return self._paramDefFile

    def getPar(self, key):
        #returns the handle to the parameter defined by key or returns None
        #if it doesn't exist
        if key in self.parameterNames:
            return getattr(self.parameters, key)
        else:
            logging.warning(
                    'Could not find parameter {}. Define base parameter in parameter settings file first'.format(key)
                    )
            return None

    def setParVal(self, par, value):
        "shortcut method for setting the value of a parameter only"
        parhandle = self.getPar(par)
        parhandle.setValue(value)
    
    def getParVal(self, par):
        "shortcut method for getting the value of a parameter"
        parhandle = self.getPar(par)
        return parhandle.value()

    def getParSTD(self, par):
        "shortcut method for getting the relative uncertainty of a parameter"
        parhandle = self.getPar(par)
        return parhandle.valueSTD()

    def getParUnit(self, par):
        "shortcut method for getting the unit of a parameter"
        parhandle = self.getPar(par)
        return parhandle.unit()

    def set(self, par, **kwargs):
        #sets one or more parameter attributes 
        parhandle = self.getPar(par)
        
        for kw in kwargs:
            parhandle.set(kw, kwargs[kw])

