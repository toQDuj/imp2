#!/usr/bin/env python                                                          
#coding: utf-8
#
"""
Overview:
========
This tool finds the scaling factor to bring the second curve in line with the first.
The Q-values are expected to match

Required input arguments:
    *Q1*: Q-vector of the first dataset
    *I1*: intensity of the first dataset
    *E1*: relative intensity uncertainty of the first dataset
    *I2*: intensity of the second dataset
    *E2*: relative intensity uncertainty of the second dataset
Optional input arguments: 
    *backgroundFit*: Boolean indicating whether or not to fit the background,
        Default: True
"""

__author__ = "Brian R. Pauw"
__contact__ = "brian@stack.nl"
__license__ = "GPLv3+"
__date__ = "2014/03/19"
__status__ = "beta"

# correctionBase contains the __init__ function for these classes
from numpy import sqrt
import numpy as np
from scipy.interpolate import interp1d
from scipy.optimize import leastsq

def findScaling(I1 = None, E1 = None, I2 = None, E2 = None, backgroundFit = True):

    def csqr(sc, I1, I2, E1, E2):
        #csqr to be used with scipy.optimize.leastsq
        return (I1 - sc[0] * I2 - sc[1]) / (sqrt(E1**2 + E2**2))

    def csqrNoBG(sc, I1, I2, E1, E2):
        #csqr to be used with scipy.optimize.leastsq, ignores background
        return (I1 - sc[0] * I2 ) / (sqrt(E1**2 + E2**2))

    def csqrV1(I1, I2, E1, E2):
        #complete reduced chi-squared calculation
        return sum(((I1 - I2) / (sqrt(E1**2 + E2**2)) )**2) / np.size(I1)

    #match the curves
    sc = np.zeros(2)
    sc[1] = I1.min() - I2.min()
    sc[0] = (I1 / I2).mean()
    if backgroundFit:
        sc, dummySuccess = leastsq(csqr, sc, 
                args = (I1, I2, E1, E2)
                )
    else:
        sc, dummySuccess = leastsq(csqrNoBG, sc, 
                args = (I1, I2, E1, E2)
                )
        sc[1] = 0.
        
    conVal = csqrV1(I1, I2*sc[0] + sc[1], 
            E1, E2)
    
    print('scaling factor: {}, background factor: {}, chi-squared: {}'
            .format(sc[0], sc[1], conVal) )

    return sc

