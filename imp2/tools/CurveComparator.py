#!/usr/bin/env python                                                          
#coding: utf-8
#
"""
Overview:
========
This is a tool for calculating the scaling factor between the datacurve 
(integrated) and a given (calibrated) dataset. 
This tool can be used to determine the absolute intensity calibration factor
from a scattering pattern collected (and corrected) of a standard sample.
Shows the integrated dataplot, and compares it with the supplied calibrated
datacurve. Outputs the scaling constand and (optional) background level.

Requirements:
============
  - Data must have been corrected and integrated
  - Q must have been calculated
  - A mask should have been applied (fits over the entire range)

Required input arguments:
    *Q*: Q-vector of the calibrated data to compare against
    *I*: intensity of the calibrated data 
    *IError*: relative intensity uncertainty of the calibrated data 
Optional input arguments: 
    *backgroundFit*: Boolean indicating whether or not to fit the background,
        Default: True
"""

__author__ = "Brian R. Pauw"
__contact__ = "brian@stack.nl"
__license__ = "GPLv3+"
__date__ = "2014/03/19"
__status__ = "beta"

# correctionBase contains the __init__ function for these classes
from imp2.modules.correctionBase import correctionBase
from imp2.tools.findScaling import findScaling
from numpy import sqrt
import numpy as np
from scipy.interpolate import interp1d
from matplotlib.pyplot import figure, errorbar, plot, draw, subplot
from matplotlib.pyplot import yscale, xlabel, xscale, ylabel, legend
from matplotlib import gridspec 
from scipy.optimize import leastsq

# name the class properly
class CurveComparator(correctionBase):
    _passthrough = list()
    _passthrough.append('backgroundFit')
    _passthrough.append('Q')
    _passthrough.append('I')
    _passthrough.append('IError')

    # keep this name to "do", with no input arguments (settings are taken 
    # from the configuration

    def do(self, backgroundFit = True, Q = None, I = None, IError = None):

        # good thing we are only passing on data handles here
        Q1 = self.data.data.intDataQ()
        I1 = self.data.data.intDataI()
        E1 = self.data.data.intDataIRU()
        
        IInterp = interp1d(Q, I, bounds_error = False)
        EInterp = interp1d(Q, IError, bounds_error = False)
        I2 = IInterp(Q1)
        E2 = EInterp(Q1)

        sc = findScaling(I2, E2, I1, E1, backgroundFit = backgroundFit)
        
        #display
        figure(figsize = (8,6))
        gs = gridspec.GridSpec(2, 1, height_ratios = [3, 1])
        ax0 = subplot(gs[0])
        errorbar(Q1, I1 * sc[0] + sc[1], E1 * sc[0], label = 'imp dataset \n (scaled: {}, offset: {} )'
                .format(sc[0], sc[1]) )
        errorbar(Q1, I2, E2, label = 'external (calibration) dataset')
        legend()
        yscale('log')
        xlabel('Q')
        ylabel('Intensity')

        ax1 = subplot(gs[1])
        I1a = (I1 * sc[0] + sc[1])
        errorbar(Q1, (I1a - I2) / (I1a), 
                sqrt(E1**2 + E2**2) / I1a)
        ylabel('fractional deviation')
        xlabel('Q')

        return sc

