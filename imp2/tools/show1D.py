#!/usr/bin/env python                                                          
#coding: utf-8
#
"""
Overview:
========
This tool plots one or multiple datasets on a double-logarithmic scaled image.
Very simple and straightforward, but hopefully useful. 

Required input arguments:
    *filename*: A single filename or list of imp .csv filenames to plot
Optional input arguments: 
    *xLim*: A two-element vector of X-axis limits
    *yLim*: A two-element vector of y-axis limits
"""

__author__ = "Brian R. Pauw"
__contact__ = "brian@stack.nl"
__license__ = "GPLv3+"
__date__ = "2014/03/31"
__status__ = "beta"

# correctionBase contains the __init__ function for these classes
from numpy import sqrt
import numpy as np
import pandas
from scipy.interpolate import interp1d
from scipy.optimize import leastsq
from matplotlib.pyplot import figure, draw, errorbar, plot, legend, yscale
from matplotlib.pyplot import xscale, ylim, xlim, grid, axis, xlabel, ylabel
from imp2.modules.Read1D import Read1D

def show1D(filename = None, xLim = None, yLim = None, scaleFactors = None,
        graph = True, barebones = False):
    
    if filename == None:
        print('No filename to plot given')
        return

    if not isinstance(filename, list):
        filename = [filename]

    if scaleFactors is None:
        scaleFactors = np.ones(len(filename))
    if not isinstance(scaleFactors, np.ndarray):
        scaleFactors = np.array(scaleFactors)
    
    if graph:
        fh = figure()
    #read and plot
    fi = 0
    datalist = list()
    for fn in filename:
        pDat = Read1D.readData(filename = fn, delimiter = ',', 
                searchTag = "#DATA", readLines = -1)
        if isinstance(pDat, bool):
            pDat = pandas.read_csv(
                    fn, delimiter = ';').values

        if graph:
            errorbar(pDat[:,0], pDat[:,1] * scaleFactors[fi], 
                    pDat[:,2] * scaleFactors[fi], label = fn)
        fi += 1
        datalist.append(pDat[:,0:3])
    
    if graph:
        legend()
        xscale('log')
        yscale('log')
        #set to limits
        if xLim is not None:
            xlim(xLim)
        if yLim is not None:
            ylim(yLim)
        if xLim is None and yLim is None:
            axis('tight')
        grid('on')
        xlabel('Q, 1/m')
        ylabel('I, 1/m')
        draw()


    return datalist


