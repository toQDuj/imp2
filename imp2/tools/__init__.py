#!/usr/bin/env python                                                          
#coding: utf8    

__author__ = "Brian R. Pauw"
__contact__ = "brian@stack.nl"
__license__ = "GPLv3+"
__copyright__ = "National Institute of Materials Science, Tsukuba, Japan"
__date__ = "2013-03-19"
__status__ = "beta"
version = "0.0.1"

__all__ = [ "AnisoMap", "CurveComparator" ]
