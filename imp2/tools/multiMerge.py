#!/usr/bin/env python                                                          
#coding: utf-8
#
"""
Overview:
========
This is a tool used for combining measurements from multiple measurement
points on the same sample. 
It calculates the average intensity, the scaling factors for each curve, 
and propagates the uncertainties. Background is expected to have been
corrected for.
The Q-values are expected to match for all datasets, and all datasets are
expected to be processed identically. 

Required input arguments:
    *filelist*: list of filenames to include. If the supplied filelist are 
        HDF5 files, it will try to combine images and rebin for better 
        uncertainty estimates. Otherwise it will combine integrated curves.
Optional input arguments: 
    *graph*: Display graphs with the original data in it. Default True
    *outputFilename*: filename to write the data to.

Returns a dictionary with the fields:
    *Q* : Q values
    *I* : mean intensity
    *IERR* : propagated uncertainty estimates
    *Scalings* : scaling factors for the datasets
    *absErr* : absolute uncertainty estimated from STD of scaling factors
    *originalData* : original datasets used to calculate mean

"""

__author__ = "Brian R. Pauw"
__contact__ = "brian@stack.nl"
__license__ = "GPLv3+"
__date__ = "2014/09/02"
__status__ = "alpha"

# correctionBase contains the __init__ function for these classes
import numpy as np
import itertools
from imp2.tools.findScaling import findScaling
from imp2.tools.show1D import show1D
from imp2.impdata import iData
from imp2.modules.Write1D import writer
from imp2.modules.Integrator import binning1d
from imp2.modules.Write2D import writeData

class multiMerge(object):
    def __init__(self, filelist = None, graph = True, outputFilename = None, 
            barebones = True, store2D = False):
        #set input arguments:
        self.filelist = filelist
        self.graph = graph
        self.outputFilename = outputFilename
        self.barebones = barebones

        #check if the supplied files are HDF5 files:
        if self.fileIsH5(filelist[0]):
            self.multiMerge2D(store2D)
        else:
            self.multiMerge1D()

    def fileIsH5(self, filename):
        "tests whether the supplied filename can be read as hdf5"
        #try reading as H5
        dataIsH5 = True
        try:
            import h5py
        except ImportError:
            print('h5py Python library not available')
            dataIsH5 = False
        if dataIsH5:
            try:
                h5file = h5py.File(filename)
            except IOError:
                dataIsH5 = False
                #not a HDF5 file

        return dataIsH5

    def multiMerge2D(self, store2D = False):
        """
        processing module for handling 2D merging. 
        """
        import h5py
        #make a new imp dataset to store the data in:
        merged = iData()

        #get details from the first file:
        print('Loading details from file: {}'.format(self.filelist[0]))
        hpy = h5py.File(self.filelist[0])
        copyKeys = ["Qx", "Qy", "QxRU", "QyRU", "QxAU", "QyAU", 
                "intDataQEdges", "intDataQ", "intDataQAU", "intDataQRU", 
                "flatfield", "flatfieldRU"]
        for key in copyKeys:
            if not key in hpy['/imp'].keys():
                #skippit
                print('skipped copy of {}'.format(key))
                continue
            print('copying {}'.format(key))
            merged.set( key, hpy['/imp/{}'.format(key)].value )

        hpy.close()

        # save data we need in lists
        IList, IRUList, IAUList = list(), list(), list()
        intI, intE, maskList = list(), list(), list()
        for fn in self.filelist:
            hpy = h5py.File(fn)
            IList.append(hpy['/imp/workData'].value)
            IRUList.append(hpy['/imp/workDataRU'].value)
            IAUList.append(hpy['/imp/workDataAU'].value)
            intI.append(hpy['/imp/intDataI'].value)
            intE.append(hpy['/imp/intDataIRU'].value)
            maskList.append(hpy['/imp/mask'].value)
            hpy.close()

        # calculate mean:
        Imean = np.mean(np.array(intI), axis = 0)
        Emean = np.mean(np.array(intE), axis = 0)
        #any true value will apply to all others.
        mask = np.array(np.mean(np.array(maskList), axis = 0), dtype = bool)
        # get scaling factors, via integrated datasets:
        Scalings = list()
        for di, dset in enumerate(self.filelist):
            Scalings.append( 
                    findScaling(I1 = Imean, E1 = Emean, 
                        I2 = intI[di], E2 = intE[di], 
                        backgroundFit = False)[0] #only scaling, not bg
                    )
        ## scaling factor determination usign 2d didn't quite work:    
        # Scalings = list()
        # validi = (True - merged.get('mask'))
        # print('shape validi = {}'.format(np.shape(validi)))
        # print('shape Imean = {}'.format(np.shape(Imean)))
        # for dsi, dset in enumerate(IList):
        #     I1 = Imean[validi].flatten() 
        #     E1 = IRUmean[validi].flatten() 
        #     I2 = dset[validi].flatten() 
        #     E2 = IRUList[dsi][validi].flatten() 
        #         
        #     print dsi
        #     Scalings.append( 
        #             findScaling(I1 = Imean[validi].flatten(), 
        #                 E1 = IRUmean[validi].flatten(), 
        #                 I2 = dset[validi].flatten(), 
        #                 E2 = IRUList[dsi][validi].flatten(), 
        #                 backgroundFit = False)[0] #only scaling, not bg
        #             )

        validi = (True - mask)
        print('Shape Validi: {}, shape mask: {}'.format(np.shape(validi), np.shape(mask)))
        imshape = np.shape(validi)
        numFrames = len(self.filelist)
        frameset = np.zeros((imshape[0], imshape[1], numFrames))
        # Scale the datasets and store in frames:
        IRU = np.zeros(imshape) # first store sum of squares in here
        for nd in range(len(IList)):
            frameset[:, :, nd] = IList[nd] * Scalings[nd]
            IRU += IRUList[nd]**2
        #postprocess for uncertaity to turn into real uncertainties:
        IRU = np.sqrt(IRU) / numFrames

        #add to workdata
        merged.setFrames(frameset)
        merged.set('workDataRU', IRU)
        merged.set('workDataRU', np.nan)
        merged.set('mask', mask)
        if store2D and (self.outputFilename is not None):
            #save combined image
            writeData(merged, self.outputFilename, remove = True)
        else:
            #We let the integrator do the rest:
            Q, Psi = merged.QPsi()
            qbin, Ibin, sebin, qebin = binning1d( merged.get('intDataQEdges'),
                    Q[validi], frameset[validi[:, :, np.newaxis]], IRU[validi])

            # the scaling factor sample standard deviation is an additional measure of 
            # the absolute uncertainty.
            absDev = np.std(Scalings, ddof = 1)
            if self.outputFilename is not None:
                #write a barebones file
                iterData = itertools.izip_longest(qbin/1e9, Ibin, sebin) #q in nm
                writer(self.outputFilename, iterData, hstrs = None, 
                        sep = ';', append = False)

    def multiMerge1D(self):
        """
        processing module for handling 1D merging.
        """
        datalist = show1D(filename = self.filelist, graph = self.graph)
        dlArr = np.array(datalist)
        # extract Q
        Qmean = datalist[0][:,0]
        # calculate average I
        Imean = np.mean(dlArr[:,:,1], axis = 0)
        # average E for initial scaling fit
        Emean = np.mean(dlArr[:,:,2], axis = 0)
        # find scaling for each dataset
        Scalings = list()
        for dset in datalist:
            Scalings.append( 
                    findScaling(I1 = Imean, E1 = Emean, 
                        I2 = dset[:,1], E2 = dset[:,2], 
                        backgroundFit = False)[0] #only scaling, not bg
                    )

        # now the biggest job: re-estimate the uncertainties: 
        # better, re-integrate scaled 2D datasets from .h5 files
        # 1. by propagating the uncertainties of the data
        EmeanSD = 1./len(Scalings) * np.sqrt(
                ((dlArr[:, :, 2] * 
                    np.array(Scalings)[:, np.newaxis])**2)
                .sum(axis=0)
                )
        # 2. by calculating the sample standard error between datapoints
        EmeanSE = 1./np.sqrt(len(Scalings)) * np.sqrt(
                1./(len(Scalings) - 1) *
                ((dlArr[:,:,1] * np.array(Scalings)[:, np.newaxis] 
                    - (Imean + 0 * dlArr[:,:,1]))**2)
                .sum(axis = 0)
                )
        Emean = np.maximum(EmeanSD, EmeanSE)
        print('SD > SE for {} out of {} datapoints'
                .format((EmeanSD > EmeanSE).sum(), len(Qmean)))
        
        # the scaling factor sample standard deviation is an additional measure of 
        # the absolute uncertainty.
        absDev = np.std(Scalings, ddof = 1)
        outDict = {'Q' : Qmean,
                'I' : Imean,
                'IERR' : Emean,
                'Scalings' : Scalings,
                'absErr' : absDev,
                'originalData' : datalist}
        if self.outputFilename is not None:
            #write a barebones file
            iterData = itertools.izip_longest(Qmean/1e9, Imean, Emean) #q in nm
            writer(self.outputFilename, iterData, hstrs = None, 
                    sep = ';', append = False)

