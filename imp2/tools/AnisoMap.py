#!/usr/bin/env python                                                          
#coding: utf-8
#
"""
Overview:
========
This is a tool for displaying the difference between the 2D image, and 
the azimuthally integrated data. This can be used, for example, to check
for anisotropy in the sample, or even for calculating a flatfield image 
for the detector. 

Requirements:
============
  - Data must have been integrated, 
  - Q must have been calculated
  - A mask should be applied
"""

__author__ = "Brian R. Pauw"
__contact__ = "brian@stack.nl"
__license__ = "GPLv3+"
__date__ = "2014/03/19"
__status__ = "beta"

# correctionBase contains the __init__ function for these classes
from imp2.modules.correctionBase import correctionBase
from imp2.tools.findScaling import findScaling
from numpy import sqrt, argwhere
import numpy as np
from scipy.interpolate import interp1d
from matplotlib.pyplot import figure, imshow, colorbar, draw

# name the class properly
class AnisoMap(correctionBase):
    _passthrough = list()
    _passthrough.append("Q")
    _passthrough.append("I")
    _passthrough.append("IError")
    _passthrough.append("backgroundFit")
    _passthrough.append("setAsFlatfield")
    _passthrough.append("flatfieldLimits")
    _passthrough.append("Display")

    # keep this name to "do", with no input arguments (settings are taken 
    # from the configuration
    def do(self, Q = None, I = None, IError = None, backgroundFit = True,
            setAsFlatfield = False, flatfieldLimits = None, Display = False):

        # good thing we are only passing on data handles here
        workData = self.data.data.workData()
        workDataRU = self.data.data.workDataRU()
        mask = self.data.data.mask()
        AU = self.data.data.workDataAU()
        RU = self.data.data.workDataRU()
        Q2D, Psi = self.data.data.QPsi()
        intQ = self.data.data.intDataQ()
        intI = self.data.data.intDataI()
        intIRU = self.data.data.intDataI()
        FFmin = -0.5
        FFmax = 0.5
        
        if flatfieldLimits is not None:
            FFmin = np.min(flatfieldLimits)
            FFmax = np.max(flatfieldLimits)

        #if we are using a calibrated dataset: interpolate:
        if I is not None:
            IInterp = interp1d(Q, I, bounds_error = False)
            EInterp = interp1d(Q, IError, bounds_error = False)
            I2 = IInterp(intQ)
            E2 = EInterp(intQ)
            #find scaling factor:
            sc = findScaling(I2, E2, intI, intIRU, backgroundFit)
            #scale and use as comparison curve
            IInterp = interp1d(Q, I / sc[0] - sc[1], bounds_error = False)
        else:
            #use integrated data alone
            IInterp = interp1d(intQ, intI, bounds_error = False)

        Deviation = (workData - IInterp(Q2D))/IInterp(Q2D) * (True - mask) 
        #display difference
        if Display:
            figure()
            imshow(Deviation, origin = 'lower', vmin = FFmin, vmax = FFmax)
            colorbar()

        if setAsFlatfield:
            #store as flatfield 
            self.data.data.setFlatfield(Deviation + 1.)

            #propagate and store uncertainties if possible
            EInterp = interp1d(Q, IError / sc[0], bounds_error = False)
            if workDataRU is not None:
                FFRU = np.sqrt( 
                        (workDataRU**2 + (EInterp(Q2D) / IInterp(Q2D))**2)
                        )
            else:
                FFRU = EInterp(Q2D) / IInterp(Q2D)
            self.data.data.setFlatfieldRU(FFRU)
                

            #set mask pixels invalid of FF deviations outside flatfieldLimits
            extraMask = ( Deviation > FFmax )
            mask += extraMask
            extraMask = ( Deviation < FFmin )
            mask += extraMask
            self.data.data.setMask(mask)

        return Deviation

