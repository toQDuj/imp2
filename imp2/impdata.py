#!/usr/bin/env python                                                          
#coding: utf8    

import numpy as np
import logging
import os
import fabio
import h5py

class iData(object):
    """base class defing the content of the data blocks.
    This class defines a data object with read and write functionality,
    and passes on data handles for data, uncertainties, q, etc. 
    Writes a canSAS2012-compatible HDF5 file.
    requires h5py and fabio to be installed.
    can be called with a completed cInfo (correctionInfo) database
    containing the parameters and filenames required to load and calibrate.
    Relative (RU) and absolute (AU) uncertainties (standard deviations) can be 
    used to calculate detailed uncertainty information on the intensity values.
    The intensity for pixel j is defined as Ij +/- RUj +/- AU.
    Note that this is different than the uncertainty definition for parameters,
    which are defined as Value +/- ValueAU +/- ValueRU * Value

    TODO:
    For the dezingering correction, multiple sample images are required. 
    Each of these should be recorded for an equal amount of time. They can
    be loaded by specifying a list as the sample filename:
    fnames = ['file001.gfrm', 'file002.gfrm', ..., 'file023.gfrm']. 
    The measurement times should then also be an array of values. 

    Contents of this class are:
    *rawData* Data as loaded (fabio class). first object in series.
    *frames* individual frames of the image (xpix by ypix by nframes) 
    *workData* data after treatment and corrections
    *workDataAU* absolute uncertainty of the work data (single value)
    *workDataRU* Relative uncertainty of the work data (pixel-mapped)
    *mask* masked data, 1 for masked pixels, 0 for unmasked
    *flatfield* Sensitivity (normalized to 1) of the sensor pixels.
    *flatfieldRU* Relative uncertainty of the flatfield
    *intDataQ* q-bin mean values of the integrated data
    *intDataQEdges* q-bin *edges* of the integrated data
    *intDataQRU* q-bin edge relative uncertainty of the integrated data
    *intDataQAU* q-bin edge absolute uncertainty of the integrated data
    *intDataI* intensity values of the integrated data
    *intDataIRU* relative intensity uncertainty of the integrated data
    *intDataIAU* absolute intensity uncertainty of the integrated data
    *processLog* a logging instance writing to file
    *Qx* Q-vector (m^-1) in x-direction
    *Qy* Q-vector (m^-1) in y-direction
    *QxRU* relative uncertainty estimate of Qx
    *QyRU* relative uncertainty estimate of Qy
    *QxAU* absolute uncertainty estimate of Qx
    *QyAU* absolute uncertainty estimate of Qy

    "setter" and "getter" functions are available for all.
    Furthermore, translation functions are available:
    *QPsi*: returns Qx, Qy transformed to Q and Psi (in degree)
    *TwoTheta*: returns Qx, Qy transformed to two theta
    
    """
    _arrayDataType=np.float32 #float64 precision overkill

    def reset(self):
        self._rawData = None #data as loaded (fabio object). 
        self._frames = None 
        self._workData = None 
        self._workDataAU = None 
        self._workDataRU = None 
        self._mask = None 
        self._flatfield = None 
        self._flatfieldRU = None 
        self._intDataQ = None 
        self._intDataQEdges = None 
        self._intDataQRU = None 
        self._intDataQAU = None 
        self._intDataI = None 
        self._intDataIRU = None 
        self._intDataIAU = None 
        self.processLog = None 
        self._Qx = None
        self._Qy = None
        self._QxRU = None 
        self._QyRU = None 
        self._QxAU = None 
        self._QyAU = None 

    def __init__(self):
        """set each value to the right datatype"""
        #reset everything:
        self.reset()

    ##################setters#######################

    def QPsi(self):
        """returns Q and Psi calculated from Qx and Qy"""
        if self.Qx() is None:
            logging.warning('Qx and Qy not set yet, cannot continue')
            return
        Qx = self.Qx()
        Qy = self.Qy()
        Q = np.sqrt( Qx**2 + Qy **2 )
        Psi = 180./np.pi * np.arctan2(Qx, Qy)
        Psi %= 360.
        return Q, Psi

    def TwoTheta(self, wavelength = 0.):
        """returns two theta calculated form Qx and Qy. 
        (two theta is required for some corrections)
        required input argument is the wavelength (in m)"""
        if self.Qx() is None:
            logging.warning('Qx and Qy not set yet, cannot continue')
            return
        #need Q
        Q, Psi = self.QPsi()
        TwoTheta = 2.* np.arcsin( Q * wavelength / (4. * np.pi) )
        return TwoTheta
    
    def setRawData(self, value):
        self._rawData = value

    def setFrames(self, value):
        """sets the frameset value, adds a dimension if not ndim = 3"""
        # adds third axis if not there.
        np.array(value, dtype = self.arrayDataType() )
        if value.ndim < 3:
            value = value[:,:,np.newaxis]
        self._frames = value

    def addFrames(self, value):
        """appends a frame to the frameset"""
        # appends a frame to the set of frames
        np.array(value, dtype = self.arrayDataType() )
        if value.ndim < 3:
            value = value[:,:,np.newaxis]
        self._frames = np.concatenate(
                (self.frames(), value), axis = 2
                )

    def setWorkData(self,value):
        logging.warning('setWorkData depreciated. Apply to frames instead')
        self._workData = np.array(value, dtype=self.arrayDataType())

    def setWorkDataAU(self,value):
        self._workDataAU = float(value)

    def setWorkDataRU(self,value):
        self._workDataRU = np.float32(np.array(value))

    def setMask(self,value):
        self._mask = np.array(value, dtype=bool)

    def setFlatfield(self,value):
        self._flatfield = np.array(value, dtype=float)

    def setFlatfieldRU(self,value):
        self._flatfieldRU = np.array(value, dtype=float)

    def setIntDataQ(self,value):
        self._intDataQ = value
        
    def setIntDataQEdges(self,value):
        self._intDataQEdges = value

    def setIntDataQRU(self,value):
        self._intDataQRU = value

    def setIntDataQAU(self,value):
        self._intDataQAU = float(value)

    def setIntDataI(self,value):
        self._intDataI = value

    def setIntDataIRU(self,value):
        self._intDataIRU = value

    def setIntDataIAU(self,value):
        self._intDataIAU = float(value)

    def setQx(self,value):
        self._Qx = value

    def setQy(self,value):
        self._Qy = value

    def setQxRU(self,value):
        self._QxRU = value

    def setQyRU(self,value):
        self._QyRU = value

    def setQxAU(self,value):
        self._QxAU = float(value)

    def setQyAU(self,value):
        self._QyAU = float(value)

    #####################getters#####################
    def attributes(self):
        """Returns a list of all the datasets in the object"""
        alist = dir(self)
        blist = list()
        for att in alist:
            if (not att.startswith("set")) or len(att) < 4:
                continue #skip: has no setter or is too short

            if att[3] == "Q":
                #for all the Q related values out there
                atest = att[3:]
            else:
                atest = att[3].lower() + att[4:]
            if ( 
                    att.startswith("set") #has a setter function
                    and ("_" + atest) in alist #has an internal data item
                    and atest in alist #has a getter function
                    ):
                blist.append(atest)

        return blist

    def get(self,key):
        #metagetter for data
        if not key in self.attributes():
            return False
        else:
            return getattr(self, key)()

    def set(self, key, value):
        #metasetter for data
        if not key in self.attributes():
            return False
        else:
            setkey = 'set{}{}'.format(key[0].upper(), key[1:])
            return getattr(self, setkey)(value)

    def arrayDataType(self):
        return self._arrayDataType

    def rawData(self):
        return self._rawData

    def frames(self, frameNum = None):
        """
        if no argument is provided, returns all frames (as 3D matrix), 
        otherwise returns a single frame
        """
        if frameNum is None:
            return self._frames
        else:
            return self._frames[:,:,frameNum]

    def workData(self):
        """returns transient data. workData is the mean of the frames"""
        return self.frames().mean(axis = 2)

    def workDataAU(self):
        """returns transient data"""
        return self._workDataAU

    def workDataRU(self):
        """returns transient data"""
        return self._workDataRU

    def mask(self):
        if self._mask is None:
            return np.zeros(np.shape(self.workData()),dtype = bool)
        else:
            return self._mask 

    def flatfield(self):
        return self._flatfield 

    def flatfieldRU(self):
        return self._flatfieldRU 

    def intDataQ(self):
        return self._intDataQ 

    def intDataQEdges(self):
        return self._intDataQEdges 

    def intDataQRU(self):
        return self._intDataQRU 

    def intDataQAU(self):
        return self._intDataQAU 

    def intDataI(self):
        return self._intDataI 

    def intDataIRU(self):
        return self._intDataIRU

    def intDataIAU(self):
        return self._intDataIAU 

    def rawData(self):
        return self._rawData

    def Qx(self):
        return self._Qx

    def Qy(self):
        return self._Qy

    def QxRU(self):
        return self._QxRU

    def QyRU(self):
        return self._QyRU

    def QxAU(self):
        return self._QxAU

    def QyAU(self):
        return self._QyAU

class impData(object):
    """defines supplementary functions for data objects, such as those to read
    raw data files and HDF5 data files (containing processed data), and for
    writing HDF5 files and logging.
    Can be initiated as: 
    >>> a=impData()

    a file can then be read with
    >>> a.readRaw('MgZn_N0_C_001.img')

    if no logger is requested (for example if this is not the main image
    for corrections:)
    >>> a.readRaw('MgZn_N0_C_001.img',startLogger=False)

    """

    def __init__(self, **kwargs):
        """initialise the defaults and populate the database with values
        where appropriate"""
        self.data = iData() #initialise the data structure

    def __initLogger__(self, filename):
        if filename is None:
            raise IOError #no filename specified
        """filename argument should be the filename of the datafile"""
        fpath, fname = os.path.split(filename)
        logfname = filename + '.implog'
        #discard previous log if exists
        try: 
            os.unlink(logfname)
        except OSError:
            #file did not exist
            pass
        except:
            #some other error
            raise

        fh = logging.FileHandler(logfname)
        #fh.setLevel(logging.INFO)
        #also stream to console
        ch = logging.StreamHandler()
        #ch.setLevel(logging.DEBUG)
        #add date and time to the logging messages of file; remove level
        fmt = logging.Formatter('%(asctime)s - %(message)s')
        fh.setFormatter(fmt)

        #initialise logging, set filename in main class.
        logging.basicConfig(level = logging.INFO)
        self.data.processLog = logging.getLogger("processLog")
        self.data.processLog.addHandler(fh)
        self.data.processLog.addHandler(ch)
        self.data.processLog.setLevel(logging.INFO)
        #logger file handle can be recalled from self.data.processLog.handlers

    def logEntry(self, logString):
        self.data.processLog.info( logString )

    def close(self):
        """closes all open files"""
        
        for hand in self.data.processLog.handlers:
            hand.flush()
            hand.close()
            self.data.processLog.removeHandler(hand)

        logging.shutdown()

    def reInitialise(self):
        #set everything to None; resetting the data class
        self.data.reset()

