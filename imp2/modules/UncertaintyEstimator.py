#!/usr/bin/env python                                                          
#coding: utf-8
#
"""
Overview
========
This module calculates the uncertainty for photon-counting detectors. It 
should be used directly after data ingestion through the DS module. For
non photon-counting detectors it should not be used.

"""

__author__ = "Brian R. Pauw"
__contact__ = "brian@stack.nl"
__license__ = "GPLv3+"
__date__ = "2014/09/05"
__status__ = "beta"

# correctionBase contains the __init__ function for these classes
from correctionBase import correctionBase
from numpy import sqrt
import numpy as np

# name the class properly
class UncertaintyEstimator(correctionBase):

    # keep this name to "do", with no input arguments (settings are taken 
    # from the configuration
    def do(self):

        # good thing we are only passing on data handles here
        frames = self.data.data.frames()
        AU = self.data.data.workDataAU() # should be None
        RU = self.data.data.workDataRU() # should be None

        if AU is None:
            AU = 0
        if RU is None:
            RU = np.ones(np.shape(frames[:,:,0]))
        else:
            RU = np.maximum(1, RU) #no zero uncertainties here
        ########### insert custom operation here ############

        numFrames = np.size(frames, axis = 2)
        #we estimate the uncertainty from the collected counts in the frames
        RU = np.maximum(RU, np.sqrt(frames.sum(axis = 2)) / numFrames)
        AU = np.maximum(AU, np.sqrt(frames.sum()) / numFrames)

        # store action in the processlog:                          
        self.data.logEntry(                         
                'Uncertainty estimated based on photon-counting assumption')

        ########### end custom operation here ############

        # store if available:
        if AU is not None:
            self.data.data.setWorkDataAU(AU)
        if RU is not None:
            self.data.data.setWorkDataRU(RU)

        return 

