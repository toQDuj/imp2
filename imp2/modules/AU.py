#!/usr/bin/env python                                                          
#coding: utf-8
#
"""
Overview
========
correction module for absolute unit scaling

"""

__author__ = "Brian R. Pauw"
__contact__ = "brian@stack.nl"
__license__ = "GPLv3+"
__date__ = "2013/12/24"
__status__ = "beta"

# correctionBase contains the __init__ function for these classes
from correctionBase import correctionBase
from numpy import sqrt

# name the class properly
class AU(correctionBase):

    # keep this name to "do", with no input arguments (settings are taken 
    # from the configuration
    def do(self):

        # good thing we are only passing on data handles here
        frames = self.data.data.frames()
        AU = self.data.data.workDataAU()
        RU = self.data.data.workDataRU()

        ########### insert custom operation here ############

        # get parameters that are required for the correction
        cfact = self.par.getParVal('cfact')
        
        #corrections are done to frames. workData follows automatically
        framesCor = frames * cfact

        if AU is not None:
            #load absolute parameter uncertainties
            cfactSTD = self.par.getParSTD('cfact')
            # correct absolute uncertainty. timeSTD already relative
            AU = framesCor.sum() * sqrt( 
                    ( AU / frames.sum() )**2 + ( cfactSTD )**2 
                    )

        if RU is not None:
            # correct *relative* uncertainties for multiplication operation
            # interpixel uncertainty not affected other than simple scaling
            RU = RU * cfact

        # store action in the processlog:                          
        self.data.logEntry(                         
                'Data scaled to absolute units with factor {} {}'.format(
                    cfact, self.par.getParUnit('cfact'))          
                )                       

        ########### end custom operation here ############

        # store data:
        self.data.data.setFrames( framesCor )

        # store if available:
        if AU is not None:
            self.data.data.setWorkDataAU(AU)
        if RU is not None:
            self.data.data.setWorkDataRU(RU)


        return 

