#!/usr/bin/env python                                                          
#coding: utf-8
#
"""
Overview
========
Special module for calculating Qx, Qy-values, along with their uncertainty
estimates. This is an extra-special version for cylindrical detectors, with
their cylinder axis vertical (y), and a radius identical to the 
sample-to-detector distance. 
ONLY SIMPLE VERSION IMPLEMENTED FOR NOW:
    (does not take droffset or offset point of normal incidence into account)
"""

__author__ = "Brian R. Pauw"
__contact__ = "brian@stack.nl"
__license__ = "GPLv3+"
__date__ = "2014/03/06"
__status__ = "beta"

# correctionBase contains the __init__ function for these classes
from correctionBase import correctionBase
import numpy as np

# name the class properly
class QCalcVertCylDet(correctionBase):

    # keep this name to "do", with no input arguments (settings are taken 
    # from the configuration
    def do(self):

        # get parameters that are required for the correction
        imsize = np.array([ 
                np.size(self.data.data.workData(), 1),
                np.size(self.data.data.workData(), 0) 
                ])
        [wlength, plength, clength, beampos] = [
                self.par.getParVal(pn) for pn in 
                ('wlength', 'plength', 'clength', 'beampos')
                ]
        [wlengthSTD, plengthSTD, clengthSTD, beamposSTD] = [
                self.par.getParSTD(pn) for pn in 
                ('wlength', 'plength', 'clength', 'beampos')
                ]

        #horizontal "x"-direction, 2-theta = arcsection / radius
        xpixeldist = plength[0] * (np.arange(imsize[0]) - beampos[0])
        ypixeldist = plength[1] * (np.arange(imsize[1]) - beampos[1])
        Qx = 4. * np.pi / wlength * np.sin(0.5 * (xpixeldist / clength))
        Qy = 4. * np.pi / wlength * np.sin(0.5 * 
                np.arctan(ypixeldist / clength))[:, np.newaxis]
        #extend to cover all pixels:
        Qx = Qx + Qy*0
        Qy = Qy + Qx*0

        #now we estimate the uncertainties on these values:
        #TODO: probably not correct: 
        QxU = Qx * np.sqrt( wlengthSTD**2 + ( np.arctan (
            np.sqrt( plengthSTD[0]**2 + clengthSTD**2 ) 
            ) )**2
            )
        QyU = Qy * np.sqrt( wlengthSTD**2 + ( np.arctan (
            np.sqrt( plengthSTD[1]**2 + clengthSTD**2 ) 
            ) )**2
            )
        #separate into AU and RU:
        QyAU = QyU.min()
        QxAU = QxU.min()
        QyRU = QyU - QyAU
        QxRU = QxU - QxAU


        # store action in the processlog:                          
        self.data.logEntry( 'Q-values computed for dataset.' )                       

        # store data:
        self.data.data.setQx( Qx )
        self.data.data.setQy( Qy )
        self.data.data.setQxRU( QxRU )
        self.data.data.setQyRU( QyRU )
        self.data.data.setQxAU( QxAU )
        self.data.data.setQyAU( QyAU )

        return 

