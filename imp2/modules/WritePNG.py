#!/usr/bin/env python                                                          
#coding: utf-8
#
"""
Overview
========
A writer for 2D data. Writes the workData to a png file. This can then
be used to draw a mask in an image (pixel)-editor. 
Additional arguments: 
    *filename*: a filename to write the image to.
    *imscale*: 'linear' or 'log', writing I or log10(I) respectively.
"""

__author__ = "Brian R. Pauw"
__contact__ = "brian@stack.nl"
__license__ = "GPLv3+"
__date__ = "2014/01/18"
__status__ = "beta"

# correctionBase contains the __init__ function for these classes
from correctionBase import correctionBase
from imp2 import cfg
import os
from matplotlib.pyplot import imsave
from numpy import log10, isnan
from time import sleep

# name the class properly
class WritePNG(correctionBase): #does not inherit from correctionBase
    _passthrough = list()
    #I wanna filename!
    _passthrough.append('filename')
    _passthrough.append('imscale')
    _passthrough.append('colormap')

    def do(self, filename = None, imscale = None, colormap = None):
        if filename is None:
            sfname = self.par.getParVal('fname')
            ofname = sfname + '.png'
        else: 
            ofname = filename
        if os.path.exists(ofname):
            #delete if exists
            os.remove(ofname)
        if imscale is None:
            imscale = 'linear'
        if colormap is None:
            colormap = 'hsv'

        #write the workData to the .png file, done by writeData module
        self.writeData(ofname, imscale, colormap)

        # store action in the processlog:                          
        self.data.logEntry(                         
                'workData written to png file: {}'.format(                     
                    ofname)          
                )                       

        ########### end custom operation here ############

        return 

    def writeData(self, filename, imscale, colormap):
        """
        Writes the workData to a .png file. This can be edited to 
        make (for example) the mask file. Not intended for final
        storage of the result. 
        """

        I = self.data.data.workData().copy()
        #remove nans
        I[isnan(I)] = 0
        if imscale == 'log':
            ##clip to minimum, add 1% to everything to avoid zeros
            #I = I - I.min() + 0.01 * (I.max() - I.min())
            #pin zeros to minimum value:
            minval = I[I > 0.].min()
            I = log10(I)
            I[isnan(I)] = log10(minval)

        #print('image minmax for png: {}, {}'.format(I.min(), I.max()))
        imsave(filename, I, cmap = colormap)



