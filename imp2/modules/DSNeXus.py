#!/usr/bin/env python                                                          
#coding: utf-8
#
"""
Overview
========
Custom module of the main imp class for data read-in, reads NeXus files. 
Mostly intended to read Diamond's I22 files, but is hopefully compatible
with much more.

Can be supplied with a dictionary of NeXus paths to imp variable mappings. 
These will then be used to map information contained in NeXus to imp 
settings. *NeXDict*

For NeXus files containing multiple frames... X- and Y- indices can be 
supplied. "all" loads all frames. For the moment, only one of the two 
indices can be "all". 

Related keyword arguments:
    *name* : A freeform short string identifying the measurement
    *fname* : A single image filename in case only one image was recorded
    *notes* : Sample notes (freeform string)
    *dname* : Detector name (freeform string)
    *dtransform* : Detector frame transformation (int 1-8)

"""

__author__ = "Brian R. Pauw"
__contact__ = "brian@stack.nl"
__license__ = "GPLv3+"
__date__ = "2013/12/19"
__status__ = "beta"


from correctionBase import correctionBase
import numpy as np
import h5py

class DSNeXus(correctionBase):
    _passthrough = list()
    _passthrough.append('Xi')
    _passthrough.append('Yi')

    def do(self, Xi = None, Yi = None):

        # parse input arguments
        if Xi is None:
            Xi = 0
        elif Xi in {'all', 'All', ':'}:
            Xi = slice(None)
        if Yi is None:
            Yi = 0
        elif Yi in {'all', 'All', ':'}:
            Yi = slice(None)

        # get the necessary parameters from settings
        fnPar = self.par.getPar('fname')
        dtransform = self.par.getParVal('dtransform')
        dwindow = self.par.getParVal('dwindow')
        filename = fnPar.value()

        # initialise the data logger
        self.data.__initLogger__( filename )                                      

        # store as first action in the processlog:                          
        self.data.logEntry( 'Data correction process started for file {}'
                .format(filename)) 

        # open HDF5 file in read-only mode:
        h = h5py.File(filename, mode = 'r')

        # read frame(s); can result in "array is too big"-error 
        # if both Xi and Yi are ranges
        frames = h['entry1/detector/data'][Yi,Xi,:,:]
        # close after reading, we no longer need the HDF5/NeXus file:
        h.close()

        # rearrange to move to imp2-style data: x, y, framei
        frames = frames.transpose().squeeze()
        if frames.ndim > 3:
            logging.error('ndim extracted from NeXus file >3 ') 
            raise ValueError
        # cycle through data storage to ensure correct dimensions:
        self.data.data.setFrames(frames)
        frames = self.data.data.frames()
        # do transformations if necessary (not recommended)
        if dtransform > 1:
            for framei in range( np.size(frames, 2)):
                frames[:, :, framei] = self.imtransform(
                        frames[:, :, framei], dtransform)
        # store frames
        self.data.data.setFrames(frames)

        # load I for mask:
        I = self.data.data.workData()
        # set mask (all valid)
        mask = np.array(np.zeros(np.shape(I)), dtype=bool)
        #cycle through the frames to mask invalid pixels (dwindow)
        for framei in range( np.size(frames, 2)):
            mask[frames[:, :, framei] < dwindow.min()] = True 
            mask[frames[:, :, framei] > dwindow.max()] = True
        
        # store mask
        self.data.data.setMask(mask)


        # store as first action in the processlog:                          
        self.data.logEntry(                         
                'datafile {} read as class NeXus with shape: {}, dwindow masked: {}'
                .format(filename, frames.shape, mask.sum())          
                )                       

        ########### end custom operation here ############

        return 

    def imtransform(self, img, ti=1):
        """
        image transformation following P. Boesecke's raster orientation schema.
        *img* is a required argument containing a 2D image.
        *ti* is an integer between 1-8 indicating which transformation to apply.
        returns: a transformed image.
        """
        if ti > 4:
            img = np.transpose(img)
            ti = 9 - ti
        if ti == 2:
            img = np.fliplr(img)
        elif ti == 3:
            img = np.flipud(img)
        elif ti == 4:
            img = np.fliplr( np.flipud(img) )
        return img


