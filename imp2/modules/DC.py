#!/usr/bin/env python                                                          
#coding: utf-8
#
"""
Overview
========
correction module for darkcurrent correction. Darkcurrent parameters are a 
    four-element vector: [Da, Db, Dc, tdc] defined as
    Idc = Da + Db * tdc, Dc * int(I) 
    (darkcurrent collection time: tdc = -1 sets to measurement time)

"""

__author__ = "Brian R. Pauw"
__contact__ = "brian@stack.nl"
__license__ = "GPLv3+"
__date__ = "2013/12/24"
__status__ = "beta"

# correctionBase contains the __init__ function for these classes
from correctionBase import correctionBase
from numpy import sqrt
import numpy as np

# name the class properly
class DC(correctionBase):

    # keep this name to "do", with no input arguments (settings are taken 
    # from the configuration
    def do(self):

        # good thing we are only passing on data handles here
        frames = self.data.data.frames()
        AU = self.data.data.workDataAU()
        RU = self.data.data.workDataRU()

        ########### insert custom operation here ############

        # get parameters that are required for the correction
        dcparam = self.par.getParVal('dcparam')
        mtime = self.par.getParVal('time')
        if dcparam[3] == -1:
            dcparam[3] = mtime

        framesCor = frames
        for framei in range(np.size(frames, 2)):
            dccor = (
                    dcparam[0] + dcparam[1] * dcparam[3] + 
                    dcparam[2] * framesCor[:,:,framei].sum()
                    )
            framesCor[:,:,framei] -= dccor

        if AU is not None:
            #load absolute parameter uncertainties
            dcparamSTD = self.par.getParSTD('dcparam')
            # correct absolute uncertainty. STD already relative. Probably not correct, check:
            AU = framesCor.sum() * sqrt( 
                    ( AU / frames.sum() )**2 + 
                    ( dcparamSTD[0] )**2 + 
                    ( dcparamSTD[1] )**2 + 
                    ( dcparamSTD[2] )**2 
                    )

        if RU is not None:
            # correct *relative* uncertainties for multiplication operation
            # interpixel uncertainty not affected 
            RU = RU 

        # store action in the processlog:                          
        self.data.logEntry(                         
                'Data darkcurrent corrected for approx. {} CPS '.format(
                    dccor)          
                )                       

        ########### end custom operation here ############

        # store data:
        self.data.data.setFrames( framesCor )

        # store if available:
        if AU is not None:
            self.data.data.setWorkDataAU(AU)
        if RU is not None:
            self.data.data.setWorkDataRU(RU)


        return 

