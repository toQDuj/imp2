#!/usr/bin/env python                                                          
#coding: utf-8
#
"""
Overview
========
Flatfield correction module
Required input argument: 
    *filename*: a filename (written by the imp2 "Write2D" code) containing
        a suitable flatfield dataset

"""

__author__ = "Brian R. Pauw"
__contact__ = "brian@stack.nl"
__license__ = "GPLv3+"
__date__ = "2014/01/18"
__status__ = "beta"

from correctionBase import correctionBase
from numpy import sqrt
from imp2.modules.Read2D import Read2D
import numpy as np

class FF(correctionBase):
    #think about how to do this...
    _passthrough = list()
    _passthrough.append('filename')

    def do(self, filename = None):

        if filename is None:
            filename = self.par.getParVal('fffname')
        if filename is None:
            logging.warning(
                    'Cannot do flatfield correction without flatfield file'
                    )
            return

        FFData = Read2D.readData(filename)

        # good thing we are only passing on data handles here
        frames = self.data.data.frames()
        AU = self.data.data.workDataAU()
        RU = self.data.data.workDataRU()
        mask = self.data.data.mask()

        ########### insert custom operation here ############
        for framei in range(np.size(frames, 2)):
            frames[:,:,framei] /= FFData["flatfield"] #workData is the mean of frames
        
        # store action in the processlog:                          
        self.data.logEntry(                         
                'Data corrected for flatfield with data in file {}'
                .format(filename)
                )                       

        if not (RU is None) and not (FFData["flatfieldRU"] is None):
            # correct relative uncertainties for multiplication operation
            # interpixel uncertainty not affected other than simple scaling
            RU = sqrt( RU**2 + FFData["flatfieldRU"]**2 ) / FFData["flatfield"] 

        mask += FFData['mask']

        #store flatfield along with data:
        self.data.data.setFlatfield(FFData["flatfield"])
        if FFData["flatfieldRU"] is not None:
            self.data.data.setFlatfieldRU(FFData["flatfieldRU"])

        ########### end custom operation here ############

        # store data:
        self.data.data.setFrames( frames )
        self.data.data.setMask( mask )

        if AU is not None:
            self.data.data.setWorkDataAU(AU)
        if RU is not None:
            self.data.data.setWorkDataRU(RU)

        return 
