#!/usr/bin/env python                                                          
#coding: utf-8
#
"""
Overview
========
correction module for time normalisation. 
Related keyword argument:
    *time*: The measurement time (in seconds) /per frame/ 

"""

__author__ = "Brian R. Pauw"
__contact__ = "brian@stack.nl"
__license__ = "GPLv3+"
__date__ = "2013/12/19"
__status__ = "beta"

# correctionBase contains the __init__ function for these classes
from correctionBase import correctionBase
from numpy import sqrt

# name the class properly
class TI(correctionBase):

    # keep this name to "do", with no input arguments (settings are taken 
    # from the configuration
    def do(self):

        # good thing we are only passing on data handles here
        frames = self.data.data.frames()
        AU = self.data.data.workDataAU()
        RU = self.data.data.workDataRU()

        ########### insert custom operation here ############

        # get parameters that are required for the correction
        mtime = self.par.getParVal('time')

        framesCor = frames / mtime

        if AU is not None:
            #load absolute parameter uncertainties
            timeSTD = self.par.getParSTD('time')
            # correct absolute uncertainty. timeSTD already relative
            AU = framesCor.sum() * sqrt( 
                    ( AU / frames.sum() )**2 + ( timeSTD )**2 
                    )

        if RU is not None:
            # correct *relative* uncertainties for multiplication operation
            # interpixel uncertainty not affected other than simple scaling
            RU = RU / mtime

        # store action in the processlog:                          
        self.data.logEntry(                         
                'Data normalized to a measurement time of {} {}'.format(
                    mtime, self.par.getParUnit('time'))          
                )                       

        ########### end custom operation here ############

        # store data:
        self.data.data.setFrames( framesCor )

        # store if available:
        if AU is not None:
            self.data.data.setWorkDataAU(AU)
        if RU is not None:
            self.data.data.setWorkDataRU(RU)


        return 

