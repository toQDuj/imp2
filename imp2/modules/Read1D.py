#!/usr/bin/env python                                                          
#coding: utf-8
#
"""
Overview
========
A reader for 1D CSV data. returns a matrix of NRows x NCols. 
Delimiter (if not specified) is ",". By default, a "#DATA"-tag is sought
that indicates the start of the data on the next line. This can be skipped 
with the "noTag"-boolean flag, in which case "skiplines" can be used to 
indicate how many header lines to skip (default zero)

Required input arguments:
    *filename*: a filename (written by the imp2 "Write2D" code) to read
Optional input arguments:
    *delimiter*: a delimiter, default ",".
    *skipLines*: number of header lines to skip, default 0, performed before
        searchTag
    *searchTag*: search for a single line containing a tag after which data starts.
        default: "#DATA", can be set to None for no search
    *readLines*: maximum number of data lines to read, default = inf. Negative
        values clip lines off the end.

Usage: 
    DataMatrix = Read1D.readData(filename = 'test.csv', searchTag = "#Data\\n", 
        delimiter = '\\t'"
"""

__author__ = "Brian R. Pauw"
__contact__ = "brian@stack.nl"
__license__ = "GPLv3+"
__date__ = "2014/03/18"
__status__ = "incomplete"

# correctionBase contains the __init__ function for these classes
from correctionBase import correctionBase
from imp2 import cfg
import os
import csv
import numpy as np

# name the class properly
class Read1D(correctionBase): 
    _passthrough = list()
    #we wish to receive this as argument into "do".
    _passthrough.append('filename')
    _passthrough.append('delimiter')
    _passthrough.append('skipLines')
    _passthrough.append('readLines')
    _passthrough.append('searchTag')

    @classmethod
    def do(self,filename = None):

        #read all datablocks from HDF5 file, done by readData module
        DataDict = self.readData(filename)

        #space to read datablocks from NeXus format

        #and parameters...

        return DataDict

    @classmethod
    def readData(self, filename, delimiter = ',', skipLines = 0, 
            readLines = np.inf, searchTag = None):
        """
        reads the data from a CSV file
        """

        #reads a csv file and returns floating-point array of result. 
        if filename == []:
            filename = raw_input('paste the full file path here (if you are on a mac, dragging the file from Finder to the python terminal window works too): \n').strip()

        Ja = [] #empty array in which information will be stored
        fh = open(filename,'rU')
        if delimiter == '':
            delimiter = raw_input("Please specify the delimiter, tab and space work as well. ")
            if delimiter == 'tab':
                delimiter = '\t'
            dellist = {'\t',',',';',' '}

        #open file specifying delimiter
        reader = csv.reader(fh, delimiter = delimiter, skipinitialspace = True) 

        #do we need to skip lines?
        for dummy in range(skipLines):
            fh.readline()
        
        #skip until tag found?
        lineData = ''
        fileLoc = fh.tell()
        if searchTag is not None:
            lineData = fh.readline()
            while (not (searchTag in lineData)) and (not (fh.tell() == fileLoc)):
                fileLoc = fh.tell()
                lineData = fh.readline()
            if fileLoc == fh.tell():
                print('EOF reached while searching for data tag: {}'
                        .format(searchTag))
                return False
            
        #clip lines from end if necessary
        clipLines = 0
        if readLines < 0:
            clipLines = readLines
            readLines = np.inf
        nlines = 0
        for data in reader: #loop over lines
            Ja.append(data) #store lines into array
            nlines+=1
            if nlines >= readLines:
                break

        fh.close()
        
        Jb=np.array(Ja) #hmm. not necessary perhaps
        if clipLines < 0:
            Jb = Jb[:clipLines,:]
        Jc=np.float32(Jb[:,:]) #turn into numbers, skipping the first line.

        return Jc

