#!/usr/bin/env python                                                          
#coding: utf-8
#
"""
Overview
========
background subtraction module. 
Required input argument: 
    *filename*: a filename (written by the imp2 "Write2D" code) to read

"""

__author__ = "Brian R. Pauw"
__contact__ = "brian@stack.nl"
__license__ = "GPLv3+"
__date__ = "2014/01/18"
__status__ = "beta"

from correctionBase import correctionBase
from numpy import sqrt
from imp2.modules.Read2D import Read2D
import numpy as np

class BG(correctionBase):
    #think about how to do this...
    _passthrough = list()
    _passthrough.append('filename')

    def do(self, filename = None):

        if filename is None:
            filename = self.par.getParVal('bfname')
        if filename is None:
            logging.warning(
                    'Cannot do background subtraction without bg file'
                    )
            return

        BGData = Read2D.readData(filename)

        # good thing we are only passing on data handles here
        frames = self.data.data.frames()
        AU = self.data.data.workDataAU()
        RU = self.data.data.workDataRU()

        ########### insert custom operation here ############
        for framei in range(np.size(frames, 2)):
            frames[:,:,framei] -= BGData["workData"] #workData is the mean of frames
        
        # store action in the processlog:                          
        self.data.logEntry(                         
                'Data background subtracted with data in file {}'
                .format(filename)
                )                       

        if not ( AU is None ) and not (BGData["workDataAU"] is None):
            # add absolute uncertainty 
            AU = sqrt( AU**2 + BGData["workDataAU"]**2 )

        if not (RU is None) and not (BGData["workDataRU"] is None):
            # correct relative uncertainties for multiplication operation
            # interpixel uncertainty not affected other than simple scaling
            RU = sqrt( RU**2 + BGData["workDataRU"]**2 ) 

        ########### end custom operation here ############

        # store data:
        self.data.data.setFrames( frames )

        if AU is not None:
            self.data.data.setWorkDataAU(AU)
        if RU is not None:
            self.data.data.setWorkDataRU(RU)


        return 

