#!/usr/bin/env python                                                          
#coding: utf-8
#
"""
Overview
========
template for a correction module

"""

__author__ = "Brian R. Pauw"
__contact__ = "brian@stack.nl"
__license__ = "GPLv3+"
__date__ = "2013/12/19"
__status__ = "beta"

from correctionBase import correctionBase
from numpy import sqrt

class FL(correctionBase):

    def do(self):

        # good thing we are only passing on data handles here
        frames = self.data.data.frames()
        AU = self.data.data.workDataAU()
        RU = self.data.data.workDataRU()

        ########### insert custom operation here ############
        
        flux = self.par.getParVal('flux')

        # store action in the processlog:                          
        self.data.logEntry(                         
                'Data normalized to flux of level {}'.format(                     
                    flux)          
                )                       

        framesCor = frames / flux

        if AU is not None:
            #load parameter uncertainties
            fluxSTD = self.par.getParSTD('flux')
            # correct absolute uncertainty, fluxSTD already relative  
            AU = framesCor.sum() * sqrt( 
                    ( AU / frames.sum() )**2 + ( fluxSTD )**2 
                    )

        if RU is not None:
            # correct relative uncertainties for multiplication operation
            # interpixel uncertainty not affected other than simple scaling
            RU = RU / flux

        ########### end custom operation here ############

        # store data:
        self.data.data.setFrames( framesCor )

        if AU is not None:
            self.data.data.setWorkDataAU(AU)
        if RU is not None:
            self.data.data.setWorkDataRU(RU)


        return 

