#!/usr/bin/env python                                                          
#coding: utf-8
#
"""
Overview
========
module of the base class for all data corrections

"""

__author__ = "Brian R. Pauw"
__contact__ = "brian@stack.nl"
__license__ = "GPLv3+"
__date__ = "2013/12/19"
__status__ = "beta"


# from imp2 import cfg #imports global configurations (data, parameters)

class correctionBase(object):
    _passthrough = None
    """
    base class for corrections, specifying the initialisation
    "_passthrough" can be set to contain keywords in the modules, which are
    passed on to the "do"-function as kwargs pairs
    """

    #standard initialisation
    def __init__(self, **kwargs):
        #Let's see if this works.
        # should be already in place
        # self.par = cfg.parameters
        # self.data = cfg.data
        doDict = None
        if not (self._passthrough is None):
            doDict = dict()
            for item in self._passthrough:
                doDict[item] = kwargs.pop(item, None)

        for kw in kwargs:
            self.parameters.setParVal(kw, kwargs[kw] )

        #Do the thing!
        if doDict is None:
            self.do()
        else:
            self.do(**doDict)

    def do(self):
        """defined in each correction-specific definition file"""
        pass
