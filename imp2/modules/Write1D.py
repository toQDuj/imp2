#!/usr/bin/env python                                                          
#coding: utf-8
#
"""
Overview
========
A writer for 1D (integrated) data. Writes a header, and a set of columns. 
The header contains the absolute uncertainty of the intensity and of Q. 
The correction details are available in the .implog file and in the correction
parameter file. It is therefore not included in here to keep it easy to read.
The data begins after the line containing:
"#DATA". 
The columns written are:
    *Q*: The Q (wavevector) of the bins (mean value of the two bin edges)
    *I*: The intensity in the bin
    *IRU*: The relative uncertainty of the intensity
    --followed by two more unusual columns--
    *Qbinedge*: The bin edges (length of I plus one), useful for plotting in log
    *QRU*: The relative uncertainty of the Q-values, not including bin width

Optional input arguments:
    *barebones*: if set to *True*, writes only data columns Q, I and IRU
    *qunits*: if set to *nm*, writes Q in nm
    *sep*: separator, default ','
"""

__author__ = "Brian R. Pauw"
__contact__ = "brian@stack.nl"
__license__ = "GPLv3+"
__date__ = "2013/12/28"
__status__ = "beta"

# correctionBase contains the __init__ function for these classes
from correctionBase import correctionBase
from imp2 import cfg
import time
import itertools
import os

def writer(filename, iterData, hstrs = None, sep = None, append = True):
    def writeLine(filename, line = None, append = True):
        if append:
            openarg = 'a'
        else:
            openarg = 'w'
        with open(filename, openarg, 0) as fh:
            if isinstance(line,(str,unicode)):
                fh.write(line)
            else:
                # iterable object containing multiple lines
                fh.writelines(line)

    # parse input
    if filename is None:
        raise IOError
    if sep is None:
        sep = ','

    # truncate file if exists (i.e. discard)
    if os.path.exists(filename) and (not append):
        os.remove(filename)

    # write header and data:
    if hstrs is not None:
        writeLine(filename, hstrs)

    # store in file
    moreData = True
    while moreData:
        try:
            # generate formatted datastring containing column data
            wstr = sep.join(['{}'.format(k) for k in iterData.next()]) + "\n"
        except StopIteration:
            # end of data reached
            moreData = False
            break
        except:
            raise

        writeLine(filename, wstr)


# name the class properly
class Write1D(correctionBase): 
    #pass through the filename argument to the "do"-function
    _passthrough = list()
    _passthrough.append('filename')
    _passthrough.append('barebones')
    _passthrough.append('qunits')
    _passthrough.append('sep')

    def do(self, filename = None, barebones = None, qunits = None, sep = None):

        nbin = self.par.getParVal('nbin')
        # load the data we need here
        Q = self.data.data.intDataQ() #the bin *edges* of Q
        QEdges = self.data.data.intDataQEdges() #the bin *edges* of Q
        I = self.data.data.intDataI() #I in the bin
        IAU = self.data.data.intDataIAU() #absolute uncertainty (single value)
        IRU = self.data.data.intDataIRU() #relative uncertainty (length I)
        QAU = self.data.data.intDataQAU() #absolute Q unc. (single value)
        QRU = self.data.data.intDataQRU() #relative Q uncertainty (length Q)

        ########### insert custom operation here ############
        #write header, truncate existing file:
        hstrs = None
        if not barebones:
            hstrs = (
                    "IMP2-generated csv file containing corrected, integrated data \n",
                    "Generated on date: {}, in timezone: {} \n".format(
                        time.asctime(), time.timezone / 3600.),
                    "Absolute intensity uncertainty: {} \n".format(IAU),
                    "Absolute Q uncertainty: {} \n".format(QAU),
                    "Columns: Q, I, IError (rel.), QBinEdge, QError (rel.) \n",
                    "\n"
                    "#DATA\n"
                    )

        #prepare data for printing:
        if qunits == 'nm':
            Qwrite = Q/1.e9
        else:
            Qwrite = Q

        if barebones:
            iterData = itertools.izip_longest(Qwrite,I,IRU)
        else:
            iterData = itertools.izip_longest(Qwrite,I,IRU,QEdges,QRU)

        writer(filename, iterData, hstrs, sep = sep, append = False)
        # store action in the processlog:                          
        self.data.logEntry(                         
                'Integrated data written to csv file: {}'.format(                     
                    filename)          
                )                       

        ########### end custom operation here ############

        return 

