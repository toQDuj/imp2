#!/usr/bin/env python                                                          
#coding: utf-8
#
"""
Overview
========
module of the main imp class for data read-in. Also reads Diamond's NeXus files

For NeXus files containing multiple frames... X- and Y- indices can be 
supplied. "all" loads all frames. For the moment, only one of the two 
indices can be "all". 

Module-specific keyword arguments:
    *isNeXus* (boolean): default False, uses FabIO for data read-in. If True, 
        uses custom Diamond I22 NeXus reader defined herein.
    *Xi* (int or None): first index for frame to load. None reads all as 
        separate frames.
    *Yi* (int or None): second index for frame to load. None reads all as 
        separate frames. Note: Only Xi or Yi can be None!

Related keyword arguments:
    *name* : A freeform short string identifying the measurement
    *fname* : A single image filename in case only one image was recorded
    *fnames* : A list of filenames of images recorded comprising one measurement
        (recommended measurement method, allows for dezingering and 
        radiation damage checks), not used for NeXus files.
    *notes* : Sample notes (freeform string)
    *dname* : Detector name (freeform string)
    *dtransform* : Detector image transformation (integer from 1 to 8)

"""

__author__ = "Brian R. Pauw"
__contact__ = "brian@stack.nl"
__license__ = "GPLv3+"
__date__ = "2015/05/08"
__status__ = "beta"


from correctionBase import correctionBase
import numpy as np
import h5py # for NeXus support

class DS(correctionBase):
    _passthrough = list()
    _passthrough.append('Xi') # for loading image from 4D NeXus
    _passthrough.append('Yi') # for loading image from 4D NeXus
    _passthrough.append('isNeXus') # for loading image from 4D NeXus

    def do(self, Xi = None, Yi = None, isNeXus = False):

        # parse input arguments
        if isNeXus is None:
            isNeXus = False
        if isNeXus:
            if Xi is None:
                Xi = 0
            elif Xi in {'all', 'All', ':'}:
                Xi = slice(None)
            if Yi is None:
                Yi = 0
            elif Yi in {'all', 'All', ':'}:
                Yi = slice(None)

        # get the necessary parameters from settings.
        fnPar = self.parameters.getPar('fname')
        fnsPar = self.parameters.getPar('fnames')
        dtransform = self.parameters.getParVal('dtransform')
        dwindow = self.parameters.getParVal('dwindow')

        if fnsPar.isList():
            #there is a list of filenames instead of a single file.
            #set filename to the first file, used for storing details
            fnPar.setValue(fnsPar.value()[0])
        filename = fnPar.value()

        # initialise the data logger
        self.data.__initLogger__( filename )

        # store as first action in the processlog:                          
        self.data.logEntry( 'Data correction process started for file {}'
                .format(filename))                       

        if isNeXus:
            # open HDF5 file in read-only mode:
            with h5py.File(filename, mode = 'r') as h:
                # read frame(s); can result in "array is too big"-error 
                # if both Xi and Yi are ranges
                frames = h['entry1/detector/data'][Yi,Xi,:,:]
            # rearrange to move to imp2-style data: x, y, framei
            frames = frames.transpose().squeeze()
            if frames.ndim > 3:
                logging.error('ndim extracted from NeXus file >3 ')
                raise ValueError
            # cycle through data storage to ensure correct dimensions:
            self.data.data.setFrames(frames)
            frames = self.data.data.frames()

        else: # not NeXus
            # job kind of already done by readRaw function in data type.
            # which also sets workData
            if fnsPar.isList():
                #load a series of images
                self.data.data.setRawData( fabio.open(fnsPar.value()[0]) )
                self.data.data.setFrames(
                self.data.data.arrayDataType()(
                self.data.data.rawData().data.copy() )
                )
                if len(filename)>1:
                    for filen in filename[1:]:
                        fabimg = fabio.open(filen)
                        self.data.data.addFrames( fabimg.data.copy() )

            else:
                self.data.data.setRawData( fabio.open(fnPar.value()) )
                self.data.data.setFrames(
                self.data.data.arrayDataType()(
                self.data.data.rawData().data.copy() )
                )

            #so we reload
            frames = self.data.data.frames()

        #and transform if necessary
        if dtransform > 1:
            for framei in range( np.size(frames, 2)):
                frames[:, :, framei] = self.imtransform(
                        frames[:, :, framei], dtransform)
        # store frames
        self.data.data.setFrames(frames)

        # load I for mask
        I = self.data.data.workData()
        # set mask (all valid)
        mask = np.array(np.zeros(np.shape(I)), dtype = bool)
        # cycle through the frames to mask invalid pixels (dwindow)
        for framei in range( np.size(frames, 2)):                              
            mask[frames[:, :, framei] < dwindow.min()] = True                  
            mask[frames[:, :, framei] > dwindow.max()] = True       

        self.data.data.setMask(mask)

        # store as first action in the processlog:                          
        className = "NeXus"
        if not isNeXus:
            className = self.data.data.rawData().getclassname()
        self.data.logEntry(                         
                'datafile {} read as class {} with shape: {}, dwindow masked: {}'
                .format(                     
                    filename, className, frames.shape, mask.sum())          
                )                       

        return 

    def imtransform(self,img,ti=1):
        """
        image transformation following P. Boesecke's raster orientation schema.
        *img* is a required argument containing a 2D image.
        *ti* is an integer between 1-8 indicating which transformation to apply.
        returns: a transformed image.
        """
        if ti > 4:
            img = np.transpose(img)
            ti = 9 - ti
        if ti == 2:
            img = np.fliplr(img)
        elif ti == 3:
            img = np.flipud(img)
        elif ti == 4:
            img = np.fliplr( np.flipud(img) )
        return img


