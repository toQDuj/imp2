#!/usr/bin/env python                                                          
#coding: utf-8
#
"""
Overview
========
A writer for 2D data. Writes a NeXus and (hopefully) CanSAS2012-compatible 
HDF5 file containing the entire set of parameters used for the corrections 
as well as the correction log. 
The file thus written can be used for background subtraction or the more 
advanced "sandwich"-type background correction modules. This writer can also 
be used for final (archival) storage of 2D data *once the NeXus extension
has been written*.
The output filename is the sample filename with ".hdf5" tacked on at the end

TODO: 
    Write the NeXus extension storing the parameters and instrument settings
"""

__author__ = "Brian R. Pauw"
__contact__ = "brian@stack.nl"
__license__ = "GPLv3+"
__date__ = "2014/01/03"
__status__ = "incomplete"

# correctionBase contains the __init__ function for these classes
from correctionBase import correctionBase
from imp2 import cfg
import h5py
import os
import numpy as np

def writeData(iData, filename, skipFrames = False, remove = False):
    """
    writes all the internal datablocks to an HDF5-file.
    This file can be used for later data reduction but should
    not be used to store the final data. Future modules of a 
    NeXus- and CanSAS-style datawriter type are planned for this.
    """

    if os.path.exists(filename) and remove:
        #delete if exists
        os.remove(filename)

    h5f = h5py.File(filename)

    ##provision for canSAS2012 format:
    #cs = h5f.create_group("SASentry")

    #save our datasets in a special "imp"-group, preserving data format
    #as in this program:
    ix = h5f.create_group("imp")

    datasets = list()
    for ds in iData.attributes():
        if iData.get(ds) is None:
            print('* * * * no data for attribute {}'.format(ds))
            #skip data items with Nonetype value (not set)
            continue
        elif ds == "rawData":
            #only store the data element
            ix.create_dataset( ds, data = iData.get(ds).data,
                    compression = 'gzip')
        elif ds == "frames" and skipFrames:
            #skip frames
            continue
        else:
            data = iData.get(ds)
            if isinstance(data, np.ndarray):
                ix.create_dataset( ds, data = iData.get(ds), 
                        compression = 'gzip')
            else:
                ix.create_dataset( ds, data = iData.get(ds))
    h5f.close()

# name the class properly
class Write2D(correctionBase): #does not inherit from correctionBase
    _passthrough = list()
    _passthrough.append('filename')
    _passthrough.append('skipFrames')

    def do(self, filename = None, skipFrames = None):
        if filename is None:
            sfname = self.par.getParVal('fname')
            ofname = sfname + '.hdf5'
        else:
            ofname = filename
        if skipFrames is None:
            skipFrames = False
        if os.path.exists(ofname):
            #delete if exists
            os.remove(ofname)

        #write all datafields to HDF5 file, done by writeData module
        writeData(self.data.data, ofname, skipFrames)
        ##write the metadata (parameters) to the imp group in the file
        #self.writeMetaData(ofname)

        #Extend the file to include NeXus definitions
        #self.nexusExtension(self, ofname)

        #Extend the file to include canSAS2012 definitions
        #self.canSAS2012Extension(self, ofname)

        # store action in the processlog:                          
        self.data.logEntry(                         
                '2D data written to hdf5 file: {}'.format(                     
                    ofname)          
                )                       

        ########### end custom operation here ############

        return 

    def nexusExtension(self,filename):
        """Makes NeXus-structure and data links in the HDF file"""
        #TODO: write.
        #provision for NeXus format, link to imp datasets where possible:
        nx = h5f.create_group("NeXus")
        nx.attrs["NX_class"] = "NXentry"
        nd = nx.create_group("2DData")
        nd.attrs["NX_class"] = ["NXdata"]

        #store the datasets we need for further analysis: 2D data
        #should be link:
        nDWork = nd.create_dataset( "workData", 
                data = self.data.workData() )
        #set attributes
        nDWork.attrs["units"] = "A.U."
        nDWork.attrs["signal"] = 1
        #want both Qx and Qy here.
        nDWork.attrs["axes"] = "Qx,Qy"

        #should be link:
        nDQx = nd.create_dataset( "Qx",
                data = self.data.Qx() )
        #should be link:
        nDQy = nd.create_dataset( "Qy",
                data = self.data.Qy() )

        #also store 1D data
        nd = nx.create_group("1DData")
        nd.attrs["NX_class"] = ["NXdata"]
        #should be link:
        nDIntQ = nd.create_dataset( "intDataQ", 
                data = self.data.intDataQ() )
        #should be link:
        nDIntI = nd.create_dataset( "intDataI", 
                data = self.data.intDataI() )
        nDIntI.attrs["signal"] = 1
        nDIntI.attrs["axes"] = "intDataQ"
        nDIntI.attrs["units"] = "A.U. or (m sr)^{-1}"

        #close file
        h5f.flush()
        h5f.close()

