#!/usr/bin/env python                                                          
#coding: utf-8
#
"""
Overview
========
A reader for 2D IMP data. returns a dict with the data objects found in the
"imp2"-group in the HDF file. 
Required input arguments:
    *filename*: a filename (written by the imp2 "Write2D" code) to read

"""

__author__ = "Brian R. Pauw"
__contact__ = "brian@stack.nl"
__license__ = "GPLv3+"
__date__ = "2014/01/03"
__status__ = "incomplete"

# correctionBase contains the __init__ function for these classes
from correctionBase import correctionBase
from imp2 import cfg
import h5py
import os

# name the class properly
class Read2D(correctionBase): 
    _passthrough = list()
    #we wish to receive this as argument into "do".
    _passthrough.append('filename')

    @classmethod
    def do(self,filename = None):

        #read all datablocks from HDF5 file, done by readData module
        DataDict = self.readData(filename)

        #space to read datablocks from NeXus format

        #and parameters...

        return DataDict

    @classmethod
    def readData(self, filename):
        """
        reads all the internal datablocks of an imp2-written HDF5-file.
        """

        #file existence checks are supposed to be done by calling functions
        h5f = h5py.File(filename)
        
        DataDict = dict()
        for keyName in h5f['imp'].keys():
            #read items
            item = h5f['imp/'+keyName]
            DataDict[keyName] = item.value.copy()
            #cast into dataform as defined by self.data. not sure if necessary
            #if keyName in self.data.data.attributes():
            #    keyVal = self.data.data.get(keyName)
            #    if keyVal is None:
            #        DataDict[keyName] = item.value.copy()
            #        continue
            #    keyType = type(keyVal.value)
            #    if not keyType is numpy.ndarray:
            #        DataDict[keyName] = keyType(item.value.copy())
            #    else:
            #        DataDict[keyName] = np.array(item.value.copy(), dtype = keyVal.dtype)

        h5f.close()
        return DataDict

    @classmethod
    def nexusExtension(self,filename):
        """Makes NeXus-structure and data links in the HDF file"""
        #TODO: write.
        #provision for NeXus format, link to imp datasets where possible:
        pass
