#!/usr/bin/env python                                                          
#coding: utf-8
#
"""
Overview
========
Mask corrector module.
masks pinned pixels (to maximum or minimum), as well as applies 
the mask details given in mfname and mwindow
Arguments:
    *mfname* : The filename of the .png image containing the mask
    *mwindow* : two-element vector between which .png image values are
        considered to be masked values. Will be converted to float grayscale:
        range spanning from 0 to 1.
        For example, if the mask image contains "white" blotches that indicate
        the to-be-masked areas, set mwindow to [0.99,1.1].
        If black indicates masked areas, set mwindow to [-.01,.01]
        
"""

__author__ = "Brian R. Pauw"
__contact__ = "brian@stack.nl"
__license__ = "GPLv3+"
__date__ = "2013/12/19"
__status__ = "beta"

# correctionBase contains the __init__ function for these classes
from correctionBase import correctionBase
from matplotlib.pyplot import imread
import numpy as np

# name the class properly
class MK(correctionBase):

    # keep this name to "do", with no input arguments (settings are taken 
    # from the configuration
    def do(self):

        ########### insert custom operation here ############

        # get parameters that are required for the correction
        mfname = self.par.getParVal('mfname')
        mwindow = self.par.getParVal('mwindow')
        #if there is already a mask of sorts defined.
        mask = self.data.data.mask()

        #load image (should be png type), and make float
        maskImg = imread(mfname) * 1.
        #remove alpha channel:
        if np.size(maskImg, axis = 2) == 4:
            maskImg = maskImg[:,:,0:3]
            print('Alpha channel removed from mask image')
        if np.ndim(maskImg) > 2:
            #reduce to grayscale
            maskImg = maskImg.mean(axis = 2)
        
        #reduce to boolean
        maskImg = np.array(
                (maskImg > mwindow[0]) * (maskImg < mwindow[1]) ,
                dtype = bool)

        mask += maskImg
        self.data.data.setMask(mask)

        # store action in the processlog:                          
        self.data.logEntry(                         
                'Mask determined and stored. {} (additional) masked pixels'.format(                     
                    maskImg.sum())          
                )                       

        ########### end custom operation here ############

        return 

