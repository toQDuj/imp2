#!/usr/bin/env python                                                          
#coding: utf-8
#
"""
Overview
========
correction module for sample self-absorption (plate-like samples only)
"""

__author__ = "Brian R. Pauw"
__contact__ = "brian@stack.nl"
__license__ = "GPLv3+"
__date__ = "2013/12/24"
__status__ = "beta"

# correctionBase contains the __init__ function for these classes
from correctionBase import correctionBase
from numpy import sqrt
import numpy as np

# name the class properly
class SA(correctionBase):

    # keep this name to "do", with no input arguments (settings are taken 
    # from the configuration
    def do(self):

        # good thing we are only passing on data handles here
        frames = self.data.data.frames()
        AU = self.data.data.workDataAU()
        RU = self.data.data.workDataRU()

        ########### insert custom operation here ############

        # get parameters that are required for the correction
        wlength, tfact, sac = [
                self.par.getParVal(pn) for pn in 
                ("wlength", "tfact", "sac")
                ]
        # need Two Theta
        TwoT = self.data.data.TwoTheta(wlength)
        if sac == "plate":
            corr = (
                    1. - tfact**( 1 / np.cos(TwoT) - 1 ) 
                    ) / (
                    np.log(tfact) - 1 / np.cos(TwoT) * np.log(tfact) 
                    )
            corr[ (TwoT == 0) ] = 1.
        else:
            corr = 1.

        framesCor = frames
        for framei in range(np.size(frames, 2)):
            framesCor[:,:,framei] *= corr

        if AU is not None:
            #load absolute parameter uncertainties
            tfactSTD = self.par.getParSTD('tfact')
            # correct absolute uncertainty. timeSTD already relative
            if sac == "plate":
                AU = framesCor.sum() * sqrt( 
                        ( AU / frames.sum() )**2 + ( tfactSTD )**2 
                        )

        if RU is not None:
            # correct *relative* uncertainties for multiplication operation
            # interpixel uncertainty not affected other than simple scaling
            RU *= corr

        # store action in the processlog:                          
        self.data.logEntry(                         
                'Data corrected for self-absorption of type {}'.format(
                    sac)          
                )                       

        ########### end custom operation here ############

        # store data:
        self.data.data.setFrames( framesCor )

        # store if available:
        if AU is not None:
            self.data.data.setWorkDataAU(AU)
        if RU is not None:
            self.data.data.setWorkDataRU(RU)


        return 

