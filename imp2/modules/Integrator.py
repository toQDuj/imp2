#/usr/bin/env python                                                          
#coding: utf-8
#
"""
Overview
========
Integrator module for imp2, just a simple implementation at the moment. 
limitations: Only linear binning, slow, only azimuthal binning
Should be importing pyFAI modules for faster integration in the near future.
based on the code from McSAS/utils/binning.py:

     - :py:func:`binningWeighted1d`:
       Weighted binning, where the intensities of a
       pixel are divided between the two neighbouring bins depending on the
       distances to the centres. If error provided is empty, the standard
       deviation of the intensities in the bins are computed.

"""

__author__ = "Brian R. Pauw"
__contact__ = "brian@stack.nl"
__license__ = "GPLv3+"
__date__ = "2014/09/03"
__status__ = "beta"

# correctionBase contains the __init__ function for these classes
from correctionBase import correctionBase
import numpy as np
from numpy import (zeros, mean, sqrt, std, reshape, size, linspace,
                   argsort, ones, array, sort, diff, log10)

#define binning function (formerly binning_1D.py):

def binning1d(qEdges, q, intensity, error = None, qError = None):
    """An unweighted binning routine.
    imp-version of binning, taking q-bin edges in which binning takes place,
    and calculates the mean q uncertainty in the bin as well from the relative
    Q uncertainties provided.

    The intensities are sorted across bins of equal size. If provided error
    is empty, the standard deviation of the intensities in the bins is
    computed.
    """
    
    #flatten q, intensity and error
    q = reshape(q, size(q), 0)
    intensity = reshape(intensity, size(intensity), 0)

    # sort q, let intensity and error follow sort
    sortInd = argsort(q, axis = None)
    q = q[sortInd]
    intensity = intensity[sortInd]

    #initialise storage: 
    numBins = len(qEdges) - 1
    ibin = zeros(numBins)
    qbin = zeros(numBins)
    sdbin = zeros(numBins)    
    sebin = zeros(numBins)    
    qebin = zeros(numBins)    
    if error is not None:
        error = reshape(error, size(error), 0)
        error = error[sortInd]
    if qError is not None:
        qError = reshape(qError, size(qError), 0)
        qError = qError[sortInd]

    # now we can fill the bins
    for bini in range(numBins):
        # limit ourselves to only the bits we're interested in:
        limMask = ((q >= qEdges[bini]) &
                          (q <= qEdges[bini + 1]))

        iToBin = intensity[limMask]
        # sum the intensities in one bin and normalize by number of pixels
        if limMask.sum() > 0:
            ibin[bini] = iToBin.mean()
            qbin[bini] = ( q[limMask].mean() )
        else:
            #no pixels in bin
            (ibin[bini], sebin[bini], qebin[bini], qbin[bini]) = (
                    None, None, None, None)
            continue

        if error is not None:
            sebin[bini] = np.sqrt( (error[limMask]**2).sum() ) / limMask.sum()
        else:
            sebin[bini] = 0.

        # now we deal with the Errors:
        #calculate the standard deviation of the intensity in the bin
        # according to the definition of sample-standard deviation
        sdbin = iToBin.std(ddof = 1)
        #what we want is to have the "standard error of the mean": 
        sdbin = sdbin / sqrt(1.*np.size(iToBin))
        # maximum between standard error and Poisson statistics
        sebin[bini] = np.maximum(sebin[bini],sdbin)
        #qebin is the mean error of the q-values in the bin, should 
        #probably be superseded by the bin width
        qe = 0.
        if qError is not None:
            qe = np.sqrt( (qError[limMask]**2).sum() )
        #SSTD of q in bin:
        qs = np.std(q[limMask], ddof = 1) #sample standard deviation
        qebin[bini] = np.maximum(qe, qs)



    return qbin, ibin, sebin, qebin

# name the class properly
class Integrator(correctionBase):
    """The Imp2 module for data integration"""

    # keep this name to "do", with no input arguments (settings are taken 
    # from the configuration
    def do(self):

        # good thing we are only passing on data handles here
        I = self.data.data.workData()
        AU = self.data.data.workDataAU()
        RU = self.data.data.workDataRU()

        ########### insert custom operation here ############

        # get parameters that are required for the correction
        [nbin, ilim, idir, binscale] = [
                self.par.getParVal(pn) for pn in
                ('nbin', 'ilim', 'idir', 'binscale')
                ]
        [nbinSTD, ilimSTD, idirSTD, binscaleSTD] = [
                self.par.getParSTD(pn) for pn in
                ('nbin', 'ilim', 'idir', 'binscale')
                ]
        # need Q and Psi
        Q, Psi = self.data.data.QPsi()
        # need mask
        mask = self.data.data.mask().copy()
        #appy integration limits:
        iind = np.array((
            (Q < ilim[0]) + (Q > ilim[1]) + (Psi < ilim[2]) + (Psi > ilim[3])
            ),dtype=bool)
        mask[iind] = True #masked (set to False (size(img)) in DS by default)
        if RU is None:
            IRU = None
        else:
            IRU = RU[True - mask]

        #define binning limits
        (qmin, qmax) = (np.abs(Q[True - mask]).min(), np.abs(Q[True - mask]).max())
        #define bin edges
        if binscale.lower() in ("linear", "lin"):
            qEdges = np.linspace(qmin, qmax, nbin + 1)
        else:
            qEdges = np.logspace(log10(qmin), log10(qmax), nbin + 1)

        #integrate the valid pixels:
        Qbin, Ibin, Ebin, qEbin = binning1d(
                qEdges, Q[True - mask], I[True - mask], 
                error = IRU
                )

        # store action in the processlog:                          
        self.data.logEntry(                         
                'Data {} integrated in {} {}-spaced bins'.format(
                    "azimuthally", nbin, binscale)          
                )                       

        #store data:

        self.data.data.setIntDataQ(Qbin)
        self.data.data.setIntDataQEdges(qEdges)
        self.data.data.setIntDataQRU(qEbin)
        self.data.data.setIntDataI(Ibin)
        self.data.data.setIntDataIRU(Ebin)
        if not AU is None:
            self.data.data.setIntDataIAU(AU)

        ########### end custom operation here ############

        return 

# vim: set ts=4 sts=4 sw=4 tw=0:
