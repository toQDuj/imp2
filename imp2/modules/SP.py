#!/usr/bin/env python                                                          
#coding: utf-8
#
"""
Overview
========
correction module for spherical distortion (area dilation) 
"""

__author__ = "Brian R. Pauw"
__contact__ = "brian@stack.nl"
__license__ = "GPLv3+"
__date__ = "2014/03/19"
__status__ = "beta"

# correctionBase contains the __init__ function for these classes
from correctionBase import correctionBase
from numpy import sqrt, cos, arcsin, pi
import numpy as np

# name the class properly
class SP(correctionBase):

    # keep this name to "do", with no input arguments (settings are taken 
    # from the configuration
    def do(self):

        # good thing we are only passing on data handles here
        frames = self.data.data.frames()
        AU = self.data.data.workDataAU()
        RU = self.data.data.workDataRU()

        ########### insert custom operation here ############

        # get parameters that are required for the correction
        plength, clength, wlength = [
                self.par.getParVal(pn) for pn in 
                ("plength", "clength", "wlength")
                ]
        # need Q
        Q, dummy = self.data.data.QPsi()
        # convert to real-world distance
        Lp = clength / cos( 2 * arcsin( Q * wlength / (4 * pi) ) )
        corr = Lp**3 / clength**3

        framesCor = frames
        for framei in range(np.size(frames, 2)):
            framesCor[:,:,framei] *= corr

        if RU is not None:
            # correct *relative* uncertainties for multiplication operation
            # interpixel uncertainty not affected other than simple scaling
            RU *= corr

        # store action in the processlog:                          
        self.data.logEntry(                         
                'Data corrected for spherical aberration to max. {} %'.format(
                    corr.max()*100.)          
                )                       

        ########### end custom operation here ############

        # store data:
        self.data.data.setFrames( framesCor )

        # store if available:
        if AU is not None:
            self.data.data.setWorkDataAU(AU)
        if RU is not None:
            self.data.data.setWorkDataRU(RU)


        return 

