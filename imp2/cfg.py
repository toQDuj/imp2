#!/usr/bin/env python                                                          
#coding: utf8    
"""
carrier for data and info used for a correction
"""

__author__ = "Brian R. Pauw"
__contact__ = "brian@stack.nl"
__license__ = "GPLv3+"
__copyright__ = "National Institute of Materials Science, Tsukuba, Japan"
__date__ = "2013-12-16"
__status__ = "alpha"
version = "0.0.1"

# from impdata import impData
# from correctionInfo import cInfo

#instantiate an empty data class and default param class
# data = impData()
# param = cInfo()
#identical use
# parameters = param

