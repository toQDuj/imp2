#!/usr/bin/env python                                                          
#coding: utf-8
#

__author__ = "Brian R. Pauw"
__contact__ = "brian@stack.nl"
__license__ = "GPLv3+"
__date__ = "2013/12/11"
__status__ = "beta"

import logging, inspect
import numpy as np
from string import punctuation

class ParameterBase(object):
    """
    Overview
    ========
    Defines a parameter by its attributes and provides additional functionality
    for checking the validity of parameters. Parameters can be defined throuh
    use of keyword-value pairs or a simple JSON dict.
    Note that the parameter uncertainties are in relative units (standard dev.): 
    value = Value +/- STD * Value
    So indicate STD as a fraction of the value (i.e. 0.1 for 10% uncertainty)

    Example Usage
    ============
    testParameter=Parameter()
    testParameter.factory(name="tpar",value=12.3,valueRange=[0,inf],unit="mm",
        explanation="this is a test parameter")

    #change the value:
    testParameter.setValue(14.3)
    #or
    testParameter.set("Value",14.3)
    #set an evil bit (not part of the base attributes):
    testParameter.set("evil",True)
    #retrieve it at a later stage:
    testParameter.get("evil")
    
    Parses Parameter configuration supplied as keyword-value pairs, 
    adding Parameter attributes if they are not part of the base set.
    Can also take a simple json dict, using Parameter.factory(**[jsonDict])

    Base Parameter attributes are:
    - *Name*: internal (code) variable name
    - *DisplayName*: Short, descriptive name for GUI
    - *Value*: The value of the parameter
    - *ValueRange*: The valid value limits (for string values, this can be a 
        list of valid string choices). Two-element vector. For arrays, this  
        must be valid for all array values.
    - *ValueSTD*: The relative fractional uncertainty on the parameter value in 1 STD
    - *DataType*: Can be 'float', 'int', 'array', 'str' or 'bool'
    - *Explanation*: A string with a detailed explanation
    - *Default*: Default value to revert to in case of invalid supplied value
    - *Unit*: Short string that indicates the units, f.ex. 'm' or '1/(s m)' 
    - *Size*: The required number of values in an array.

    """
    #using Ingo Bressler's cutesnake/parameter functions for lots of inspiration:

    logging.getLogger('Parameter')   
    logging.basicConfig(level = logging.DEBUG)

    _name = None #internal variable name
    _displayName = None #short but descriptive name for GUIs
    _value = None #value of the parameter
    _valueRange = None #list of valid strings or 2-element min/max array 
    _valueSTD = None #relative uncertainty of the value indicating 1 STD 
    _dataType = None #data type, must be class or type, set by factory
    _explanation = None #detailed explanation of value
    _default = None #default value to revert to in case of error
    _unit = "" #units (or list of units) for the value
    _size = 1 #number of values in array if applicable, default is one. doesn't apply to strings
    
    #this dict indicates the base parameter attributes, base attributes
    #may use additional checks, whereas custom ones are accepted as is.
    _nameDict= { 
            'name' : 'Name', #identifier used in code
            'displayname' : 'DisplayName',
            'value' : 'Value',
            'valuerange' : 'ValueRange',
            'valuestd' : 'ValueSTD',
            'datatype' : 'DataType',
            'explanation' : 'Explanation',
            'default' : 'Default',
            'unit' : 'Unit',
            'size' : 'Size',
            }

    #from Ingo's cutesnake parameter class, not sure what it does
    @classmethod
    def _base(cls):
        lst = [b for b in cls.__bases__ if issubclass(b, ParameterBase) ]
        if not len(lst):
            return None
        return lst[0]

    @classmethod
    def factory(self, **kwargs):
        #processes input
        for kw in kwargs:
            try:
                self.set(kw, kwargs[kw])
            except:
                logging.warning('Could not set: {} to: {}'
                        .format(kw, kwargs[kw]) )

        #postprocessing:
        self.checkSize()
        self.clipValue()
        return self
    
    ################### BASIC FUNCTIONS ###################
    #added to improve compatibility with Ingo's parameter class
    def __str__(self):
        return "{}: {}".format(self.displayName(), self.value())

    def __eq__(self, other):
        if self.dataType() == other.dataType():
            try:
                return self.attributes()==other.attributes()
            except:
                return False
        else:
            return False

    def __ne__(self, other):
        return not self.__eq__(other)

    ################### METASETTERS AND METAGETTERS ###################

    @classmethod
    def setterName(self, name):
        return 'set' + self._nameDict[name.lower()]

    @classmethod
    def set(self, name, value):
        """sets a particular attribute, irrespective of whether it is a 
        standard parameter attribute or a custom one"""
        if name.lower() in self._nameDict:
            logging.debug('setting standard parameter {} to {}'
                    .format(name.lower(), value))
            
            setterName = self.setterName(name)
            setterFunc = getattr(self, setterName)
            setterFunc(value)

        else: #was a custom attribute
            logging.debug('setting custom parameter {} to {}'
                    .format(name, value))
            setattr(self, name, value)

    @classmethod
    def get(self, name):
        """gets value whether it is an extra attribute or standard value"""
        if name.lower() in self._nameDict:
            fullName = self._nameDict[name.lower()]
            getterName = fullName[0].lower() + fullName[1:]
            #lower the first letter:
            getterFunc = getattr(self,getterName)
            return getterFunc()

        else: #was a custom attribute
            return getattr(self, name)


    ################### SETTERS ###################

    #use setattr directly instead of this function:
    #should not be used, gets it wrong:
    #@classmethod
    #def setattr(self,aname,avalue):
    #    setattr(self,aname,avalue)
    #    #alternative, use self.__setattr__

    @classmethod
    def setName(self, name):
        #no check necessary, but should be a string
        self._name = str(name)

    @classmethod
    def setDisplayName(self, displayName):
        #no check necessary, but should be a string
        self._displayName = str(displayName)

    @classmethod
    def setValueSTD(self, value):
        if (value < 0.) or (value > 1.):
            logging.warning(
                    'STD must be relative, therefore between 0 and 1, not {}. setting to 0'
                    .format(value))
            value = 0.

        self._valueSTD = float(value)

    @classmethod
    def setExplanation(self, value):
        #nothing much to do
        self._explanation = str(value)

    @classmethod
    def setDefault(self, value):
        self.setValue(value, default = True)
        #not checking for clipping, should be unneccesary for defaults

    @classmethod
    def setUnit(self, value):
        self._unit = str(value)

    @classmethod
    def setValueRange(self,Vals):
        """should take care of putting minimum value in [0] position
        also should allow valueRange to be a list of valid options
        in case of a string value"""
        #defined for numeric and string
        pass

    @classmethod
    def setDataType(self,value=None):
        """stores the input data type as the data type itself has
        been defined on parameter generation. Data types cannot
        be changed"""
        pass #do nothing. Data type is set by class.
    #self._dataType = value

    @classmethod
    def setSize(self,value):
        #defined in array class
        if not (value==1):
            logging.warning('Only size of array type can be changed.')
            logging.warning('setting size to 1')
        self._size = 1

    @classmethod
    def setValue(self,value,default=False):
        """
        Sets the value in the parameter. 
        Datatype has to have been determined beforehand.
        """

        if not self.dataType() is None: 
            if isinstance(value, self.dataType(swapArray = True)):
                #swapArray in dataType ensures np.array is substituted for
                #its base class/type np.ndarray
                output = value
            else:
                logging.info('value {} not of the right datatype, trying to cast into {}'
                        .format(value, self.dataType()) )
                try:
                    output = self.dataType()(value)
                except:
                    logging.error('Could not cast value {} into datatype {}'
                            .format(value, self.dataType()) )
                    return
                logging.info('successfully cast.')
        else:
            #no datatype supplied
            logging.warning('Setting a value while the dataType is unknown: {}'
                    .format( self.dataType() ) )
            #self.setDataType( type(value) )
            output = value
        
        if default:
            self._default = output
            self._value = output
        else:
            self._value = output

    ################### GETTERS ###################

    #use getattr directly, not through this function:
    #@classmethod
    #def getattr(self,aname):
    #    if self.hasattr(aname):
    #        return getattr(self,aname)
    #    else:
    #        raise AttributeError

    #@classmethod
    #def hasattr(self,aname):
    #    return aname in self.attributeNames()


    @classmethod
    def attributeNames(self):
        """list of attibute names. Mine wasn't functioning
        correctly, so copied Ingo's attribute functions"""
        base = self._base()
        lst = list()
        if base is not None:
            lst.extend(base.attributeNames())
        cand = [name for name in dir(self) if not name.startswith("__")]
        for name in cand:
            if not name.startswith("_"):
                continue #only get internal members
            name = name[1:]
            if name in lst or not (name.lower() in self._nameDict.keys()):
                continue #skip if in base
            if self.setterName(name) in cand and name in cand:
                lst.append(name)
        #return dir(self)
        return lst

    @classmethod
    def attributes(self):
        """dictionary of attibutes and their values or locations"""
        res = dict(cls = self._base())
        for name in self.attributeNames():
            res[name] = getattr(self,name)

        #return vars(self)
        return res

    @classmethod
    def summary(self):
        """provides a text summary of the parameter details"""
        ds=unicode()
        ds+="Summary details for parameter: {}\n".format(self.name())
        for kw in self.attributeNames():
            ds+="\t {}: {}\n".format(kw,self.get(kw))

        return ds


    @classmethod
    def name(self):
        return self._name

    @classmethod
    def displayName(self):
        return self._displayName

    @classmethod
    def value(self):
        if self._value is None:
            return None

        dataType=self.dataType()
        #cast into datatype
        logging.debug('{} value: {}'.format(self.name(),self._value) )
        return dataType(self._value)

    @classmethod
    def valueRange(self):
        return self._valueRange

    @classmethod
    def dataType(self,swapArray=False):
        if swapArray and self._dataType is np.array:
            return np.ndarray

        return self._dataType

    #one more to follow standard definition:
    @classmethod
    def dtype(cls):
        return self.dataType()

    @classmethod
    def valueSTD(self):
        vSTD = self._valueSTD
        if vSTD is None:
            return 0.
        else:
            return vSTD

    @classmethod
    def explanation(self):
        return self._explanation

    @classmethod
    def default(self):
        return self._default

    @classmethod
    def unit(self):
        return self._unit

    @classmethod
    def size(self):
        #number of elements
        return self._size

    ################### CHECKERS ###################

    @classmethod
    def checkSize(self,value=None):
        #only defined for array
        pass #must be defined in the subclasses
            

    @classmethod
    def clipValue(self,value=None):
        #only defined for numeric
        pass #must be defined in the subclasses

    @classmethod
    def isDefault(self):
        """checks whether the set value is the default value"""
        return self.value()==self.default()

    ################### TESTS ###################

    @classmethod
    def canClip(self,value=None):
        if value is None:
            value=self.value()
        if value is None: #retrieved value is also not set
            #nothing to do if there is no value to clip
            return False

        valueRange=self.valueRange()
        if valueRange == None:
            logging.info( 'no valueRange set for {}, cannot perform clipping'
                    .format( self.name() ) )
            return False
        return True

    @classmethod
    def isBoolean(self,value=None):
        if value is None:
            value=self.value()
        return isinstance(value, bool)

    @classmethod
    def isList(self,value=None):
        if value is None:
            value=self.value()
        return isinstance(value, list)

    @classmethod
    def isString(self,value=None):
        if value is None:
            value=self.value()
        return isinstance(value, basestring)

    @classmethod
    def isFilename(self,value=None):
        """
        checks if file exists or can exist at a given string location
        """
        if value is None:
            value=self.value()
        
        if not isString(value):
            return False

        if not os.path.exists(value):
            try:
                open(value,'w').close()
                os.unlink(value)
            except OSError:
                #not a valid filename
                return False

        return True 
        
    @classmethod
    def isArray(self,value=None):
        #check for numpy.ndarray isinstance
        if value is None:
            value=self.value()
        return isinstance(value,np.ndarray)

    @classmethod
    def isNumeric(self,value=None):
        """
        Checks if the value in self or supplied value is numeric through duck typing
        Check if it is an array before checking if it is numeric.
        """
        if value is None:
            value=self.value()

        #check if it is a string:
        if self.isString(value):
            return False

        try:
            float(value)
        except StandardError:
            try:
                complex(value)
            except StandardError:
                return False

        return True

############# SUBCLASSES ##############

class ParameterNumerical(ParameterBase):
    #forms the basis of any numeric class: int, float and array

    @classmethod
    def setValueRange(self,Vals):
        """should take care of putting minimum value in [0] position
        also should allow valueRange to be a list of valid options
        in case of a string value"""
        if not (self.isList(Vals) or self.isArray(Vals)):
            logging.warning('ValueRange should be a list of strings or 2-element array, not setting valueRange.')
            return

        if self.isList(Vals):
            output = Vals

        elif self.isArray(Vals):
            output = np.array([np.min(Vals), np.max(Vals)])

        else:
            logging.warning('Please explain how you got here')
            output = None

        self._valueRange = output
        
    @classmethod
    def clipValue(self,value=None):
        """
        this will test if the value is out of the provided bounds, and clips
        the value to the bounds if necessary. 
        Works for arrays, numbers, boolean and strings 
        
        """
        if value is None:
            value=self.value()

        if not self.canClip(value):
            return

        valueRange = self.valueRange()
        #test to see if the supplied value falls within limits
        if ( value >= valueRange[0] ) and ( value <= valueRange[1] ):
            return #no problem
        else:
            logging.warning('supplied values {} out of range {} to {}. Clipping...' 
                    .format(value,valueRange[0],valueRange[1]) )     
            value=np.maximum( value, valueRange[0] )
            value=np.minimum( value, valueRange[1] )
            output=value

        self.setValue(output)

class ParameterFloat(ParameterNumerical):
    _dataType = float
    pass

class ParameterInteger(ParameterNumerical):
    _dataType = int
    pass

class ParameterArray(ParameterNumerical):
    _dataType = np.array

    #@classmethod
    #def value(self):
    #    #do not try to cast it into datatype, this is headache.
    #    return self._value

    @classmethod
    def setSize(self,value):
        if not isinstance(value,int):
            logging.warning('size value should be an integer')
            try:
                value = int(value)
                logging.warning('setting size to {}'.format(value))
            except:
                logging.warning('not setting size value, remains at {}'
                        .format(self.size() ) )
                return
        self._size = value

    @classmethod
    def setValueSTD(self,value):
        #for setting array of uncertainties:
        if not isinstance(value,np.ndarray):
            try:
                value = np.array(value)
            except:
                logging.warning(
                        "could not cast STD value {} of parameter {} into an array type"
                        .format(value,self.name() )
                        )
                return

        if any(value < 0.) or any(value > 1.):
            logging.warning(
                    'STD must be relative, therefore between 0 and 1, not {}. aetting offending values to 0'
                    .format(value))
            value[ ( value < 0. ) ] = 0.
            value[ ( value > 1. ) ] = 0.

        self._valueSTD = value

    @classmethod
    def valueSTD(self):
        vSTD = self._valueSTD
        if vSTD is None:
            return np.zeros( np.shape( self.value() ) )
        else:
            return vSTD

    @classmethod
    #replacement for the numerical function, this time for arrays
    def clipValue(self,value=None):
        """
        this will test if the value is out of the provided bounds, and clips
        the value to the bounds if necessary. 
        Works for arrays, numbers, boolean and strings 
        
        """

        if value is None:
            value=self.value()

        if not self.canClip(value):
            return
        valueRange = self.valueRange()

        test=np.array((value == value.clip( valueRange[0], valueRange[1]) ))
        if False in test:
            logging.warning('supplied values {} out of range {} to {}. Clipping...'
                    .format(value,valueRange[0],valueRange[1]) )
            output = value.clip( valueRange[0], valueRange[1] )
        else:
            output = value

        self.setValue(output)

    @classmethod
    def checkSize(self,value=None):
        """checks whether the number of values in a value array is identical
        to the required number of elements"""
        if value is None:
            value=self.value()

        reqSize=self.size()

        if (reqSize == 1) and not self.isArray(value):
            return True
        else:
            return np.prod( np.shape(value) )== reqSize

        return True


class ParameterBoolean(ParameterBase):
    _dataType = bool

class ParameterList(ParameterBase):
    #nothing special to do for now
    _dataType = list

class ParameterString(ParameterBase):
    _dataType = unicode

    @classmethod
    def canRead(self,value):
        """
        only has use in case parameter value is a filename!
        checks if file exists and can be read from a given string location
        """
        if value is None:
            value=self.value()

        if not isFilename(self,value):
            return False

        if os.path.exists(value):
            try:
                open(value,'r').close()
            except OSError:
                #not a valid filename
                return False

        return True

    @classmethod
    def setValueRange(self,Vals):
        """
        valueRange can be a list of valid options
        in case of a string value"""
        if not (self.isList(Vals)):
            logging.warning('ValueRange should be a list of strings, not setting valueRange.')
            return
        self._valueRange = Vals


    @classmethod
    def clipValue(self,value=None):
        """
        this will test if the value is out of the provided bounds, and clips
        the value to the bounds if necessary. 
        Works for arrays, numbers, boolean and strings 
        
        """

        if value is None:
            value=self.value()

        if not self.canClip(value):
            return
        #test to see if the supplied value falls within limits
        #check if value is part of list in valueRange
        if not value in self.valueRange():
            #set to default
            logging.warning('value {} not valid. Should be either of {}'
                    .format(value,self.valueRange()) )
            defa=self.default()
            if not defa is None:
                logging.warning('Default provided, setting to default: {}'
                        .format(self.default()) )
                output=defa
            else:
                logging.warning('Cannot revert to default: no default set')
                return
        else:
            output = value

        self.setValue(output)


#class ParameterFilename(ParameterString):
#    #special case of string
#    @classmethod
#    def canRead(self,value):
#        """
#        checks if file exists and can be read from a given string location
#        """
#        if value is None:
#            value=self.value()
#
#        if not isFilename(self,value):
#            return False
#
#        if os.path.exists(value):
#            try:
#                open(value,'r').close()
#            except OSError:
#                #not a valid filename
#                return False
#
#        return True
#

################## FACTORY ####################

def factory(**kwargs):
    """
    Provides a new parameter class based on input kwargs. dataType can
    be provided as kwarg or can be inferred from provided value 
    (or defualt) . If no value, default or dataType is given, it will 
    raise an error.
    """
    def parseDataType(value):
        """
        returns correct datatype format for a given 
        dataType (string, type or class). 
        Supported datatypes/classes:
        *array*
        *int*
        *float*
        *bool*
        *unicode*
        *filename*
        """
        #datatypes if supplied as a string (for example from JSON)
        stringSubst = {
                "array":np.array,
                "int":int,
                "float":float,
                "str":unicode,
                "bool":bool,
                "list":list,
                "unicode":unicode,
                "filename":unicode,
                }

        if isinstance(value,(str,unicode)):
            try:
                logging.info('DataType received string: {}, changing to type:'.
                        format(value) )
                value=stringSubst[value.lower()]
                logging.info('  {}'.format(value))
            except:
                logging.warning('supplied datatype: {} not understood!'
                        .format(value))

        #data type should be class or type
        if value is np.array or value is np.ndarray: #is valid, but apparently not to 
            return value
        elif not ( inspect.isclass(value) or isinstance( value, type) ):
            return None
        else:
            return value


    inputDataType = kwargs.pop("dataType",None) #should not be passed on
    value = kwargs.get("value",None)
    default = kwargs.get("default",None)
    name = kwargs.get("name",None)
    if (inputDataType is None) and (value is None) and (default is None):
        print "Cannot create a parameter without value, dataType or default"
        return False

    if inputDataType is None:
        if value is None:
            dataType = type(default)
        else:
            dataType = type(value)
        inputDataType = unicode(dataType)
    else:
        dataType = parseDataType(inputDataType)
        if dataType is None:
            print "cannot understand provided dataType {}".format(inputDataType)
            return False
        inputDataType = unicode(inputDataType)

    #Select the right subclass to use:
    #if "filename" in inputDataType.lower():
    #    cls = ParameterFilename
    #elif dataType==unicode:
    #    cls = ParameterString
    if dataType==unicode:
        cls = ParameterString
    elif dataType==str:
        cls = ParameterString
    elif dataType==bool:
        cls = ParameterBoolean
    elif dataType==np.ndarray:
        cls = ParameterArray
    elif dataType==np.array:
        cls = ParameterArray
    elif dataType==float:
        cls = ParameterFloat
    elif dataType==list:
        cls = ParameterList
    elif dataType==int:
        cls = ParameterInteger
    else:
        print "DataType {} not supported".format(dataType)
        return False

    #create a new class or type with given name and base class
    #we need a special translation map for unicode to remove punctuation:
    remove_punctuation_map = dict( (ord(character), None) 
            for character in punctuation )
    typeName = name.title().translate(remove_punctuation_map) + "Parameter" 
    NewType = type(str(typeName), (cls,), {})
    #set up the new class and return
    return NewType.factory(**kwargs)

if __name__ == "__main__":
    """
    Come up with some tests. Perhaps use doctest.
    """
    pass
